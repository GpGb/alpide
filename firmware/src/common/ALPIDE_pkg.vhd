library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.types_pkg.all;

package ALPIDE_pkg is

	---------------------------------------------------------------------------
	--------------------- ALPIDE regs and constants ---------------------------
	---------------------------------------------------------------------------

	-- small "hack" to cut slv slices (eg: slv47(15 dwto 0) -> slv47(data_rng))
	subtype OP_code_rng  is natural range 47 downto 40;
	subtype chip_id_rng  is natural range 39 downto 32;
	subtype reg_addr_rng is natural range 31 downto 16;
	subtype data_rng     is natural range 15 downto 0;

	-- ALPIDE Regs
	constant ALPIDE_REG_COMMAND     : slv16 := x"0000";
	constant ALPIDE_REG_MODE_CTRL   : slv16 := x"0001";
	constant ALPIDE_REG_FROMU_CFG1  : slv16 := x"0004";
	constant ALPIDE_REG_FROMU_CFG2  : slv16 := x"0005";
	constant ALPIDE_REG_FROMU_PLS1  : slv16 := x"0007";
	constant ALPIDE_REG_FROMU_PLS2  : slv16 := x"0008";
	constant ALPIDE_REG_CMUDMU      : slv16 := x"0010";
	constant ALPIDE_REG_PIX_CFG     : slv16 := x"0500";
	constant ALPIDE_REG_DAC_VRESETD : slv16 := x"0602";
	constant ALPIDE_REG_DAC_VCASN   : slv16 := x"0604";
	constant ALPIDE_REG_DAC_VCASN2  : slv16 := x"0607";
	constant ALPIDE_REG_DAC_IDB     : slv16 := x"060c";
	constant ALPIDE_REG_DAC_ITHR    : slv16 := x"060e";

	constant ALPIDE_DMU_FIFO_LO : slv16 := x"0012";
	constant ALPIDE_DMU_FIFO_HI : slv16 := x"0013";

	-- FSM OPcodes
	constant FSM_SET_MSK  : slv8 := x"f0";
	constant FSM_RD_MSK   : slv8 := x"f1";
	-- ALPIDE OPcodes
	constant ALPIDE_TRIG  : slv8 := x"55";
	constant ALPIDE_GRST  : slv8 := x"d2";
	constant ALPIDE_PRST  : slv8 := x"e4";
	constant ALPIDE_PULSE : slv8 := x"78";
	constant ALPIDE_BCRST : slv8 := x"36";
	constant ALPIDE_WROP  : slv8 := x"9c";
	constant ALPIDE_RDOP  : slv8 := x"4e";
	constant ALPIDE_RORST : slv8 := x"63";

	-- broadcast address
	constant ALPIDE_CHIPID_BRD : slv8  := x"0f";

	constant EV_CHIP_SKIPPED : std_logic_vector(23 downto 0) := x"ffffff";

end package;
