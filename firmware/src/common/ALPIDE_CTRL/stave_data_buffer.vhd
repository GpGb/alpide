library IEEE;
use IEEE.std_logic_1164.all;

entity stave_data_buffer is
	port(
		wr_clk : in std_logic;
		rd_clk : in std_logic;
		rst : in std_logic;
		-- input FF
		data_in       : in std_logic_vector(23 downto 0);
		data_in_strb  : in std_logic_vector(2 downto 0);
		data_in_last  : in std_logic;
		data_in_valid : in std_logic;
		data_in_ready : out std_logic;
		-- output FF
		data_out       : out std_logic_vector(7 downto 0);
		data_out_last  : out std_logic;
		data_out_valid : out std_logic;
		data_out_ready : in std_logic
	);
end stave_data_buffer;


architecture Behavioral of stave_data_buffer is

	type FSM_state is (idle, running);
	signal state : FSM_state := idle;
	attribute fsm_safe_state : string;
	attribute fsm_safe_state of state : signal is "auto_safe_state";

	signal byte_cnt : natural range 0 to 3 := 0;
	signal data_in_reg : std_logic_vector(23 downto 0);
	signal data_in_strb_reg : std_logic_vector(1 downto 0);
	signal data_in_last_reg : std_logic;
	signal byte_data_valid, byte_data_last, byte_data_ready : std_logic;
	signal buff_prog_full : std_logic;


	component data_fifo
		port (
			m_aclk : in std_logic;
			s_aclk : in std_logic;
			s_aresetn : in std_logic;
			s_axis_tvalid : in std_logic;
			s_axis_tready : out std_logic;
			s_axis_tdata : in std_logic_vector(7 downto 0);
			s_axis_tlast : in std_logic;
			m_axis_tvalid : out std_logic;
			m_axis_tready : in std_logic;
			m_axis_tdata : out std_logic_vector(7 downto 0);
			m_axis_tlast : out std_logic;
			axis_sbiterr : out std_logic;
			axis_dbiterr : out std_logic;
			axis_prog_full : out std_logic
		);
	end component;

begin

	data_in_ready <= (not buff_prog_full) when state = idle else '0';

	width_cnv: process(wr_clk) is
	begin
		if rising_edge(wr_clk) then

			byte_data_valid <= '0';

			case state is

				when idle =>
					data_in_reg <= data_in;
					data_in_strb_reg <= data_in_strb(1 downto 0);
					data_in_last_reg <= data_in_last;
					byte_data_last <= '0';
					byte_cnt <= 0;

					if data_in_valid = '1' and byte_data_ready = '1' and data_in_strb /= "000" then
						byte_data_valid <= data_in_strb(2);
						if data_in_strb /= "100" then
							state <= running;
						else
							byte_data_last <= data_in_last;
						end if;
					end if;

				when running =>
					data_in_reg <= data_in_reg(15 downto 0) & x"00";
					data_in_strb_reg <= data_in_strb_reg(0) & '0';
					byte_cnt <= byte_cnt + 1;

					byte_data_valid <= data_in_strb_reg(1);
					if data_in_strb_reg = "10" then
						byte_data_last <= data_in_last_reg;
					end if;

					if byte_cnt = 2 then
						state <= idle;
						byte_data_valid <= '0';
					end if;

			end case;

			if rst = '1' then
				state <= idle;
			end if;

		end if;
	end process;


	data_buff_inst: data_fifo
		port map(
			m_aclk    => rd_clk,
			s_aclk    => wr_clk,
			s_aresetn => not rst,
			s_axis_tvalid  => byte_data_valid,
			s_axis_tready  => byte_data_ready,
			s_axis_tdata   => data_in_reg(23 downto 16),
			s_axis_tlast   => byte_data_last,
			m_axis_tvalid  => data_out_valid,
			m_axis_tready  => data_out_ready,
			m_axis_tdata   => data_out,
			m_axis_tlast   => data_out_last,
			axis_prog_full => buff_prog_full
		);


end Behavioral;
