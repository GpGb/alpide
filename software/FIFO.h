#ifndef SRC_FIFO_H_
#define SRC_FIFO_H_

#define DATA_BUFF_MAX_LEN 512*4  // len in bytes

struct data_packet_buffer{
	uint8_t data[DATA_BUFF_MAX_LEN];
	uint32_t count;
};

extern struct data_packet_buffer RX_packet_buffer;

int init_fifo();
int read_pix_fifo();

#endif /* SRC_FIFO_H_ */
