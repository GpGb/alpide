library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package types_pkg is

	subtype slv8  is std_logic_vector(7 downto 0);
	subtype slv16 is std_logic_vector(15 downto 0);
	subtype slv32 is std_logic_vector(31 downto 0);

	type slv8_arr is array(natural range <>) of slv8;
	type slv16_arr is array(natural range <>) of slv16;
	type slv32_arr is array(natural range <>) of slv32;

	function uint2slv (num: in natural; len: in natural)
		return std_logic_vector;

	function slv2uint (vec: in std_logic_vector)
		return natural;

end package;

package body types_pkg is

	function uint2slv (num: in natural; len: in natural)
		return std_logic_vector is
	begin
		return std_logic_vector(to_unsigned(num, len));
	end uint2slv;

	function slv2uint (vec: in std_logic_vector)
		return natural is
	begin
		return to_integer(unsigned(vec));
	end slv2uint;

end types_pkg;
