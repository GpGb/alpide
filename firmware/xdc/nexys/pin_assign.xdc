# Crystal oscillator
set_property -dict { PACKAGE_PIN R4    IOSTANDARD LVCMOS33 } [get_ports { sysclk }];
# Reset swich
set_property -dict {PACKAGE_PIN E22 IOSTANDARD LVCMOS33} [get_ports reset]

# FT2232 USB bridge
set_property -dict {PACKAGE_PIN Y18  IOSTANDARD LVCMOS33} [get_ports FT_CLK]
set_property -dict {PACKAGE_PIN U20  IOSTANDARD LVCMOS33} [get_ports FT_DATA[0]]
set_property -dict {PACKAGE_PIN P14  IOSTANDARD LVCMOS33} [get_ports FT_DATA[1]]
set_property -dict {PACKAGE_PIN P15  IOSTANDARD LVCMOS33} [get_ports FT_DATA[2]]
set_property -dict {PACKAGE_PIN U17  IOSTANDARD LVCMOS33} [get_ports FT_DATA[3]]
set_property -dict {PACKAGE_PIN R17  IOSTANDARD LVCMOS33} [get_ports FT_DATA[4]]
set_property -dict {PACKAGE_PIN P16  IOSTANDARD LVCMOS33} [get_ports FT_DATA[5]]
set_property -dict {PACKAGE_PIN R18  IOSTANDARD LVCMOS33} [get_ports FT_DATA[6]]
set_property -dict {PACKAGE_PIN N14  IOSTANDARD LVCMOS33} [get_ports FT_DATA[7]]
set_property -dict {PACKAGE_PIN V17  IOSTANDARD LVCMOS33} [get_ports FT_OE_N]
set_property -dict {PACKAGE_PIN P19  IOSTANDARD LVCMOS33} [get_ports FT_RD_N]
set_property -dict {PACKAGE_PIN N17  IOSTANDARD LVCMOS33} [get_ports FT_RXF_N]
set_property -dict {PACKAGE_PIN P17  IOSTANDARD LVCMOS33} [get_ports SIWU_N]
set_property -dict {PACKAGE_PIN Y19  IOSTANDARD LVCMOS33} [get_ports FT_TXE_N]
set_property -dict {PACKAGE_PIN R19  IOSTANDARD LVCMOS33} [get_ports FT_WR_N]

# QSPI
set_property -dict {PACKAGE_PIN T19  IOSTANDARD LVCMOS33} [get_ports {qspi_cs}];
set_property -dict {PACKAGE_PIN P22  IOSTANDARD LVCMOS33} [get_ports {qspi_dq[0]}];
set_property -dict {PACKAGE_PIN R22  IOSTANDARD LVCMOS33} [get_ports {qspi_dq[1]}];
set_property -dict {PACKAGE_PIN P21  IOSTANDARD LVCMOS33} [get_ports {qspi_dq[2]}];
set_property -dict {PACKAGE_PIN R21  IOSTANDARD LVCMOS33} [get_ports {qspi_dq[3]}];


# ALPIDE TSP interface v1 card on FMC (TSP testcard v1.0)
set_property IOSTANDARD LVCMOS33 [get_ports ALP_clk];
set_property IOSTANDARD LVCMOS33 [get_ports ALP_ctrl_T];
set_property IOSTANDARD LVCMOS33 [get_ports ALP_ctrl_R];
set_property IOSTANDARD LVCMOS33 [get_ports ALP_ctrl_D];
set_property PACKAGE_PIN L16 [get_ports ALP_clk[0]];     #LA15_p
set_property PACKAGE_PIN L15 [get_ports ALP_clk[1]];     #LA11_n
set_property PACKAGE_PIN M18 [get_ports ALP_clk[2]];     #LA02_p
set_property PACKAGE_PIN N20 [get_ports ALP_ctrl_T[0]];  #LA04_p
set_property PACKAGE_PIN L13 [get_ports ALP_ctrl_T[1]];  #LA07_n
set_property PACKAGE_PIN A18 [get_ports ALP_ctrl_T[2]];  #LA19_p
set_property PACKAGE_PIN M20 [get_ports ALP_ctrl_R[0]];  #LA04_n
set_property PACKAGE_PIN L14 [get_ports ALP_ctrl_R[1]];  #LA11_p
set_property PACKAGE_PIN A19 [get_ports ALP_ctrl_R[2]];  #LA19_n
set_property PACKAGE_PIN L18 [get_ports ALP_ctrl_D[0]];  #LA02_n
set_property PACKAGE_PIN M13 [get_ports ALP_ctrl_D[1]];  #LA07_p
set_property PACKAGE_PIN K16 [get_ports ALP_ctrl_D[2]];  #LA15_n
set_property -dict {PACKAGE_PIN K17  IOSTANDARD LVCMOS33  PULLUP true} [get_ports TSP_IIC_scl_io]; #LA31_p
set_property -dict {PACKAGE_PIN J17  IOSTANDARD LVCMOS33  PULLUP true} [get_ports TSP_IIC_sda_io]; #LA31_n
set_property IOSTANDARD LVCMOS33 [get_ports TSP_pwr_good];
set_property PACKAGE_PIN K22 [get_ports TSP_pwr_good[0]];  #LA10_n
set_property PACKAGE_PIN J22 [get_ports TSP_pwr_good[1]];  #LA14_p
set_property PACKAGE_PIN H22 [get_ports TSP_pwr_good[2]];  #LA14_n
set_property -dict {PACKAGE_PIN N22  IOSTANDARD LVCMOS33} [get_ports {onewire[0]}]; #LA06_p
set_property -dict {PACKAGE_PIN K21  IOSTANDARD LVCMOS33} [get_ports {onewire[1]}]; #LA10_p
set_property -dict {PACKAGE_PIN M22  IOSTANDARD LVCMOS33} [get_ports {onewire[2]}]; #LA06_n

set_property -dict {PACKAGE_PIN A15  IOSTANDARD LVCMOS33  PULLDOWN true} [get_ports trig_evnt]; #LA32_p
set_property -dict {PACKAGE_PIN A16  IOSTANDARD LVCMOS33} [get_ports busy_out]; #LA32_n

# Voltage Adjust
set_property -dict {PACKAGE_PIN AA13  IOSTANDARD LVCMOS33} [get_ports {set_vadj[0]}];
set_property -dict {PACKAGE_PIN AB17  IOSTANDARD LVCMOS33} [get_ports {set_vadj[1]}];
set_property -dict {PACKAGE_PIN V14   IOSTANDARD LVCMOS33} [get_ports {vadj_en}];

# SEM mon shim
set_property -dict {PACKAGE_PIN Y8  IOSTANDARD LVCMOS33} [get_ports {SEM_mon_tx}];
set_property -dict {PACKAGE_PIN Y7  IOSTANDARD LVCMOS33} [get_ports {SEM_mon_rx}];

# Configuration options, can be used for all designs
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]

set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
