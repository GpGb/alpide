library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity CTRL_controller is
	port(
		clk   : in std_logic;
		rdclk : in std_logic;
		rst   : in std_logic;
		-- Command interface
		trig  : in std_logic;
		busy  : out std_logic;
		alp_cmd  : in std_logic_vector(47 downto 0);
		cmd_DV   : in std_logic;
		cmd_ack  : out std_logic;
		cmd_res  : out std_logic_vector(15 downto 0);
		res_DV   : out std_logic;
		cmd_err  : out std_logic;
		-- Data output
		data_out       : out std_logic_vector(7 downto 0);
		data_out_last  : out std_logic;
		data_out_valid : out std_logic;
		data_out_ready : in std_logic;
		-- CTRL
		ALP_clk : out std_logic;
		ALP_ctrl_R : in std_logic;
		ALP_ctrl_D : out std_logic;
		ALP_ctrl_T : out std_logic;
		-- conf
		clk_gate_dis : in std_logic
	);
end CTRL_controller;

architecture Behavioral of CTRL_controller is

	signal ALP_ctrl_out_T : std_logic;
	signal running : std_logic;

	-- CTRL FSM control and data
	signal CTRL_start, CTRL_done : std_logic;
	signal CTRL_data_out : std_logic_vector(15 downto 0);
	signal CTRL_data_in : std_logic_vector(47 downto 0);
	signal RX_err : std_logic_vector(1 downto 0);

	-- data fifo
	signal alp_data : std_logic_vector(23 downto 0);
	signal alp_data_strb : std_logic_vector(2 downto 0);
	signal alp_data_valid, alp_data_last : std_logic;
	signal buff_ready : std_logic;

begin


	CTRL_FSM: entity work.ALPIDE_FSM
	port map(
		clk => clk,
		rst => rst,
		running => running,
		-- Command interface
		trig    => trig,
		cmd_in  => alp_cmd,
		cmd_DV  => cmd_DV,
		cmd_ack => cmd_ack,
		res_DV  => res_DV,
		cmd_err => cmd_err,
		-- readout data output
		data_out      => alp_data(23 downto 0),
		data_valid    => alp_data_valid,
		data_out_strb => alp_data_strb,
		data_last     => alp_data_last,
		buff_full     => not buff_ready,
		-- CTRL module interface
		CTRL_start    => CTRL_start,
		CTRL_done     => CTRL_done,
		CTRL_data_in  => CTRL_data_in,
		CTRL_data_out => CTRL_data_out,
		RX_err        => RX_err
	);
	cmd_res <= alp_data(15 downto 0);
	busy <= (not buff_ready) or running;

	CTRL: entity work.ALPIDE_CTRL
		port map(
			clk      => clk,
			CTRL_in  => ALP_ctrl_R,
			CTRL_out => ALP_CTRL_D,
			DIR      => ALP_ctrl_out_T,
			data_in  => CTRL_data_in,
			data_out => CTRL_data_out,
			RX_err   => RX_err,
			start    => CTRL_start,
			done     => CTRL_done
		);
		ALP_ctrl_T <= not ALP_ctrl_out_T;


	data_buff: entity work.stave_data_buffer
		port map(
			wr_clk => clk,
			rd_clk => rdclk,
			rst    => rst,
			-- input FF ser
			data_in       => alp_data,
			data_in_strb  => alp_data_strb,
			data_in_last  => alp_data_last,
			data_in_valid => alp_data_valid,
			data_in_ready => buff_ready,
			-- output FF
			data_out       => data_out,
			data_out_last  => data_out_last,
			data_out_valid => data_out_valid,
			data_out_ready => data_out_ready
		);


	------------------------------------------------
	----------------- CLK Output -------------------
	------------------------------------------------
	-- inverted wrt ALP_clk
	O_ALP_clk : ODDR
		port map (
			Q  => ALP_clk,
			C  => clk,
			CE => running or clk_gate_dis,
			D1 => '1',
			D2 => '0',
			R  => '0',
			S  => '0'
		);

	------------------------------------------------
	------------------ CTRL I/O --------------------
	------------------------------------------------
	-- manchester encoding
	--ALP_ctrl_out_F <= not ALP_ctrl_out when MCR_enc_en ='1'
	--									else ALP_ctrl_out;
	--ctrl_mc_ddr : ODDR
	--	generic map(
	--		DDR_CLK_EDGE => "SAME_EDGE"
	--	)
	--	port map (
	--		Q  => ALP_CTRL_D,
	--		C  => clk,
	--		CE => '1',
	--		D1 => ALP_ctrl_out,
	--		D2 => ALP_ctrl_out_F,
	--		R  => '0',
	--		S  => '0'
	--	);

end Behavioral;
