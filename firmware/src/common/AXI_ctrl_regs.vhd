library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.types_pkg.all;

entity AXI_ctrl_regs is
	generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 32;
		n_staves : integer
	);
	port (
		S_AXI_ACLK : in std_logic;
		S_AXI_ARESETN : in std_logic;
		-- AXI Write channel
		S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWVALID : in std_logic;
		S_AXI_AWREADY : out std_logic;
		S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB   : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID  : in std_logic;
		S_AXI_WREADY  : out std_logic;
		S_AXI_BRESP   : out std_logic_vector(1 downto 0);
		S_AXI_BVALID  : out std_logic;
		S_AXI_BREADY  : in std_logic;
		-- AXI Read channel
		S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARVALID : in std_logic;
		S_AXI_ARREADY : out std_logic;
		S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP   : out std_logic_vector(1 downto 0);
		S_AXI_RVALID  : out std_logic;
		S_AXI_RREADY  : in std_logic;
		-- ALP interface
		alp_cmd       : out std_logic_vector(47 downto 0);
		alp_cmd_valid : out std_logic_vector(n_staves-1 downto 0);
		alp_cmd_res   : in slv16_arr(0 to n_staves-1);
		alp_cmd_res_DV : in std_logic_vector(n_staves-1 downto 0);
		alp_cmd_ack   : in std_logic_vector(n_staves-1 downto 0);
		alp_cmd_err   : in std_logic_vector(n_staves-1 downto 0);
		-- Ctrl regs Flags
		busy          : in std_logic_vector(n_staves-1 downto 0);
		clk_gate_dis  : out std_logic_vector(n_staves-1 downto 0);
		stave_mask    : out std_logic_vector(n_staves-1 downto 0);
		dec_skip_data : out std_logic;
		out_mux_en    : out std_logic;
		cpu_alarms    : out std_logic_vector(15 downto 0)
	);
end AXI_ctrl_regs;

architecture arch_imp of AXI_ctrl_regs is

	constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32)+ 1;
	constant OPT_MEM_ADDR_BITS : integer := 7;

	-- Slave Registers
	constant MAX_REG_LEN : integer := ADDR_LSB + OPT_MEM_ADDR_BITS;
	constant STATS_OFF : integer := 5;
	signal slv_reg_rw : slv32_arr(0 to STATS_OFF-1) := (others => (others => '0'));
	signal slv_reg_r  : slv32_arr(STATS_OFF to MAX_REG_LEN) := (others => (others => '0'));

	signal aw_en, rd_en : std_logic;

	signal alp_cmd_en : std_logic := '0';
	signal mux_stave_sel : natural range 0 to 15 := 0;
	signal mux_alp_cmd_res : std_logic_vector(15 downto 0);
	signal mux_alp_cmd_res_DV, mux_alp_cmd_err : std_logic;
	signal stave_sel : std_logic_vector(n_staves-1 downto 0);
	signal cat_alp_cmd_ack : std_logic_vector(n_staves-1 downto 0) := (others => '1');
	signal cat_alp_cmd_res_DV : std_logic_vector(n_staves-1 downto 0) := (others => '0');

	--attribute mark_debug : string;
	--attribute mark_debug of slv_reg_r, slv_reg_rw : signal is "true";
	--attribute mark_debug of alp_cmd_valid, alp_cmd_res, alp_cmd_res_DV, alp_cmd_ack,
	--	alp_cmd_err	: signal is "true";
	--attribute mark_debug of alp_cmd_en, stave_sel : signal is "true";
	--attribute mark_debug of cat_alp_cmd_ack, cat_alp_cmd_res_DV : signal is "true";
	--attribute mark_debug of mux_stave_sel, mux_alp_cmd_res, mux_alp_cmd_res_DV,
	--	mux_alp_cmd_err	: signal is "true";


begin

	-- AXI Write management
	-- The process waits for AWVALID, WVALID and then assert AWREADY and WREADY.
	-- Then then response is generated and and BVALID is asserted.
	-- In this case the write data is accepted (address is valid), data is written
	-- to memory mapped registers.
	-- Write strobes are used to select byte enables of slave registers while writing.

	-- The process waits for BREADY to deassert BVALID and restart the cycle.

	process (S_AXI_ACLK)

		variable loc_addr, octet, byte_index : integer;

	begin

		if rising_edge(S_AXI_ACLK) then

			if S_AXI_ARESETN = '0' then

				slv_reg_rw <= (others => (others => '0'));
				S_AXI_BVALID  <= '0';
				S_AXI_BRESP <= "00";
				aw_en <= '0';

			else

				loc_addr := slv2uint(S_AXI_AWADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB));

				if ((S_AXI_AWVALID and S_AXI_WVALID) = '1' and aw_en = '0') then

					if loc_addr < STATS_OFF then
						-- write data
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( S_AXI_WSTRB(byte_index) = '1' ) then
								octet := byte_index*8;
								slv_reg_rw(loc_addr)(octet+7 downto octet) <= S_AXI_WDATA(octet+7 downto octet);
							end if;
						end loop;

						aw_en <= '1';
						-- "OKAY" resp
						S_AXI_BRESP  <= "00";
					else
						-- "SLVERR" resp
						S_AXI_BRESP  <= "10";
					end if;

					S_AXI_AWREADY <= '1';
					S_AXI_WREADY <= '1';

				else
					S_AXI_AWREADY <= '0';
					S_AXI_WREADY <= '0';
				end if;


				if aw_en = '1' then
					S_AXI_BVALID <= aw_en;
					aw_en <= '0';
				end if;

				if S_AXI_BREADY = '1' and S_AXI_BVALID = '1' then
						S_AXI_BVALID <= '0';
				end if;

			end if;
		end if;
	end process;


	-- AXI read management
	-- ARREADY is always asserted until ARVALID is asserted.
	-- When ARVALID is asserted a response is generated and RVALID is asserted until RREADY is asserted.
	-- The read address is read and corresponding register is selected and served on the axi bus.

	process (S_AXI_ACLK)

		variable loc_addr : integer;

	begin

		if rising_edge(S_AXI_ACLK) then

			loc_addr := slv2uint(S_AXI_ARADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB));

			if S_AXI_ARESETN = '0' then

				S_AXI_ARREADY <= '0';
				S_AXI_RVALID  <= '0';
				S_AXI_RRESP   <= "00";
				S_AXI_RDATA <= (others => '0');
				rd_en <= '0';

			else

				S_AXI_ARREADY <= not rd_en;

				-- accept read address and generate response
				if (S_AXI_ARREADY = '1' and S_AXI_ARVALID = '1') then
					rd_en <= '1';
					S_AXI_RVALID <= '1';
					S_AXI_RRESP  <= "00"; -- 'OKAY' response
					if loc_addr < STATS_OFF then
						S_AXI_RDATA <= slv_reg_rw(loc_addr);
					else
						S_AXI_RDATA <= slv_reg_r(loc_addr);
					end if;
				end if;

				-- read complete when RREADY is asserted
				if (S_AXI_RREADY and S_AXI_RVALID) = '1' then
					rd_en <= '0';
					S_AXI_RVALID <= '0';
				end if;

			end if;
		end if;

	end process;


	/******************************************************
	******************** ALPIDE CONTROL *******************
	*******************************************************/

	----------------------------------------------------------------------------
	-- register flags assigments
	----------------------------------------------------------------------------

	-- CTRL regs map:
	--slv_reg(0) command register: (15:0) Opcode+chipID, (31:24) stave select
	--slv_reg(1) payload register: (15:0) alp data, (31:16) alp reg addr
	--slv_reg(3) readout mux conf: (15:0) stave mask, (16) output mux en, (17) skip data
	--slv_reg(4) cpu alarms: (0:1) temp alarm
	--slv_reg(5) response register: (15:0) chip data, (16) error, (17) reg data valid
	--slv_reg(6) busy register: (15:0) stave busy

	alp_cmd       <= slv_reg_rw(0)(15 downto 0) & slv_reg_rw(1);
	stave_sel     <= slv_reg_rw(0)(15+n_staves downto 16);
	clk_gate_dis  <= slv_reg_rw(2)(n_staves-1 downto 0);
	stave_mask    <= slv_reg_rw(3)(n_staves-1 downto 0);
	out_mux_en    <= slv_reg_rw(3)(16);
	dec_skip_data <= slv_reg_rw(3)(17);
	cpu_alarms    <= slv_reg_rw(4)(15 downto 0);

	----------------------------------------------------------------------------------
	-- implement operation triggers for control regs write/read
	----------------------------------------------------------------------------------
	process(S_AXI_ACLK)
		variable wr_loc_addr, rd_loc_addr : integer;
	begin
		if rising_edge(S_AXI_ACLK) then

			-- trigger operations on ctrl reg write NOTE: check strobe?
			wr_loc_addr := slv2uint(S_AXI_AWADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB));
			alp_cmd_en <= '0';

			if aw_en = '1' then
				case wr_loc_addr is
					when 0 => alp_cmd_en <= '1';
					when others => null;
				end case;
			end if;

			-- status reg ctrl latch and triggers on reg read
			rd_loc_addr := slv2uint(S_AXI_ARADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB));
			if rd_loc_addr = STATS_OFF and rd_en = '1' then
				slv_reg_r(STATS_OFF+0) <= (others => '0');
			end if;
			if mux_alp_cmd_res_DV = '1' then
				slv_reg_r(STATS_OFF+0)(17) <= '1';
				slv_reg_r(STATS_OFF+0)(16) <= mux_alp_cmd_err;
				slv_reg_r(STATS_OFF+0)(15 downto 0) <= mux_alp_cmd_res;
			end if;

			slv_reg_r(STATS_OFF+1)(n_staves-1 downto 0) <= busy;

			if S_AXI_ARESETN = '0' then
				slv_reg_r(STATS_OFF+0) <= (others => '0');
			end if;

		end if;
	end process;


	-----------------------------------------------------------------------------------
	-- implement handshake procedure for the command to the ALPIDE CTRL interface
	-----------------------------------------------------------------------------------
	alp_cmd_valid      <= not cat_alp_cmd_ack;
	-- reduction "or" as safety against stave_sel = 0;
	mux_alp_cmd_res_DV <= (or stave_sel) when ((cat_alp_cmd_res_DV or alp_cmd_res_DV) = stave_sel)
												else '0';
	mux_alp_cmd_res    <= alp_cmd_res(mux_stave_sel);
	mux_alp_cmd_err    <= alp_cmd_err(mux_stave_sel);

	ack_cat: process(S_AXI_ACLK)
	begin
		if rising_edge(S_AXI_ACLK) then
			cat_alp_cmd_ack <= cat_alp_cmd_ack or alp_cmd_ack;
			if alp_cmd_en = '1' then
				cat_alp_cmd_ack <= not stave_sel;
			end if;

			cat_alp_cmd_res_DV <= cat_alp_cmd_res_DV or alp_cmd_res_DV;
			if (cat_alp_cmd_res_DV or alp_cmd_res_DV) = stave_sel then
				cat_alp_cmd_res_DV <= (others => '0');
			end if;

			if S_AXI_ARESETN = '0' then
				cat_alp_cmd_ack <= (others => '1');
				cat_alp_cmd_res_DV <= (others => '0');
			end if;
		end if;
	end process;

	process(all)
	begin
		mux_stave_sel <= 0;
		for S in n_staves-1 downto 0 loop
			if stave_sel(S) = '1' then
				mux_stave_sel <= S;
			end if;
		end loop;

	end process;


end arch_imp;
