#ifndef SRC_ffs_io_H_
#define SRC_ffs_io_H_

#include "spiffs/spiffs.h"

int mount_spiffs();
const char* index_to_fname(uint8_t index);
int ffs_format();
int ffs_write_file(uint8_t index, void *src, uint32_t len, uint8_t mode);
int ffs_dump_file(uint8_t index, uint32_t offset);
int ffs_list_files();

spiffs_file ffs_open(const char *path, spiffs_flags flags, spiffs_mode mode);
int32_t ffs_close(spiffs_file fd);
int32_t ffs_read(spiffs_file fd, void *buf, int32_t len);
int32_t ffs_write(spiffs_file fd, void *buf, int32_t len);
int32_t ffs_lseek(spiffs_file fd, int32_t offs, int whence);
int32_t ffs_eof(spiffs_file fd);
int32_t ffs_errno();
int32_t ffs_check();

#endif
