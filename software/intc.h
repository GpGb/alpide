#ifndef SRC_INTC_H_
#define SRC_INTC_H_

extern volatile uint8_t command_rcv_intr;
extern volatile uint8_t temp_timer_intr;
extern volatile uint8_t mainloop_flag_intr;

#define INTC_WDT_SET_START 0xff
#define INTC_WDT_SET_STOP  0x11


int init_intc();
int init_start_wdtmr();
int start_stop_wdtmr(uint8_t set);

#endif // SRC_INTC_H_
