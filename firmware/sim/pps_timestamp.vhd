library ieee;
use ieee.std_logic_1164.all;
library unisim;
use unisim.vcomponents.all;

entity pps_timestamp_TB is
end pps_timestamp_TB;


architecture Behavioral of pps_timestamp_TB is

	constant tclk : time := 12.5 ns;
	constant pps_reset_len : integer := 6;

	signal clk, PPS : std_logic := '0';
	signal rst : std_logic := '1';
	signal timestamp : std_logic_vector(31 downto 0);

begin

	timestamp_inst: entity work.pps_timestamp
		generic map(
			clk_freq      => 80,
			pps_reset_len => pps_reset_len
		)
		port map(
			clk => clk,
			rst => rst,
			PPS => PPS,
			timestamp => timestamp
		);


		clk <= not clk after tclk/2;
		rst <= '0' after tclk*10;

		PPSgen: process
		begin

			PPS <= '1';
			wait for 100 us;

			-- spurios pulse
			wait for tclk*10;
			PPS <= '0';
			wait for tclk*(pps_reset_len-2);
			PPS <= '1';
			wait for 100 us;

			-- pps pulse to apply reset
			PPS <= '0';
			wait for tclk*(pps_reset_len+4);
			PPS <= '1';

			wait;

		end process;

end Behavioral;
