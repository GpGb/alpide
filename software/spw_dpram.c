#include <stdint.h>
#include <string.h>
#include "spw_dpram.h"

static uint32_t data_count = 0;

volatile uint32_t* spw_dpram_state = (volatile uint32_t*) DPRAM_BASEADDR + 1;
volatile uint8_t* spw_dpram_data = (volatile uint8_t*) DPRAM_BASEADDR + 8;


int bram_write_done(){

	// zero padding
	uint8_t remainder = data_count%2;
	if (remainder){
		memset(&spw_dpram_data[data_count], 0, remainder);
		data_count += remainder;
	}

	*spw_dpram_state = data_count;
	data_count = 0;

	return 0;
}


int bram_write_scratch(const void *data, uint32_t bytes){

	//if (*spw_dpram_state != 0)
	//	return -2;

	if (data_count + bytes >= DPRAM_SCRATCH_MAX)
		return -1;

	memcpy(&spw_dpram_data[data_count], data, bytes);
	data_count += bytes;

	return 0;
}
