----------------------------------------------------------------------------------
--
-- This core manages the serial CTRL line of the ALPIDE FE.
-- Internal interface signals description:
-- data_in, data_out : input data to serilalzie and output data received
-- start, done : also used as in/out data valid
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.types_pkg.all;
use work.ALPIDE_pkg.all;


entity ALPIDE_CTRL is
	port(
		clk : in std_logic;
		-- serial output 3-state I/O with direction sel
		CTRL_in  : in std_logic;
		CTRL_out : out std_logic;
		DIR  : out std_logic;
		-- internal interface
		data_in  : in std_logic_vector(47 downto 0);
		data_out : out std_logic_vector(15 downto 0);
		RX_err   : out std_logic_vector(1 downto 0);
		start    : in std_logic;
		done     : out std_logic
	);
end ALPIDE_CTRL;

architecture Behavioral of ALPIDE_CTRL is

	signal CTRL_in_reg : std_logic;

	type FSM_state is (idle, write, turnaround, read);
	signal state : FSM_state := idle;
	attribute fsm_safe_state : string;
	attribute fsm_safe_state of state : signal is "auto_safe_state";

	alias reg_addr_HI : slv8 is data_in(31 downto 24);
	alias reg_addr_LO : slv8 is data_in(23 downto 16);
	alias data_HI     : slv8 is data_in(15 downto 8);
	alias data_LO     : slv8 is data_in(7 downto 0);

	signal bit_cntr, bit_tot : natural range 0 to 87 := 0;
	signal rd_cnt  : natural range 0 to 8 := 0;
	signal rcv_cnt : natural range 0 to 3 := 0;
	signal rd_en : std_logic;

	signal data_pkg : std_logic_vector(59 downto 0);
	signal data_rcv : std_logic_vector(23 downto 0) := (others => '0');

	alias frame_err : std_logic is RX_err(0);
	alias bad_ID    : std_logic is RX_err(1);

	signal rd_word : std_logic := '0';

	--attribute mark_debug : string;
	--attribute mark_debug of state, CTRL_out, start, done, data_in, data_out, RX_err,
	--	bit_cntr, rd_word, data_rcv, CTRL_in, rd_cnt : signal is "true";

begin


	reg_in: process(clk) is
	begin
		if rising_edge(clk) then
			CTRL_in_reg <= CTRL_in;
		end if;
	end process;

	link_FSM: process(clk) is
	begin
		if rising_edge(clk) then

			bit_cntr <= bit_cntr + 1;

			case state is

				when idle =>
					DIR <= '0';
					CTRL_out <= '1';
					done <= '0';
					bit_cntr <= 0;
					rcv_cnt <= 0;
					rd_en <= '0';
					rd_word <= '0';

					if start = '1' then
						data_pkg <= '1' & data_HI & "01" & data_LO & "01" &
												reg_addr_HI & "01" & reg_addr_LO & "01" &
												data_in(chip_id_rng) & "01" & data_in(OP_code_rng) & '0';
						state <= write;
						RX_err <= "00";
						case data_in(OP_code_rng) is
							when ALPIDE_WROP =>
								bit_tot <= 59;
							when ALPIDE_RDOP =>
								bit_tot <= 39;
								rd_en <= '1';
							when others =>
								-- if not rdop or wrop, assume a short command
								-- (worst case scenario, alpide will ignore it)
								bit_tot <= 9;
						end case;
					end if;


				when write =>
					CTRL_out  <= data_pkg(0);
					data_pkg <= data_pkg(0) & data_pkg(59 downto 1);

					if bit_cntr = bit_tot then
						bit_cntr <= 0;
						if rd_en = '0' then
							done <= '1';
							state <= idle;
						else
							state <= turnaround;
						end if;
					end if;


				when turnaround =>
					CTRL_out <= '1';
					if bit_cntr = 4 then
						DIR <= not DIR;
					elsif bit_cntr = 13 then
						if DIR = '1' then
							state <= read;
							bit_cntr <= 0;
						else
							state <= idle;
							done <= '1';
						end if;
					end if;


				when read =>
					CTRL_out <= '1';

					-- wait starbit
					if CTRL_in_reg = '0' and rd_word = '0' then
						rd_word <= '1';
					end if;

					-- shift in word
					if rd_word = '1' then
						data_rcv <= CTRL_in_reg & data_rcv(23 downto 1);
						rd_cnt <= rd_cnt + 1;
					end if;
					if rd_cnt = 7 then
						rcv_cnt <= rcv_cnt + 1;
						rd_word <= '0';
					elsif rd_cnt = 8 then
						rd_cnt <= 0;
						frame_err <= (not CTRL_in_reg) and (not frame_err);
					end if;

					if bit_cntr = 40 then
						state <= turnaround;
						bit_cntr <= 0;
						rd_cnt <= 0;
					end if;

					-- err checks
					bad_ID <= data_rcv(7 downto 0) ?/= data_in(chip_id_rng);
					if rcv_cnt /= 3 then frame_err <= '1'; end if;

			end case;

		end if;
	end process;

	data_out <= data_rcv(23 downto 8);

end Behavioral;
