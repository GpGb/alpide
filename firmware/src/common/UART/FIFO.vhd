-- meh FIFO with FWFT and WORDS_BITS^2 adresses, seems that the simplest 
-- solution is to keep track of the number of written words using a counter.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity FIFO is

  generic( 
    WORDS_BITS  : natural := 2;
    ALMOST_FULL : natural := 1
  );
  
  port( 
    CLK         : in std_logic;
    rd, wr      : in std_logic;
    data_in     : in std_logic_vector(7 downto 0);
    data_out    : out std_logic_vector(7 downto 0);
    empty, full : out std_logic;
    almst_full  : out std_logic
  );

end FIFO;


architecture FIFO_logic of FIFO is

  constant WORDS : natural := 2**WORDS_BITS - 1;
  
  type memory_array is array(WORDS downto 0) of std_logic_vector(7 downto 0);
  signal memory : memory_array;

  signal rd_ptr   : unsigned(WORDS_BITS - 1 downto 0) := (others => '0');
  signal wr_ptr   : unsigned(WORDS_BITS - 1 downto 0) := (others => '0');
  signal counter  : natural range 0 to WORDS + 1 := 0;
  signal is_full  : std_logic := '0';
  signal is_empty : std_logic := '1';
  
begin

  empty <= is_empty;
  full  <= is_full;

  mem_logic: process(CLK)
  begin
  
    if rising_edge(CLK) then

      if rd = '1' and is_empty = '0' then
        data_out <= memory( to_integer(rd_ptr+1) );
        rd_ptr <= rd_ptr + 1;
      else
        data_out <= memory( to_integer(rd_ptr) );
      end if;

      if wr = '1' and is_full = '0' then
        memory( to_integer(wr_ptr) ) <= data_in;
        wr_ptr <= wr_ptr + 1;
      end if;
    
      if rd = '1' and is_empty = '0' and wr = '0' then
        counter <= counter - 1;
      elsif rd = '0' and wr= '1' and is_full = '0' then
        counter <= counter + 1;
      end if;
      
    end if;  
  end process mem_logic;

  -- Both this process end the uncommented code below should be equivalent.
--  process(counter)
--  begin
--    -- default values
--    is_full <= '0';
--    is_empty <= '0';
--    almst_full <= '0';
--    ----
--    if counter = WORDS + 1 then
--      is_full <= '1';
--    elsif counter = 0 then
--      is_empty <= '1';
--    end if;
--    if counter > WORDS - ALMOST_FULL then
--      almst_full <= '1';
--    end if;
--  end process;
  
  is_full <= '1' when counter = WORDS + 1 else '0';
  is_empty <= '1' when counter = 0 else '0';
  almst_full <= '1' when counter > WORDS - ALMOST_FULL else '0';
  
end FIFO_logic;
