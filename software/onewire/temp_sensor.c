#include <stdint.h>
#include <string.h>
#include <xil_io.h>

#include "ownet.h"
#include "findtype.h"
#include "../spw_dpram.h"
#include "../platform.h"
#include "../alpide.h"

#define N_PORTS 16  // 15 staves + tdaq local
#define DEVICE_FAMILY  0x28
#define OW_PORT_MAX_DEVICES 7 // Num of sensors
#define OW_SN_LEN 8

#define RAW_TEMP_MAX  125
#define RAW_TEMP_CRIT  40
#define TEMP_OK         0
#define TEMP_ALRM_TDAQ  1
#define TEMP_ALRM_STAVE 2


uint8_t temp_ignore_mask[N_PORTS] = {0};


int broadcast_convert_T(){

	for(int port=0; port<N_PORTS; port++){
		owTouchReset(port);

		// send "skip rom" 0xCC command
		owWriteByte(port, 0xCC);

		// send the convert 0x44 command and start power delivery
		owWriteBytePower(port, 0x44);
	}

	// sleep for 1 second (conversion time)
	msDelay(1000);

	for(int port=0; port<N_PORTS; port++){
		// turn off the 1-Wire Net strong pull-up
		owLevel(port, MODE_NORMAL);
	}

	return TRUE;
}


int ReadTemperature28(int portnum, uchar *SerialNum, uint16_t *Temp){

	// set the device serial number to the counter device
	owSerialNum(portnum,SerialNum,FALSE);

	// access the device
	if (!owAccess(portnum))
		return FALSE;

	// create a block to send that reads the temperature
	// read scratchpad command
	int send_cnt = 0;
	uchar send_block[30];
	send_block[send_cnt++] = 0xBE;
	// now add the read bytes for data bytes and crc8
	for (int i = 0; i < 9; i++)
		send_block[send_cnt++] = 0xFF;

	// now send the block
	if (!owBlock(portnum,FALSE,send_block,send_cnt))
		return FALSE;

	// initialize the CRC8
	setcrc8(portnum,0);
	// perform the CRC8 on the last 8 bytes of packet
	uchar lastcrc8;
	for (int i = send_cnt - 9; i < send_cnt; i++)
		lastcrc8 = docrc8(portnum,send_block[i]);

	// verify CRC8 is correct
	if (lastcrc8 != 0x00)
		return FALSE;

	// calculate the high-res temperature
	//int tsht;
	//tsht =        send_block[2] << 8;
	//tsht = tsht | send_block[1];
	//if (tsht & 0x00001000)
	//	tsht = tsht | 0xffff0000;

	*Temp = (send_block[2] << 8) | send_block[1];

	return TRUE;
}


float raw_to_float(uint16_t raw_temp){

	int tsht = raw_temp;
	if (tsht & 0x00001000)
		tsht = tsht | 0xffff0000;

	float temp = ((float)tsht)/16;
	return temp;
}


//void DisplaySerialNum(uchar sn[8]){
//	for (int i = 7; i>=0; i--)
//		dbg_print("%02X", (int)sn[i]);
//}


int read_stave_temp_data(uint32_t stave_id, int n_devs, volatile uint16_t* dest){

	//TODO: keep order on missing sensors, don't run discovery every time
	uchar ThermoSN[OW_PORT_MAX_DEVICES+1][OW_SN_LEN];
	int num = FindDevices(stave_id, &ThermoSN[0], DEVICE_FAMILY, n_devs+1);

	if (num != n_devs){
		for(int n=0; n < n_devs-num; n++){
			dest[n_devs-n-1] = 0xffff; // NaN
		}

		if (num == 0) return 0;
	}

	//dbg_print("staveid: %d, devices found: %d, exp: %d\n\r", stave_id, num, n_devs);

	uint16_t raw_temp;
	int retcode = TEMP_OK;

	for(int i=0; i<num; i++){

		int status = 0;

		if ((temp_ignore_mask[stave_id]>>i)&0x1 != 0){
			status = FALSE;
		}
		else{
			status = ReadTemperature28(stave_id, ThermoSN[i], &raw_temp);
		}

		if (status){
			dest[i] = raw_temp;
			float temp = raw_to_float(raw_temp);

			if ((temp < RAW_TEMP_MAX) && (temp > RAW_TEMP_CRIT)){
				retcode = (stave_id == 15) ? TEMP_ALRM_TDAQ : TEMP_ALRM_STAVE;
				dbg_print("T ALARM stave: %d, raw: %x\n\r", stave_id, raw_temp);
			}

		} else { // NaN
				dest[i] = 0xffff;
		}

	}

	return retcode;
}


int set_temp_mask(uint8_t staveid, uint8_t mask){

	if (staveid >= N_PORTS)
		return -1;

	temp_ignore_mask[staveid] = mask;

	// try to reset alarms as well
	write_alarm_reg(ALARM_TYPE_TEMP, 0);

	return 0;
}


int read_tracker_temp_data(){

	broadcast_convert_T();

	volatile uint16_t* dpram_temp = (volatile uint16_t*) (DPRAM_TEMP_DATA_OFFSET+8);
	int retcode = 0;
	uint32_t byte_count = 0;

	for(int port=0; port<N_PORTS; port++){

		int temps_num = 0;
		if (port == 15)
			temps_num = 2;
		else if (port%3 == 1)
			temps_num = 7;
		else
			temps_num = 6;

		retcode |= read_stave_temp_data(port, temps_num, dpram_temp);
		dpram_temp += temps_num;
		byte_count += temps_num*sizeof(*dpram_temp);
	}

	// tstamp
	Xil_Out32(DPRAM_TEMP_DATA_OFFSET+4,
			Xil_In32(DPRAM_TEMP_DATA_OFFSET+4) + 1);
	// area len, byte_count + tstamp
	Xil_Out32(DPRAM_TEMP_DATA_OFFSET, byte_count+4);


	write_alarm_reg(ALARM_TYPE_TEMP, retcode&0x03);
	return 0;
}
