library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library std;
use std.textio.all;

use work.types_pkg.all;


entity data_decoder_TB is
end data_decoder_TB;

architecture Behavioral of data_decoder_TB is

	constant tclk : time := 10 ns;

	signal clk, rst : std_logic := '0';
	signal ready, word_in_dv, word_in_lst : std_logic;
	signal word_in : std_logic_vector(7 downto 0) ;
	signal dec_skip_data : std_logic;

	signal axis_pix_tdata : std_logic_vector(31 downto 0);
	signal axis_pix_tready, axis_pix_tvalid, axis_pix_tlast : std_logic := '0';

	type t_data_vec is record
		data : slv8_arr(0 to 41);
		len  : natural;
	end record t_data_vec;

	constant test_data : t_data_vec := (
		data => (
			-- stave 0, id 3: 2 data long with 8 hits
			x"f0", x"a3", x"ff", x"c0",
			x"00", x"00", x"7f", x"00", x"08", x"7f",
			x"b0", x"ef", x"dc", x"00",
			-- stave 1, id 8: 2 data long with 3 hits
			x"f1", x"a8", x"ff", x"c1",
			x"00", x"00", x"03", x"00", x"08", x"03",
			x"b0", x"ef", x"dc", x"00",
			-- stave 2, id 1: 2 data long with 5 hits
			x"f2", x"a1", x"ff", x"c1",
			x"00", x"00", x"33", x"00", x"08", x"33",
			x"b0", x"ef", x"dc", x"00"
	),
		len => 42
	);


	-------------------------------- File Read --------------------------------------
	--impure function rd_from_file(filename : string)
	--	return t_data_vec is

	--		type char_file_t is file of character;
	--		file data_file : char_file_t;
	--		variable char_buff : character;

	--		variable data_buff : t_data_vec := (data => (others => x"00"), len => 0);
	--		variable pkt_len : unsigned(15 downto 0);
	--		variable pkt_cnt : natural := 0;

	--begin

	--	file_open(data_file, filename, read_mode);

	--	while not endfile(data_file) loop

	--		-- skip header 0xffff
	--		read(data_file, char_buff);
	--		read(data_file, char_buff);

	--		-- read pkt len
	--		read(data_file, char_buff);
	--		pkt_len(15 downto 8) := to_unsigned(character'pos(char_buff), 8);
	--		read(data_file, char_buff);
	--		pkt_len(7 downto 0)  := to_unsigned(character'pos(char_buff), 8);
	--		assert pkt_len < 400 report "packet too long" severity failure;
	--		report "packet len: " & to_hstring(pkt_len);

	--		-- skip rest of header
	--		for i in 0 to 10 loop
	--			read(data_file, char_buff);
	--		end loop;

	--		-- read data
	--		for i in 0 to to_integer(pkt_len) loop
	--			read(data_file, char_buff);

	--			data_buff.data(data_buff.len) :=
	--				std_logic_vector(to_unsigned(character'pos(char_buff), 8));
	--			data_buff.len  := data_buff.len + 1;
	--		end loop;

	--		pkt_cnt := pkt_cnt + 1;
	--		if pkt_cnt > 10 then
	--			exit;
	--		end if;

	--	end loop;

	--	return data_buff;

	--end rd_from_file;
	----------------------------------------------------------------------------------

	--constant test_data : t_data_vec := rd_from_file("test_data.raw");

begin


	uut: entity work.data_decoder
	port map(
		clk         => clk,
		rst         => rst,
		word_in     => word_in,
		word_in_dv  => word_in_dv,
		word_in_lst => word_in_lst,
		ready       => ready,
		axis_pix_tdata  => axis_pix_tdata,
		axis_pix_tlast  => axis_pix_tlast,
		axis_pix_tready => axis_pix_tready,
		axis_pix_tvalid => axis_pix_tvalid,
		dec_skip_data => dec_skip_data
	);

	clk <= not clk after Tclk/2;


	-- pretty much how the clock converter on the axis
	-- receiveing side works in synchronous mode
	tready_gen: process is
	begin
		wait until clk = '1';
		while true loop
			axis_pix_tready <= not axis_pix_tready;
			wait for Tclk;
		end loop;
	end process;


	data_feed: process

		-- send test_data to decoder
		procedure stream_data(
			delay : in integer;
			reps  : in integer
		) is
		begin
			for i in 0 to reps loop

				for n in 0 to test_data.len-1 loop

					while ready = '0' loop
						wait for Tclk;
					end loop;

					word_in <= test_data.data(n);
					word_in_dv <= '1';
					if n = test_data.len-1 and i = reps then
						word_in_lst <= '1';
					end if;

					wait for Tclk;

					if delay /= 0 then
						word_in_dv <= '0';
						wait for Tclk*delay;
					end if;

				end loop;

			end loop;

			word_in_lst <= '0';
			word_in_dv <= '0';

		end procedure;

	begin

		rst <= '0';
		word_in_dv <= '0';
		word_in_lst <= '0';
		dec_skip_data <= '1';

		wait for Tclk*100;
		wait until rising_edge(clk);

		-- test pixel counter mode
		stream_data(0, 0);
		wait for Tclk*40;
		stream_data(4, 0);

		wait for Tclk*40;

		-- test decoder mode
		dec_skip_data <= '0';
		stream_data(1, 0);

		-- overflow test
		wait for Tclk*40;
		stream_data(1, 200);

		wait;
	end process;



	--record_out: process
	--	variable word_index : natural := 0;
	--begin
	--	wait until rising_edge(clk);

	--	axis_pix_tready <= '1';

	--	while true loop

	--		if axis_pix_tvalid = '1' and dec_skip_data = '0' then
	--			assert axis_pix_tdata = hit_data.data(word_index)
	--				report "data out mismatch:" &
	--				" index:" & natural'image(word_index) &
	--				" word:" & to_hstring(axis_pix_tdata) &
	--				" exp:"  & to_hstring(hit_data.data(word_index));
	--		end if;

	--		word_index := word_index + 1;
	--		if axis_pix_tlast = '1' then
	--			word_index := 0;
	--		end if;

	--		wait for Tclk;
	--	end loop;

	--	wait;
	--end process;


end Behavioral;
