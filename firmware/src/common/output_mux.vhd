library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.types_pkg.all;

library xpm;
use xpm.vcomponents.all;

entity output_mux is
	generic(
		n_staves : natural := 1
	);
	port(
		clk : in std_logic;
		rst : in std_logic;
		trig_mask_ev : in std_logic;
		trig_mask  : in std_logic_vector(4 downto 0);
		trig_count : in std_logic_vector(31 downto 0);
		timestamp  : in std_logic_vector(31 downto 0);
		stave_mask : in std_logic_vector(n_staves-1 downto 0);
		out_mux_en : in std_logic;
		dec_skip_data : in std_logic;
		-- stave buffers
		data_in       : in slv8_arr(0 to n_staves-1);
		data_in_valid : in std_logic_vector(n_staves-1 downto 0);
		data_in_last  : in std_logic_vector(n_staves-1 downto 0);
		din_rd  : out std_logic_vector(n_staves-1 downto 0);
		-- output data
		data_out  : out slv32;
		dout_DV   : out std_logic;
		dout_last : out std_logic;
		dout_ack  : in std_logic;
		-- pix data to MCU
		axis_pix_tdata  : out slv32;
		axis_pix_tlast  : out std_logic;
		axis_pix_tready : in std_logic;
		axis_pix_tvalid : out std_logic
	);
end output_mux;


architecture Behavioral of output_mux is

	signal sync_rst, sync_out_mux_en, sync_dec_skip_data : std_logic;
	signal sync_stave_mask, stave_mask_reg : std_logic_vector(n_staves-1 downto 0);
	signal trig_mask_stave : std_logic_vector(n_staves-1 downto 0);

	-- packaging FSM
	type FSM_state is (idle, read_event, next_stave, ins_stave_header, add_padding, add_header,
		delay);
	signal state : FSM_state := idle;
	attribute fsm_safe_state : string;
	attribute fsm_safe_state of state : signal is "auto_safe_state";

	signal stave, first_stave : natural range 0 to n_staves := 0;
	signal EoE, buff_full : std_logic := '0';
	signal calibration_run : std_logic := '0';

	-- package shape and type
	constant hdr_size : integer := 3;
	signal hdr_cnt : integer range 0 to hdr_size := 0;
	signal crc_in, crcsum : std_logic_vector(31 downto 0);
	signal crc_rst : std_logic;


	-- packaging FSM output signals
	signal alp_data : slv8;
	signal alp_data_valid, alp_data_valid_cnv, alp_data_last : std_logic := '0';
	signal byte_counter : unsigned(15 downto 0) := (others => '0');

	-- Event buffer
	type buff_out_state_enum is (snd_header, snd_data);
	signal buff_out_state : buff_out_state_enum := snd_header;
	attribute fsm_safe_state of buff_out_state : signal is "auto_safe_state";

	signal event_buff_in, event_buff_dout: std_logic_vector(31 downto 0);
	signal event_buff_s_tlast, event_buff_m_tlast : std_logic;
	signal event_buff_wr, event_buff_rd, event_buff_m_valid, event_buff_s_ready : std_logic := '0';

	signal s_hdrff_tvalid, s_hdrff_tready, s_hdrff_tlast : std_logic;
	signal m_hdrff_tvalid, m_hdrff_tready, m_hdrff_tlast : std_logic;
	signal s_hdrff_tdata, m_hdrff_tdata : std_logic_vector(31 downto 0);

	-- data decoder
	signal dec_ready, dec_wr, dec_last : std_logic;

	-- trig id fifo
	signal trig_ff_full, trig_ff_wr : std_logic;
	signal trig_ff_empty, trig_ff_rd : std_logic;
	signal trig_ff_in, trig_ff_out : std_logic_vector(68 downto 0);

	alias  trig_ff_timestap : std_logic_vector(31 downto 0)
		is trig_ff_out(31 downto 0);
	alias  trig_ff_trigcount : std_logic_vector(31 downto 0)
		is trig_ff_out(63 downto 32);
	alias  trig_ff_trigmask : std_logic_vector(4 downto 0)
		is trig_ff_out(68 downto 64);


	component trig_fifo
		port(
			clk : in std_logic;
			srst : in std_logic;
			din : in std_logic_vector(68 downto 0);
			wr_en : in std_logic;
			rd_en : in std_logic;
			dout : out std_logic_vector(68 downto 0);
			full : out std_logic;
			empty : out std_logic;
			sbiterr : out std_logic;
			dbiterr : out std_logic
		);
	end component;

	component event_ff
		port(
			s_axis_aresetn : in std_logic;
			s_axis_aclk : in std_logic;
			s_axis_tvalid : in std_logic;
			s_axis_tready : out std_logic;
			s_axis_tdata : in std_logic_vector(31 downto 0);
			s_axis_tlast : in std_logic;
			m_axis_tvalid : out std_logic;
			m_axis_tready : in std_logic;
			m_axis_tdata : out std_logic_vector(31 downto 0);
			m_axis_tlast : out std_logic
		);
	end component;

	component header_ff
		port(
			s_axis_aresetn : in std_logic;
			s_axis_aclk : in std_logic;
			s_axis_tvalid : in std_logic;
			s_axis_tready : out std_logic;
			s_axis_tdata : in std_logic_vector(31 downto 0);
			s_axis_tlast : in std_logic;
			m_axis_tvalid : out std_logic;
			m_axis_tready : in std_logic;
			m_axis_tdata : out std_logic_vector(31 downto 0);
			m_axis_tlast : out std_logic
		);
	end component;

	--attribute mark_debug : string;
	--attribute mark_debug of state : signal is "true";
	--attribute mark_debug of data_in_valid, data_in_last : signal is "true";
	--attribute mark_debug of buff_out_state : signal is "true";
	--attribute mark_debug of buff_full : signal is "true";
	--attribute mark_debug of stave, first_stave : signal is "true";
	--attribute mark_debug of EoE : signal is "true";
	--attribute mark_debug of byte_counter : signal is "true";
	--attribute mark_debug of alp_data : signal is "true";
	--attribute mark_debug of alp_data_valid : signal is "true";
	--attribute mark_debug of s_hdrff_tvalid, s_hdrff_tready : signal is "true";
	--attribute mark_debug of s_hdrff_tdata : signal is "true";
	--attribute mark_debug of event_buff_m_tlast, m_hdrff_tlast : signal is "true";
	--attribute mark_debug of out_mux_en : signal is "true";
	--attribute mark_debug of data_out, dout_DV, dout_ack, dout_last : signal is "true";
	--attribute mark_debug of event_buff_in, event_buff_wr, event_buff_s_ready,
	--	event_buff_s_tlast : signal is "true";
	--attribute mark_debug of sync_stave_mask, stave_mask_reg, trig_mask_stave : signal is "true";
	--attribute mark_debug of trig_ff_wr, trig_mask : signal is "true";
	--attribute mark_debug of trig_ff_out, trig_ff_rd : signal is "true";
	--attribute mark_debug of trig_ff_empty : signal is "true";

begin

	ctrl_cdc : xpm_cdc_array_single
		generic map (
			DEST_SYNC_FF => 2,
			INIT_SYNC_FF => 1,
			SRC_INPUT_REG => 0,
			WIDTH => 3
		)
		port map (
			dest_out(0) => sync_rst,
			dest_out(1) => sync_dec_skip_data,
			dest_out(2) => sync_out_mux_en,
			dest_clk => clk,
			src_clk => '0',
			src_in(0) => rst,
			src_in(1) => dec_skip_data,
			src_in(2) => out_mux_en
		);

	stavemask_cdc : xpm_cdc_array_single
		generic map (
			DEST_SYNC_FF => 2,
			INIT_SYNC_FF => 1,
			SRC_INPUT_REG => 0,
			WIDTH => n_staves
		)
		port map (
			dest_out => sync_stave_mask,
			dest_clk => clk,
			src_clk  => '0',
			src_in   => stave_mask
		);

	trig_id_fifo : trig_fifo
		port map(
			clk   => clk,
			srst  => sync_rst,
			din   => trig_ff_in,
			wr_en => trig_ff_wr,
			rd_en => trig_ff_rd,
			dout  => trig_ff_out,
			full  => trig_ff_full,
			empty => trig_ff_empty,
			sbiterr => open,
			dbiterr => open
		);


	-- trigger and stave mask handling
	process(all)
	begin

		trig_ff_in <= (trig_mask & trig_count & timestamp);
		trig_ff_wr <= trig_mask_ev;

		calibration_run <= (or sync_stave_mask) and (or data_in_valid);

		for n in 0 to n_staves-1 loop
			if (or sync_stave_mask) = '1' then
				trig_mask_stave(n) <= sync_stave_mask(n);
			else
				trig_mask_stave(n) <= (not trig_ff_trigmask(n/3));
			end if;
		end loop;

		-- first unmasked stave
		first_stave <= 0;
		for n in n_staves-1 downto 0 loop
			if trig_mask_stave(n) = '0' then
				first_stave <= n;
			end if;
		end loop;

		-- End of event
		--EoE <= and stave_mask_reg(n_staves-1 downto stave+1);
		EoE <= '0';
		for n in 0 to n_staves-1 loop
			if stave = n then
				EoE <= and stave_mask_reg(n_staves-1 downto n+1);
			end if;
		end loop;

	end process;


	--------------------------------------------------------------------------------------
	-- Two-processes packaging FSM: its role is to read form the front end data buffers
	-- in the correct order and generate an output packet streamed to "alp_data"
	--------------------------------------------------------------------------------------


	process(clk)
	begin
		if rising_edge(clk)  then

			crc_rst <= '0';
			trig_ff_rd <= '0';

			case state is

				when idle =>
					stave <= 0;
					hdr_cnt <= 0;
					crc_rst <= '1';
					stave_mask_reg <= trig_mask_stave;

					-- start when the trig fifo contains data or calibration run enabled
					if trig_ff_empty = '0' or calibration_run = '1' then

						-- just add some padding for events with 0 turrets
						if trig_mask_stave = "111111111111111" then
							state <= add_padding;
						else
							state <= ins_stave_header;
							stave <= first_stave;
						end if;

					end if;

				when read_event =>
					if data_in_last(stave) = '1' and data_in_valid(stave) = '1' and buff_full = '0' then
						if EoE = '0' then
							state <= next_stave;
						elsif sync_out_mux_en = '1' then
							state <= idle;
						elsif byte_counter(1 downto 0) = "11" then
							state <= add_header;
						else
							state <= add_padding;
						end if;
					end if;

				when next_stave =>
					if stave_mask_reg(stave+1) = '0' then
						state <= ins_stave_header;
					end if;
					stave <= stave + 1;

				when ins_stave_header =>
					if buff_full = '0' then
						state <= read_event;
					end if;

				when add_padding =>
					if byte_counter(1 downto 0) = "11" then
						state <= add_header;
					end if;

				when add_header =>
					if s_hdrff_tready = '1' then
						hdr_cnt <= hdr_cnt + 1;
						if hdr_cnt = hdr_size then
							state <= delay;
							trig_ff_rd <= '1';
						end if;
					end if;

			when delay =>
				state <= idle;

			end case;

			if sync_rst = '1' then
				state <= idle;
			end if;

		end if;
	end process;


	process(all)
	begin
		din_rd <= (others => '0');
		alp_data <= data_in(stave);
		alp_data_valid <= '0';
		alp_data_last <= '0';
		s_hdrff_tvalid <= '0';
		s_hdrff_tdata <= (others => '0');
		s_hdrff_tlast <= '0';

		case state is

			when idle => null;

			when read_event =>
				din_rd(stave) <= data_in_valid(stave) and (not buff_full);
				alp_data_valid <= data_in_valid(stave);
				if byte_counter(1 downto 0) = "11" or sync_out_mux_en = '1' then
					alp_data_last <= EoE and data_in_last(stave);
				end if;

			when next_stave => null;

			when ins_stave_header =>
				alp_data_valid <= '1';
				alp_data <= x"f" & uint2slv(stave, 4);

			when add_padding =>
				alp_data_valid <= '1';
				alp_data <= x"80";
				if byte_counter(1 downto 0) = "11" then
					alp_data_last <= '1';
				end if;

			when add_header =>
				s_hdrff_tvalid <= '1';
				s_hdrff_tlast <= '0';

				case hdr_cnt is
					-- header_id & bytecount
					when 0 => s_hdrff_tdata <= x"ffff" & std_logic_vector(byte_counter(15 downto 0));
					-- CRC32
					when 1 => s_hdrff_tdata <= crcsum;
					-- trigger timestamp
					when 2 => s_hdrff_tdata <= trig_ff_timestap;
					-- trigcnt
					when 3 =>	s_hdrff_tdata <= trig_ff_trigcount;
				end case;

				if hdr_cnt = hdr_size then
					s_hdrff_tlast <= '1';
				end if;

			when delay => null;

		end case;
	end process;


	bytecntr: process(clk)
	begin
		if rising_edge(clk) then
			if sync_rst = '1' or state = idle then
				byte_counter <= (others => '0');
			elsif alp_data_valid = '1' and buff_full = '0' then
				byte_counter <= byte_counter + 1;
			end if;
		end if;
	end process;


	-- data width converter for event FIFO input
	FF_width_cnv: process(clk) is
	begin
		if rising_edge(clk) then

			event_buff_wr <= '0';

			-- hadle alpide data (8->16 conversion)
			if alp_data_valid_cnv = '1' then
				event_buff_in <= alp_data & event_buff_in(31 downto 8);

				if byte_counter(1 downto 0) = "11" then
					event_buff_wr <= '1';
					event_buff_s_tlast <= alp_data_last;
				end if;

			end if;

		end if;
	end process;

	--------------------------------------------------------------------------------------
	-- Event and header fifo with read logic
	--------------------------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			case buff_out_state is

				when snd_header =>
					if (dout_ack and m_hdrff_tvalid and m_hdrff_tlast) = '1' then
						buff_out_state <= snd_data;
					end if;

				when snd_data =>
					if (dout_ack and event_buff_m_valid and event_buff_m_tlast) = '1' then
						buff_out_state <= snd_header;
					end if;

			end case;

			if sync_rst = '1' then
				buff_out_state <= snd_header;
			end if;

		end if;
	end process;

	process(all)
	begin
		case buff_out_state is

			when snd_header =>
				data_out <= m_hdrff_tdata;
				dout_DV <= m_hdrff_tvalid;
				dout_last <= '0';
				event_buff_rd <= '0';
				m_hdrff_tready <= dout_ack;

			when snd_data =>
				data_out <= event_buff_dout;
				dout_DV <= event_buff_m_valid;
				dout_last <= event_buff_m_tlast;
				event_buff_rd <= dout_ack;
				m_hdrff_tready <= '0';

		end case;
	end process;

	event_ff_inst: event_ff
		port map(
			s_axis_aresetn => not sync_rst,
			s_axis_aclk    => clk,
			s_axis_tvalid  => event_buff_wr,
			s_axis_tready  => event_buff_s_ready,
			s_axis_tdata   => event_buff_in,
			s_axis_tlast   => event_buff_s_tlast,
			m_axis_tvalid  => event_buff_m_valid,
			m_axis_tready  => event_buff_rd,
			m_axis_tdata   => event_buff_dout,
			m_axis_tlast   => event_buff_m_tlast
		);

	header_ff_inst: header_ff
		port map(
			s_axis_aresetn => not sync_rst,
			s_axis_aclk    => clk,
			s_axis_tvalid  => s_hdrff_tvalid,
			s_axis_tready  => s_hdrff_tready,
			s_axis_tdata   => s_hdrff_tdata,
			s_axis_tlast   => s_hdrff_tlast,
			m_axis_tvalid  => m_hdrff_tvalid,
			m_axis_tready  => m_hdrff_tready,
			m_axis_tdata   => m_hdrff_tdata,
			m_axis_tlast   => m_hdrff_tlast
		);


	--------------------------------------------------------------------------------------
	-- Multiplex data output from packaging FSM to MCU FIFO or to event FIFO.
	-- Mux to MCU is explicitly enabled with the "sync_out_mux_en" control reg flag.
	--------------------------------------------------------------------------------------

	data_out_mux: process(all)
	begin
		-- Multiplex packager output and its control signals between the output FIFO buffer
		-- and data decoder writing on the MCU.
		if sync_out_mux_en = '1' then
			alp_data_valid_cnv <= '0';
			dec_wr <= alp_data_valid;
			dec_last <= alp_data_last;
			buff_full <= not dec_ready;
		else
			alp_data_valid_cnv <= alp_data_valid;
			dec_wr <= '0';
			dec_last <= '0';
			buff_full <= not event_buff_s_ready;
		end if;

	end process;


	data_decoder_inst: entity work.data_decoder
		port map(
			clk => clk,
			rst => sync_rst,
			word_in     => alp_data,
			word_in_dv  => dec_wr,
			word_in_lst => dec_last,
			ready       => dec_ready,
			axis_pix_tdata  => axis_pix_tdata,
			axis_pix_tlast  => axis_pix_tlast,
			axis_pix_tready => axis_pix_tready,
			axis_pix_tvalid => axis_pix_tvalid,
			dec_skip_data => sync_dec_skip_data
	);


	crc_impl: entity work.ucrc_par
		generic map(
		POLYNOMIAL => x"04C11DB7",
		INIT_VALUE => x"FFFFFFFF",
		FINAL_XOR  => x"FFFFFFFF",
		DATA_WIDTH => 32,
		SYNC_RESET => 1
		)
		port map(
			clk_i   => clk,
			rst_i   => crc_rst,
			clken_i => event_buff_wr and event_buff_s_ready,
			data_i 	=> crc_in,
			match_o => open,
			crc_o 	=> crcsum
		);

		crc_rf: process(all)
		begin
			for i in 0 to 7 loop
				crc_in(i)    <= event_buff_in(7-i);
				crc_in(8+i)  <= event_buff_in(15-i);
				crc_in(16+i) <= event_buff_in(23-i);
				crc_in(24+i) <= event_buff_in(31-i);
			end loop;
		end process;

end architecture;
