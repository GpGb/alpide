library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.spwpkg.all;

library UNISIM;
use UNISIM.VComponents.all;


entity spw_axis_wrapper is
	generic (
		sysfreq  : real := 50.0e6;
		debug : boolean := false
	);
	port(
		clk : in std_logic;
		rst : in std_logic;
		rst_daq : in std_logic;
		-- spw pins
		spw_di : in  std_logic;
		spw_si : in  std_logic;
		spw_do : out std_logic;
		spw_so : out std_logic;
		-- gpio spw
		event_ready : out std_logic;
		event_clear : in std_logic;
		-- data AXIS interface
		pkgr_data  : in std_logic_vector(31 downto 0);
		pkgr_valid : in std_logic;
		pkgr_ready : out std_logic;
		pkgr_last  : in std_logic;
		-- MCU dpram iface
		mcu_bram_we   : out std_logic_vector(3 downto 0);
		mcu_bram_addr : out std_logic_vector(31 downto 0);
		mcu_bram_di   : out std_logic_vector(31 downto 0);
		mcu_bram_do   : in std_logic_vector(31 downto 0);
		-- cmd
		daq_run  : out std_logic;
		cmd_intr : out std_logic;
		rst_reg  : out std_logic_vector(31 downto 0);
		-- TSP switches
		TSP_EN_A     : out std_logic_vector(14 downto 0);
		TSP_EN_D     : out std_logic_vector(14 downto 0);
		TSP_EN_BIAS  : out std_logic_vector(14 downto 0);
		TSP_pwr_good : in std_logic_vector(14 downto 0);
		-- other regs
		busy         : in std_logic;
		SEM_flags    : in std_logic_vector(3 downto 0);
		trig_count   : in std_logic_vector(31 downto 0);
		busy_viol_cnt: in std_logic_vector(15 downto 0);
		timestamp    : in std_logic_vector(31 downto 0);
		cpu_alarms   : in std_logic_vector(15 downto 0);
		trigger_lut  : out std_logic_vector(31 downto 0)
	);
end spw_axis_wrapper;


architecture wrap_bhv of spw_axis_wrapper is

	constant cmd_addr : std_logic_vector := x"0000_2000";

	-- spw signals
	signal txrdy, txflag, txwrite : std_logic;
	signal rxvalid, rxflag, rxread : std_logic;
	signal rxdata, txdata  : std_logic_vector(7 downto 0);

	signal started, connecting, running : std_logic;
	signal errdisc, errpar, erresc, errcred : std_logic;
	signal spw_err_counter : unsigned(15 downto 0) := (others => '0');

	-- spw controller
	signal s_data_in, s_data_out : std_logic_vector(31 downto 0);
	signal s_addr : std_logic_vector(31 downto 0);
	signal s_we, s_write_done, s_busy : std_logic;

	signal regfile_data_in, regfile_data_out : std_logic_vector(31 downto 0);
	signal regfile_addr : std_logic_vector(31 downto 0);
	signal regfile_we, regfile_write_done : std_logic;

	signal dpram_we : std_logic;
	signal dpram_addr, dpram_di, dpram_do  : std_logic_vector(31 downto 0);

	constant g_spw_data_width : natural := 32;

	component spw_controller
		generic(
			g_spw_addr_width  : integer := 16;
			g_spw_data_width  : integer := 32;
			g_spw_addr_offset : unsigned := x"0000";
			g_spw_num         : integer := 32;
			g_spw_idx         : unsigned(7 downto 0) := x"00"
		);
		port (
			i_spw_clk     : in  std_logic;
			i_reset       : in  std_logic;
			--regfile inte
			i_data_in     : in  std_logic_vector(g_spw_data_width - 1 downto 0);
			o_data_out    : out std_logic_vector(g_spw_data_width - 1 downto 0);
			o_we          : out std_logic;
			o_addr        : out std_logic_vector(g_spw_data_width - 1 downto 0);
			i_write_done  : in  std_logic;
			o_busy        : out std_logic;
			--SPW interfac
			i_txrdy       : in  std_logic;
			i_rxvalid     : in  std_logic;
			i_rxflag      : in  std_logic;
			i_rxdata      : in  std_logic_vector(7 downto 0);
			o_rxread      : out std_logic;
			o_txwrite     : out std_logic;
			o_txflag      : out std_logic;
			o_txdata      : out std_logic_vector(7 downto 0)
		);
	end component spw_controller;


	signal spw_data_in, vio_data_in : std_logic_vector(31 downto 0);
	signal spw_addr, vio_addr : std_logic_vector(31 downto 0);
	signal vio_we : std_logic_vector(1 downto 0);
	signal spw_we, spw_busy : std_logic;
	signal vio_override : std_logic;

	--attribute mark_debug : string;
	--attribute mark_debug of cmd_intr : signal is "true";
	--attribute mark_debug of s_data_in, s_data_out, s_addr, s_we : signal is "true";
	--attribute mark_debug of s_write_done, s_busy, s_burst_count : signal is "true";
	--attribute mark_debug of rxdata, rxvalid, rxflag : signal is "true";
	--attribute mark_debug of txdata, txwrite, txrdy, txflag : signal is "true";

begin

	register_file_unit : entity work.spw_regfile
	generic map(
		g_spw_addr_width  => 20,   -- spw address width generic parameter
		g_spw_data_width  => 32,   -- spw data width generic parameter
		g_spw_addr_offset => x"00000",
		g_spw_num         => 32    -- spw number generic parameter
	)
	port map (
		i_spw_clk         => clk,
		i_rf_clk          => clk,
		i_reset           => rst,
		-- controller interface
		i_data_in         => regfile_data_in,
		o_data_out        => regfile_data_out,
		i_we              => regfile_we,
		i_addr            => regfile_addr,
		o_write_done      => regfile_write_done,
		i_busy            => s_busy,
		-- FPGA interface
		daq_run           => daq_run,
		rst_reg           => rst_reg,
		TSP_EN_A          => TSP_EN_A,
		TSP_EN_D          => TSP_EN_D,
		TSP_EN_BIAS       => TSP_EN_BIAS,
		TSP_pwr_good      => TSP_pwr_good,
		busy              => busy,
		gpio              => event_ready & event_clear,
		SEM_flags         => SEM_flags,
		spw_err_cnt       => std_logic_vector(spw_err_counter),
		trigger_count     => trig_count,
		busy_viol_cnt     => busy_viol_cnt,
		timestamp         => timestamp,
		cpu_alarms        => cpu_alarms,
		trigger_lut       => trigger_lut
	);

	spw_controller_inste: spw_controller
	generic map (
		g_spw_addr_width   => 20,    -- spw address width generic parameter
		g_spw_data_width   => 32,    -- spw data width generic parameter
		g_spw_addr_offset  => x"0000",
		g_spw_num          => 32,    -- spw number generic parameter
		g_spw_idx          => x"00"  -- unique ID index generic parameter
	)
	port map (
		i_spw_clk          => clk,
		i_reset            => rst,
		--regfile interface
		i_data_in          => s_data_out,
		o_data_out         => spw_data_in,
		o_we               => spw_we,
		o_addr             => spw_addr,
		i_write_done       => s_write_done,
		o_busy             => spw_busy,
		--SPW interface
		i_txrdy            => txrdy,
		i_rxvalid          => rxvalid,
		i_rxflag           => rxflag,
		i_rxdata           => rxdata,
		o_rxread           => rxread,
		o_txwrite          => txwrite,
		o_txflag           => txflag,
		o_txdata           => txdata
	);


	spwstream_inst: entity work.spwstream
	generic map (
		sysfreq         => sysfreq,
		txclkfreq       => sysfreq,
		rximpl          => impl_generic,
		rxchunk         => 1,
		tximpl          => impl_generic,
		rxfifosize_bits => 6,
		txfifosize_bits => 6
	)
	port map (
		clk         => clk,
		rxclk       => clk,
		txclk       => clk,
		rst         => rst,
		autostart   => '0',
		linkstart   => '1',
		linkdis     => '0',
		txdivcnt    => x"03",
		tick_in     => '0',
		ctrl_in     => (others => '0'),
		time_in     => (others => '0'),
		txwrite     => txwrite,
		txflag      => txflag,
		txdata      => txdata,
		txrdy       => txrdy,
		txhalff     => open,
		tick_out    => open,
		ctrl_out    => open,
		time_out    => open,
		rxvalid     => rxvalid,
		rxhalff     => open,
		rxflag      => rxflag,
		rxdata      => rxdata,
		rxread      => rxread,
		started     => started,
		connecting  => connecting,
		running     => running,
		errdisc     => errdisc,
		errpar      => errpar,
		erresc      => erresc,
		errcred     => errcred,
		spw_di      => spw_di,
		spw_si      => spw_si,
		spw_do      => spw_do,
		spw_so      => spw_so
	);


	dpram_mngr_inst: entity work.dpram_mngr
	port map(
		clk       => clk,
		rst       => rst_daq,
		-- data input stream
		rx_tdata  => pkgr_data,
		rx_tvalid => pkgr_valid,
		rx_tready => pkgr_ready,
		rx_tlast  => pkgr_last,
		-- regfile read/write interface
		we        => dpram_we,
		addr      => dpram_addr,
		di        => dpram_di,
		do        => dpram_do,
		-- oob signals
		data_ready => event_ready,
		data_clear => event_clear
	);


	addrmux: process(all)
	begin

		dpram_we <= '0';
		dpram_di <= (others => '0');
		dpram_addr <= (others => '0');

		regfile_we <= '0';
		regfile_data_in <= (others => '0');
		regfile_addr <= (others => '0');

		mcu_bram_we <= (others => '0');
		mcu_bram_di <= (others => '0');
		mcu_bram_addr <= (others => '0');

		-- The cpu bram use byte-addressing, the others word-addressing
		if (s_addr < x"0000_1000") then

			regfile_we <= s_we;
			regfile_data_in <= s_data_in;
			regfile_addr <= s_addr;
			s_data_out <= regfile_data_out;
			s_write_done <= regfile_write_done;

		elsif (s_addr < x"0000_2000") then

			dpram_we <= s_we;
			dpram_di <= s_data_in;
			dpram_addr <= 21x"00" & s_addr(10 downto 0);
			s_data_out <= dpram_do;
			s_write_done <= s_we;

		else

			mcu_bram_we <= (others => s_we);
			mcu_bram_di <= s_data_in;
			mcu_bram_addr <= 19x"00" & s_addr(10 downto 0) & "00";
			s_data_out <= mcu_bram_do;
			s_write_done <= s_we;

		end if;


	end process addrmux;


	cmd_intr_gen: process(clk)
	begin

		if rising_edge(clk) then

			cmd_intr <= '0';

			if s_we = '1' and s_addr = cmd_addr then
				cmd_intr <= '1';
			end if;

		end if;

	end process;


	err_counter_inst: process(clk)
	begin
		if rising_edge(clk) then
			if (errpar or erresc or errcred) = '1' then
				spw_err_counter <= spw_err_counter + 1;
			end if;
		end if;
	end process;



Debug_logic: if debug generate

	-- Debug
	vio_spw_inst : entity work.vio_spw
		port map(
			clk => clk,
			probe_in0 => s_data_out,
			probe_out0 => vio_data_in,
			probe_out1 => vio_addr,
			probe_out2 => vio_we(0),
			probe_out3 => vio_override
		);


	process(clk)
	begin

		if rising_edge(clk) then

			vio_we(1) <= vio_we(0);

			if vio_override = '1' then
				s_data_in <= vio_data_in;
				s_addr    <= vio_addr;
				s_we      <= vio_we(0) and (not vio_we(1));
				s_busy    <= '1';
			else
				s_data_in <= spw_data_in;
				s_addr    <= spw_addr;
				s_we      <= spw_we;
				s_busy    <= spw_busy;
			end if;

		end if;

	end process;

else generate

	s_data_in <= spw_data_in;
	s_addr    <= spw_addr;
	s_we      <= spw_we;
	s_busy    <= spw_busy;
	vio_data_in  <= (others => 'X');
	vio_addr     <= (others => 'X');
	vio_we       <= (others => 'X');
	vio_override <= 'X';

end generate Debug_logic;


end wrap_bhv;
