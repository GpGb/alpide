library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types_pkg.all;

entity spacewire_axis_wrapper_tb is
end spacewire_axis_wrapper_tb;

architecture behavior of spacewire_axis_wrapper_tb is

	-- clock & constants
	constant clk_period : time := 16.667 ns;
	signal clk : std_logic := '1';
	signal FT_CLK : std_logic := '0';

	constant test_data_len : natural := 10;

	constant spw_write_cmd : std_logic_vector(7 downto 0) := x"0c";
	constant spw_read_cmd  : std_logic_vector(7 downto 0) := x"03";
	constant spw_bread_cmd : std_logic_vector(7 downto 0) := x"ee";

	constant data_dpram_baseaddr : std_logic_vector(31 downto 0) := x"0000_1000";

	signal FT_DATA : std_logic_vector(7 downto 0) := (others => '0');
	signal FT_RXF_N, FT_TXE_N : std_logic := '1';
	signal FT_RD_N, FT_WR_N, FT_OE_N : std_logic;

	-- uut pixel data in inface
	signal rst : std_logic;
	signal pkgr_valid, pkgr_ready, pkgr_last : std_logic;
	signal pkgr_data : std_logic_vector(31 downto 0);

	-- test signals
	signal spwpkt_rcv_data : std_logic_vector(31 downto 0) := (others => '0');
	signal read_addr_ptr : unsigned(31 downto 0) := (others => '0');

begin

	uut: entity work.spw_axis_wrapper
	PORT MAP (
		clk => clk,
		rst => rst,
		rst_daq => rst,
		-- spw pins
		FT_CLK   => FT_CLK,
		FT_DATA  => FT_DATA,
		FT_RXF_N => FT_RXF_N,
		FT_TXE_N => FT_TXE_N,
		FT_OE_N  => FT_OE_N,
		FT_RD_N  => FT_RD_N,
		FT_WR_N  => FT_WR_N,
		-- gpio spw
		event_ready => open,
		event_clear => '1',
		-- data AXIS interface
		pkgr_data  => pkgr_data,
		pkgr_valid => pkgr_valid,
		pkgr_ready => pkgr_ready,
		pkgr_last  => pkgr_last,
		-- MCU dpram iface
		mcu_bram_we   => open,
		mcu_bram_addr => open,
		mcu_bram_di   => open,
		mcu_bram_do   => x"fafa1b1b",
		-- cmd
		daq_run  => open,
		cmd_intr => open,
		rst_reg  => open,
		-- TSP switches
		TSP_EN_A     =>	open,
		TSP_EN_D     =>	open,
		TSP_EN_BIAS  =>	open,
		TSP_pwr_good =>	(others => '0'),
		SEM_flags    => (others => '0')
	);

	clk <= not clk after clk_period/2;
	FT_CLK <= not FT_CLK after clk_period/2;


	-- generates spacewire commands to uut
	spwr_master: process

		-- send a spacewire command (10 bytes)
		procedure send_packet(
			cmd     : in std_logic_vector(7 downto 0);
			address : in std_logic_vector(31 downto 0);
			data    : in std_logic_vector(31 downto 0)
		) is
		begin

			FT_RXF_N <= '0';
			FT_TXE_N <= '1';

			wait until FT_RD_N = '0';

			-- send cmd
			FT_DATA <= cmd;
			wait for clk_period;

			-- send address
			for n in 0 to 3 loop
				FT_DATA <= address(7+8*n downto 8*n);
				wait for clk_period;
			end loop;

			-- send data
			for n in 0 to 3 loop
				FT_DATA  <= data(7+8*n downto 8*n);
				wait for clk_period;
			end loop;

			-- EOP
			FT_DATA  <= (others => '0');
			wait for clk_period;

			-- stop write
			FT_RXF_N <= '1';
			wait for clk_period;

		end procedure send_packet;


		-- read a spacewire packet (10 bytes) from uut
		procedure rcv_packet(
			exp_cmd : in std_logic_vector(7 downto 0);
			address : out std_logic_vector(31 downto 0);
			data    : out std_logic_vector(31 downto 0)
		) is
		begin

			FT_RXF_N <= '1';
			FT_TXE_N <= '0';
			FT_DATA <= (others => 'Z');
			wait until FT_WR_N = '0' and FT_CLK = '0' for clk_period*60;
			assert FT_WR_N = '0' report "WR_N not asserted" severity failure;

			-- read cmd
			assert FT_DATA = exp_cmd report "rcv wrong cmd, rcv: " &
				to_hstring(FT_DATA) & " exp: " & to_hstring(exp_cmd);
			wait for clk_period;

			-- read address
			for n in 0 to 3 loop
				assert FT_WR_N = '0' report "WR_N not asserted" severity failure;

				address(7+8*n downto 8*n) := FT_DATA;
				wait for clk_period;

			end loop;

			-- read data
			for n in 0 to 3 loop
				assert FT_WR_N = '0' report "WR_N not asserted" severity failure;

				data(7+8*n downto 8*n) := FT_DATA;
				wait for clk_period;

			end loop;

			-- read EOP
			assert FT_WR_N = '0' report "WR_N not asserted" severity failure;
			assert FT_DATA = x"00" report "rcv wrong EOP" severity failure;
			wait for clk_period;

			FT_TXE_N <= '1';

		end procedure rcv_packet;


		-- write a single register
		procedure write_register (
			address : in std_logic_vector(31 downto 0);
			data    : in std_logic_vector(31 downto 0)
		) is
		begin

			send_packet(spw_write_cmd, address, data);

		end procedure write_register;


		-- read a single register
		procedure read_register (
				address : in std_logic_vector(31 downto 0)
			) is
			variable rcv_addr : std_logic_vector(31 downto 0);
			variable rcv_data : std_logic_vector(31 downto 0);
		begin

			send_packet(spw_read_cmd, address, x"00000000");
			rcv_packet(spw_read_cmd, rcv_addr, rcv_data);

			assert rcv_addr = address report "rcv wrong address from read" severity failure;
			spwpkt_rcv_data <= rcv_data;
			wait for clk_period;

		end procedure read_register;


		-- burst read of a range of registers
		procedure read_block (
				address : in std_logic_vector(31 downto 0);
				payload : in std_logic_vector(31 downto 0)
			) is
			variable rcv_data  : std_logic_vector(31 downto 0);
			variable addr_ptr  : std_logic_vector(31 downto 0);
		begin

			-- write 'sandbox' register with check data
			write_register(x"0000_0005", x"deadbeef");

			addr_ptr := address;

			-- send readblock command
			send_packet(spw_bread_cmd, address, payload);

			-- read response packets
			for i in 0 to slv2uint(payload) loop

				rcv_packet(spw_bread_cmd, addr_ptr, rcv_data);
				addr_ptr := std_logic_vector(unsigned(addr_ptr) + 1);
				spwpkt_rcv_data <= rcv_data;

			end loop;

			-- read check data to make sure readblock is not sending
			-- any unexpected packets
			read_register(x"0000_0005");
			assert spwpkt_rcv_data = x"deadbeef" report "unexpected data after readblock"
				severity failure;

		end procedure read_block;


	begin

		wait for clk_period*100;

		-- read and write "sandbox" register
		write_register(x"0000_0005", x"deadbeef");
		wait for clk_period*50;
		read_register(x"0000_0005");
		assert spwpkt_rcv_data = x"deadbeef" report "fail reg r/w test 1" severity failure;

		wait for clk_period*50;
		read_register(x"0000_2001");
		assert spwpkt_rcv_data /= x"00000000" report "fail reg r/w test 2" severity failure;
		wait for clk_period*50;


		-- poll for data in the dpram
		read_register(data_dpram_baseaddr);
		while (spwpkt_rcv_data = x"0000_0000") loop
			read_register(data_dpram_baseaddr);
		end loop;
		assert spwpkt_rcv_data = uint2slv(test_data_len, spwpkt_rcv_data'length)
			report "unexpected data length" severity failure;

		-- read the data
		read_addr_ptr <= unsigned(data_dpram_baseaddr);
		wait for clk_period;

		for i in 0 to test_data_len loop
			read_addr_ptr <= read_addr_ptr + x"1";
			read_register(std_logic_vector(read_addr_ptr));
		end loop;

		wait for clk_period*50;


		-- test readblock
		read_block(data_dpram_baseaddr, uint2slv(test_data_len, 32));
		wait for clk_period*50;

		-- clear dpram
		write_register(data_dpram_baseaddr, x"0000_0000");

		wait;
	end process;



	datagen: process
	begin

		pkgr_valid <= '0';
		pkgr_last  <= '0';

		-- reset cycle
		rst <= '0';
		wait for clk_period*16;
		rst <= '1';
		wait for clk_period*16;
		rst <= '0';
		wait for clk_period*16;

		assert pkgr_ready = '1' report "pkgr data input not ready" severity failure;

		pkgr_valid <= '1';

		for i in 1 to test_data_len loop

			pkgr_data <= uint2slv(i, pkgr_data'length);
			assert pkgr_ready = '1' report "pkgr not ready";

			if i = test_data_len then
				pkgr_last <= '1';
			end if;

			wait for clk_period;
		end loop;
		pkgr_valid <= '0';

		wait;
	end process;


end;
