#include <string.h>
#include <sleep.h>
#include "platform.h"
#include "xparameters.h"

#include "alpide.h"
#include "spw_dpram.h"
#include "FIFO.h"
#include "ffs_io.h"

#define ALP_CMD_REGS_BASEADDR    XPAR_AXI_CTRL_BASEADDR
#define ALP_CMD_REGS_HIGHADDR    XPAR_AXI_CTRL_HIGHADDR

#define HOTSCAN_BUFFSIZE DATA_BUFF_MAX_LEN/4

struct hit_s{
	uint32_t hit_data;
	uint16_t counter;
};

uint16_t chipmask[15] = {0};

/******************************* Low level interface ***********************************/

volatile uint32_t *ctrl_regs = (volatile uint32_t *) ALP_CMD_REGS_BASEADDR;

extern int write_alp_reg(uint16_t chip_id, uint16_t addr, uint16_t data);
extern int read_alp_reg(uint16_t chip_id, uint16_t addr, uint16_t* res);

// res contains chip response if any (ie. with opcode ALPIDE_RDOP)
int send_alpide_cmd(uint8_t op_code, uint16_t chip_id, uint16_t addr, uint16_t data, uint16_t* res){

	// wait stave is ready
	wait_nBusy(chip_id);

	// send data
	uint16_t stave_sel = (chip_id == 0xf0f) ? 0xffff :  1<<(chip_id>>8);
	ctrl_regs[ALP_CMD_PAYLOAD_REG_OFF] = (addr<<16) | data;
	ctrl_regs[ALP_CMD_COMMAND_REG_OFF] = (stave_sel<<16) | (op_code<<8) | (chip_id & 0xff);

	// wait for answer, nromally ready after 100 clk's from request (2.5 us)
	volatile uint32_t res_reg=0;
	int tries = 0;
	for (tries=0; tries<5; tries++){
		res_reg = ctrl_regs[ALP_CMD_RESPONSE_REG_OFF];
		if (res_reg&0xfe0000)
			break;
		usleep(2.5);
	}

	// timeout
	if (tries == 5)
		return -2;

	// firmware reported an error
	if ( (res_reg>>16)&0x1 )
		return -1;

	// save any chip answer
	if(res)
		*res = res_reg&0xffff;

	return 0;
}


int write_ctrl_reg(uint16_t offset, uint32_t value){
	if (offset >= ALP_CMD_REGS_RO_BASEADDR)
		return -1;

	ctrl_regs[offset] = value;
	return 0;
}


int read_ctrl_reg(uint32_t offset, uint32_t *value){

	if (offset+ALP_CMD_REGS_BASEADDR > ALP_CMD_REGS_HIGHADDR)
		return -1;

	*value = ctrl_regs[offset];
	return 0;
}


int wait_nBusy(uint16_t chip_id){

	uint32_t mask = 0;
	uint8_t stave_id = (chip_id>>8);

	if (stave_id == 0xf)
		mask = 0xff;
	else
		mask = (1<<stave_id);

	// will always become true (monitored by watchdogs)
	while((ctrl_regs[ALP_BUSY_REG_OFF] & mask) != 0);

	return 0;
}


void write_alarm_reg(uint8_t type, uint16_t data){

	uint16_t shift = 0, mask = 0;

	switch (type){
		case ALARM_TYPE_TEMP:
			shift = 0;
			mask = 0x3;
			break;
		case ALARM_TYPE_MASK:
			shift = 2;
			mask = (0x1f<<shift);
			break;
		case ALARM_TYPE_BOOT:
			shift = 7;
			mask = (0x3f<<shift);
			break;
		default:
			dbg_print("Warn: invalid alarm type\n\r");
			return;
	}

	uint32_t alarm = 0;
	read_ctrl_reg(ALP_CMD_ALARM_REG_OFF, &alarm);

	// update selected field
	data = (data << shift);
	alarm ^= (data ^ alarm) & mask;

	write_ctrl_reg(ALP_CMD_ALARM_REG_OFF, alarm);
}

/******************************** Alpide procedures *********************************/

int chipid_to_index(uint16_t chip_id){
	uint8_t stave_id = chip_id>>8;
	uint8_t l_chip_id = chip_id&0xf;

	int chip_index = stave_id*10;
	if (l_chip_id <= 4)
		chip_index += l_chip_id;
	else
		chip_index += l_chip_id-3;

	return chip_index;
}


uint16_t index_to_chipid(uint16_t index){
	uint8_t stave_id = index/10;

	uint8_t chip_id_lsb =  index%10;
	if (chip_id_lsb > 4)
		chip_id_lsb += 3;

	uint16_t chip_id = 0x70 | (stave_id<<8) | chip_id_lsb;
	return chip_id;
}


int set_reset_pixregs(uint16_t chip_id, char reg, uint8_t s){
	uint16_t pixreg_val = (s&1)<<1;
	if (reg == 'm')
		pixreg_val |= 0;
	else if (reg == 'p')
		pixreg_val |= 1;
	else
		return -1;

	write_alp_reg(chip_id, 0x487, 0x0000);
	write_alp_reg(chip_id, ALPIDE_REG_PIXCFG, pixreg_val);
	write_alp_reg(chip_id, 0x487, 0xffff);
	write_alp_reg(chip_id, 0x487, 0x0000);

	return 0;
}


uint16_t column_pixreg_addr(uint16_t x){
	return 0x0400 | (x&0x3e0)<<6 | (1 + (x>>4&0x1));
}
uint16_t row_pixreg_addr(uint16_t x){
	return 0x0404 | (x<<7);
}

int mask_pixels(){

	spiffs_file fd = ffs_open("hpx", SPIFFS_O_RDONLY, 0);
	if (fd < 0){
		dbg_print("fail to open hpx file\n\r");
		return ffs_errno();
	}

	while (!ffs_eof(fd)){

		uint32_t word = 0;
		int32_t len = ffs_read(fd, &word, sizeof(uint32_t));
		if (len < sizeof(uint32_t)){
			dbg_print("hpx file read returned bad len\n\r");
			return ffs_errno();
		}

		uint16_t short_id = (word>>20) & 0x3ff;
		uint16_t chip_id = 0x70 | (short_id&0xf) | (short_id&0xf0)<<4;
		uint16_t y = (word>>10) & 0x3ff;
		uint16_t x = word & 0x3ff;
		//dbg_print("raw: %x. msk %x, %d,%d\n\r", word, chip_id, x, y);

		uint16_t pix_reg_addr, pix_reg_val, old_pixreg_val = 0;

		write_alp_reg(chip_id, ALPIDE_REG_PIXCFG, 0x2);

		// select and write column pixreg
		pix_reg_addr = column_pixreg_addr(x);
		pix_reg_val  = 1<<(x&0xf);

		read_alp_reg(chip_id, pix_reg_addr, &old_pixreg_val);
		pix_reg_val |= old_pixreg_val;
		write_alp_reg(chip_id, pix_reg_addr, pix_reg_val);

		// select and write row pixreg
		pix_reg_addr = 0x404 | (y/16)<<11;
		pix_reg_val  = 1<<(y%16);

		read_alp_reg(chip_id, pix_reg_addr, &old_pixreg_val);
		pix_reg_val |= old_pixreg_val;
		write_alp_reg(chip_id, pix_reg_addr, pix_reg_val);

		// clear selection
		write_alp_reg(chip_id, 0x487, 0);

	}

	ffs_close(fd);
	return 0;
}


int save_mask_list(struct hit_s hit_list[], uint8_t thresh){

	spiffs_file fd = ffs_open("hpx", SPIFFS_CREAT | SPIFFS_RDWR | SPIFFS_DIRECT, 0);

	for(int n=0; n<HOTSCAN_BUFFSIZE; n++){

		if (hit_list[n].counter == 0){
			break;
		}

		if (hit_list[n].counter < thresh){
			hit_list[n].hit_data = 0;
			hit_list[n].counter = 0;
			continue;
		}

		// check if already masked (i.e. pixel is unmaskable)
		int unmaskable = 0;
		ffs_lseek(fd, 0, SPIFFS_SEEK_SET);

		while (!ffs_eof(fd)){
			uint32_t value = 0;
			int32_t len = ffs_read(fd, &value, sizeof(value));
			if (len<4) break;

			if ((value&0x7fffffff) == hit_list[n].hit_data){
				unmaskable = 1;
				// already marked as unmaskable, skip..
				if (value>>31) break;

				// mark as unmaskable
				value = (value | (1<<31) );
				ffs_lseek(fd, -sizeof(value), SPIFFS_SEEK_CUR);
				ffs_write(fd, &value, sizeof(value));
				break;
			}
		}
		ffs_lseek(fd, 0, SPIFFS_SEEK_END);

		// if not unmaskable save new pixel
		if (!unmaskable) {
			uint32_t hot_pix_data = hit_list[n].hit_data;
			ffs_write(fd, &hot_pix_data, sizeof(hot_pix_data));
			hit_list[n].hit_data = 0;
			hit_list[n].counter = 0;
		}

	}

	ffs_close(fd);

	return 0;
}


int update_noise_counters(struct hit_s hit_list[]){

	if (read_pix_fifo() < 0)
		return -1;

	if (RX_packet_buffer.count == 0)
		return 0;

	// iterate on received hits and update hit_s struct array
	for(uint32_t index=0; index < RX_packet_buffer.count-4; index += 4){

		uint32_t hit;
		memcpy(&hit, &RX_packet_buffer.data[index], 4);

		for(int n=0; n<HOTSCAN_BUFFSIZE; n++){

			if (hit_list[n].hit_data == hit){
				hit_list[n].counter++;
				break;
			}

			if(hit_list[n].counter == 0){
				hit_list[n].hit_data = hit;
				hit_list[n].counter = 1;
				break;
			}

		}

	}

	return 0;
}


int scan_noisy_pixels(uint8_t stave_id, uint8_t thresh, uint32_t n_trigger){

	uint8_t hot_thresh = 10;
	if (thresh != 0) hot_thresh = thresh;
	uint32_t triggers = 1000;
	if (n_trigger != 0) triggers = n_trigger;

	// enable output mux
	uint32_t stave_mask = (1<<16);

	// set stave mask
	stave_mask |= ~(1 << stave_id) & 0xffff;

	uint32_t old_rdoutmux_reg = ctrl_regs[ALP_CMD_RDOUTMUX_REG_OFF];
	write_ctrl_reg(ALP_CMD_RDOUTMUX_REG_OFF, stave_mask);


	// discovery procedure
	struct hit_s hit_list[HOTSCAN_BUFFSIZE] = {{.hit_data = 0}};
	uint16_t stave_brdcst_addr = (stave_id<<8) | 0x0f;

	for(uint32_t i=0; i<triggers; ++i){

		send_alpide_cmd(ALPIDE_TRIG, stave_brdcst_addr, 0, 0, NULL);
		wait_nBusy(stave_id<<8);

		int ret = update_noise_counters(hit_list);

		usleep(500);
		BRAM_COMMAND_PROGRESS(i+1);
	}

	// save and apply the mask
	int ret = save_mask_list(hit_list, hot_thresh);
	mask_pixels();

	write_ctrl_reg(ALP_CMD_RDOUTMUX_REG_OFF, old_rdoutmux_reg);

	return 0;
}



int configure_stave(uint8_t stave_id, uint8_t file){

	const int conf_line_len = 3 * sizeof(uint16_t);
	const uint32_t wait_time = 1000;
	const uint16_t stave_brdcst_addr = (stave_id<<8) | 0xf;
	int retcode = 0;

	// important: disable clock gating during configuration
	write_ctrl_reg(0x2, 1<<stave_id);

	// Global and pixel matrix reset
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_CMD, ALPIDE_GRST);
	usleep(wait_time);
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_CMD, ALPIDE_PRST);
	usleep(wait_time);

	// load default OB tracker config
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_CMD, ALPIDE_LOADOBDEFCFG);

	// set mode and disable machester encoding
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_MODECTRL, 0x3cd);
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_CMUDMU, 0x76);

	// load default conf suggested by ALICE
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_FROMU1, 0x70);
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_FROMU2, 0x50);
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_FROMU_PULSE1, 0x14);
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_FROMU_PULSE2, 0x1f4);
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_VRESETD, 0x93);
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_IDB, 0x1d);

	// read configuration from flash and realy to chips
	spiffs_file fd = ffs_open(index_to_fname(file), SPIFFS_O_RDONLY, 0);
	if (fd < 0){
		retcode = -1;
		goto conf_finalize;
	}

	// write conf data
	while (!ffs_eof(fd)){

		uint16_t conf_data[3];

		int32_t len = ffs_read(fd, conf_data, conf_line_len);
		if (len != conf_line_len){
			retcode =  -1;
			goto conf_finalize;
		}

		uint16_t cid = conf_data[0];

		if ((cid>>8) == 0xf || (cid>>8) == stave_id){
			cid = (cid&0xff) | (stave_id<<8);
			dbg_print("cfgwr: %x %x %x\n\r", cid, conf_data[1], conf_data[2]);
			write_alp_reg(cid, conf_data[1], conf_data[2]);
		}

	}

conf_finalize:
	ffs_close(fd);

	// reset pixel registers
	set_reset_pixregs(stave_brdcst_addr, 'p', 0);
	set_reset_pixregs(stave_brdcst_addr, 'm', 0);

	// readout reset
	write_alp_reg(stave_brdcst_addr, ALPIDE_REG_CMD, ALPIDE_RORST);
	usleep(wait_time);

	// enable clock gating
	write_ctrl_reg(0x2, 0x0);

	return retcode;
}


int update_chipmask(){

	uint16_t rd_chipmask;
	uint8_t alarm_mask = 0;

	for(uint8_t stave = 0; stave<15; stave++){
		int ret = send_alpide_cmd(FSM_GET_MSK, (stave<<8), 0, 0, &rd_chipmask);

		if (ret) return ret;

		if (rd_chipmask != chipmask[stave]){
			dbg_print("chipmask change, %d: %x to %x\n\r", stave, chipmask[stave], rd_chipmask);
			alarm_mask |= (1<<(stave/3));
		}

		chipmask[stave] = rd_chipmask;
	}

	if (alarm_mask != 0)
		write_alarm_reg(ALARM_TYPE_MASK, alarm_mask);

	return 0;
}


int set_chipmask(uint8_t stave_id, uint16_t mask){
	if (stave_id >= 0xf)
		return -1;

	mask &= 0x3ff;
	int ret = send_alpide_cmd(FSM_SET_MSK, (stave_id<<8), 0, mask, 0);
	chipmask[stave_id] = mask;

	return ret;
}


uint16_t get_chipmask(uint8_t stave_id){
	return chipmask[stave_id];
}


int run_chip_scan(){
	uint16_t reg_value;
	int res;

	for(uint8_t stave_id=0; stave_id<15; stave_id++){

		// reset stave chipmask
		chipmask[stave_id] = 0;

		// scan chips in stave
		for(uint8_t index=0; index<10; index++){

			uint16_t chip_id = index_to_chipid(index+stave_id*10);
			res = read_alp_reg(chip_id, 0x1, &reg_value);

			if (res){
				chipmask[stave_id] |= (1<<index);
			}
		}

		// apply chipmask
		set_chipmask(stave_id, chipmask[stave_id]);
	}

	// output mask
	bram_write_scratch(chipmask, sizeof(chipmask));
	bram_write_done();

	return 0;
}


//int alp_fifo_selftest(uint16_t chip_id){
//
//	uint16_t recv_data;
//
//	write_ctrl_reg(0x2, 0xff);
//
//	write_alp_reg(chip_id, ALPIDE_REG_CMD, ALPIDE_FIFOTEST);
//	usleep(1000000);
//
//	uint16_t addr;
//	int error = 0;
//	for(int region=0; region<32; region++){
//		addr = region<<11 | 0x301;
//		read_alp_reg(chip_id, addr, &recv_data);
//		if (recv_data&0x2){
//			error++;
//		}
//	}
//
//	write_ctrl_reg(0x2, 0x0);
//
//	return error;
//}


int dpram_readback_test(uint16_t chip_id, uint16_t pattern){

	// set alpide configuration mode
	uint16_t old_modectrl;
	read_alp_reg(chip_id, ALPIDE_REG_MODECTRL, &old_modectrl);
	write_alp_reg(chip_id, ALPIDE_REG_MODECTRL, old_modectrl&0xfffc);

	uint16_t addr_lsb, addr_msb;

	//write pattern
	for(int region=0; region<32; region++){
		for(int reg=0; reg<=0x7f; reg++){
			addr_lsb = region<<11 | reg | 0x100;
			addr_msb = region<<11 | reg | 0x200;
			write_alp_reg(chip_id, addr_lsb, pattern);
			write_alp_reg(chip_id, addr_msb, pattern);
		}
	}

	//read pattern
	uint32_t errors = 0;
	for(int region=0; region<32; region++){
		for(int reg=0; reg<=0x7f; reg++){
			addr_lsb = region<<11 | reg | 0x100;
			addr_msb = region<<11 | reg | 0x200;
			uint16_t read_lsb, read_msb;
			read_alp_reg(chip_id, addr_lsb, &read_lsb);
			read_alp_reg(chip_id, addr_msb, &read_msb);

			// check (msb is truncated..)
			if ( read_lsb != pattern || read_msb != (pattern&0xff) )
				errors++;
		}
	}

	// restore modectrl
	write_alp_reg(chip_id, ALPIDE_REG_MODECTRL, old_modectrl);

	return errors;
}


int run_dpram_test(uint16_t chip_id){

	if ((chip_id&0xf) == 0xf || (chip_id>>8 == 0xf))
		return -1;

	uint32_t errcount = 0;
	errcount += dpram_readback_test(chip_id, 0x0000);
	errcount += dpram_readback_test(chip_id, 0xffff);
	errcount += dpram_readback_test(chip_id, 0xaaaa);
	errcount += dpram_readback_test(chip_id, 0x5555);

	return errcount;
}


int dump_chip_regs(uint16_t chip_id){

	uint16_t reg_value;

	for (uint16_t n=1; n<=0x1b; n++){
		read_alp_reg(chip_id, n, &reg_value);
		bram_write_scratch(&reg_value, sizeof(reg_value));
	}

	for (uint16_t n=0x600; n<=0x610; n++){
		read_alp_reg(chip_id, n, &reg_value);
		bram_write_scratch(&reg_value, sizeof(reg_value));
	}

	bram_write_done();

	return 0;
}

//int adc_calibrate(uint16_t chip_id){
//
//	uint16_t ADCmode = 0x281; // cal mode, nominal settings, AVSS input
//	uint16_t value1, value2 = 0;
//
//	write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, ADCmode);
//
//	// ****** find discr sign ******
//	write_alp_reg(chip_id, ALPIDE_REG_CMD, ALPIDE_ADCMEASURE);
//	usleep(5000);
//	send_alpide_cmd(ALPIDE_RDOP, chip_id, ALPIDE_REG_ADC_CAL, 0, &value1);
//
//	ADCmode |= (1<<8); // set discr sign to 1
//	write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, ADCmode);
//	write_alp_reg(chip_id, ALPIDE_REG_CMD, ALPIDE_ADCMEASURE);
//	usleep(5000);
//	send_alpide_cmd(ALPIDE_RDOP, chip_id, ALPIDE_REG_ADC_CAL, 0, &value2);
//
//	if (value1 > value2){
//		ADCmode &= ~(1<<8);
//		write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, ADCmode);
//	}
//
//	// ****** find halfbittrim ******
//	write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, ADCmode);
//	write_alp_reg(chip_id, ALPIDE_REG_CMD, ALPIDE_ADCMEASURE);
//	usleep(5000);
//	send_alpide_cmd(ALPIDE_RDOP, chip_id, ALPIDE_REG_ADC_CAL, 0, &value1);
//
//	ADCmode |= (1<<11); // set half bit trim to 1
//	write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, ADCmode);
//	write_alp_reg(chip_id, ALPIDE_REG_CMD, ALPIDE_ADCMEASURE);
//	usleep(5000);
//	send_alpide_cmd(ALPIDE_RDOP, chip_id, ALPIDE_REG_ADC_CAL, 0, &value2);
//	if (value1 > value2){
//		ADCmode &= ~(1<<11);
//		write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, ADCmode);
//	}
//
//	// ***** measure offset ******
//	write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, ADCmode);
//	write_alp_reg(chip_id, ALPIDE_REG_CMD, ALPIDE_ADCMEASURE);
//	usleep(5000);
//	//send_alpide_cmd(ALPIDE_RDOP, chip_id, ALPIDE_REG_ADC_CAL, 0, &value1);
//
//
//	// ****** find m=dac/adc ******
//	//ADCmode &= ~1;  // manual mode
//	//ADCmode |= (0x5<<2); // adc input DACMONV
//	//write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, ADCmode);
//	//write_alp_reg(chip_id, ALPIDE_REG_MON_OVR, 0x408); // vmon to VTEMP
//
//	// measure VTEMP for two dac levels
//	//write_alp_reg(chip_id, ALPIDE_REG_VTEMP, 0x50);
//	//usleep(5000);
//	//write_alp_reg(chip_id, ALPIDE_REG_CMD, ALPIDE_ADCMEASURE);
//	//usleep(5000);
//	//send_alpide_cmd(ALPIDE_RDOP, chip_id, ALPIDE_REG_ADC_AVSS, 0, &value1);
//	//write_alp_reg(chip_id, ALPIDE_REG_VTEMP, 0xc0);
//	//usleep(5000);
//	//write_alp_reg(chip_id, ALPIDE_REG_CMD, ALPIDE_ADCMEASURE);
//	//usleep(5000);
//	//send_alpide_cmd(ALPIDE_RDOP, chip_id, ALPIDE_REG_ADC_AVSS, 0, &value2);
//	//float m = (float)(value2-value1)/(float)(0xc0-0x50);
//
//	write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, 0x420);
//
//	return 0;
//}


int measure_alp_temp(uint8_t stave_id){

	if (stave_id == 0xf)
		return -1;

	// diable clock gating
	write_ctrl_reg(0x2, (1<<stave_id));

	uint16_t chip_id = (stave_id<<8) | 0xf;

	// configure ADC and start measure
	write_alp_reg(chip_id, ALPIDE_REG_ADC_CTRL, 0x420);
	write_alp_reg(chip_id, ALPIDE_REG_CMD, 0xff20);
	usleep(5000);

	// read temp
	uint16_t start = stave_id*10;

	for (int i=start; i<start+10; i++){
		chip_id = index_to_chipid(i);

		uint16_t temp_val = 0;
		read_alp_reg(chip_id, ALPIDE_REG_ADC_AVSS, &temp_val);
		bram_write_scratch(&temp_val, sizeof(temp_val));
	}

	// enable clock gating
	write_ctrl_reg(0x2, 0);

	bram_write_done();

	return 0;
}
