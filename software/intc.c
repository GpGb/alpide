#include "xparameters.h"
#include "xstatus.h"
#include "xintc.h"
#include "xil_exception.h"
#include "xtmrctr.h"
#include "xwdttb.h"

#include "platform.h"
#include "spw_dpram.h"
#include "intc.h"


#define INTC_DEVICE_ID		XPAR_INTC_0_DEVICE_ID
#define CMD_INTR_ID       XPAR_MICROBLAZE_0_AXI_INTC_CMD_INTR_CDC_DEST_PULSE_INTR
#define TMR_INTR_ID       XPAR_MICROBLAZE_0_AXI_INTC_AXI_TIMER_GP_0_INTERRUPT_INTR
#define WDT_INTR_ID       XPAR_MICROBLAZE_0_AXI_INTC_AXI_TIMEBASE_WDT_0_WDT_INTERRUPT_INTR

#define TIMER_RESET_VALUE 40e6*10 // 10 secs at 40 MHz

XIntc InterruptController;
XTmrCtr TimerCounter;
XWdtTb WatchdogTimer;

// extern'd interrupt flags
volatile uint8_t command_rcv_intr = 0;
volatile uint8_t temp_timer_intr = 0;
volatile uint8_t mainloop_flag_intr = 0;

// expire counter for interrupt
volatile uint32_t expire_cnt  = 0;
volatile uint32_t comm_last_status = 0;


void command_intr_handler(void *CallbackRef){

	// if idle set flag and return
	if (command_rcv_intr == 0){
		command_rcv_intr = 1;
	}
	else {
		if (Xil_In8(DPRAM_BASEADDR) == 0xab){
			command_rcv_intr = 2;
			dbg_print("recv abort command...\n\r");
		}

	}

}


void timer_intr_handler(void *callbackref, u8 timer_num){
	temp_timer_intr = 1;
}


void wdt_intr_handler(void *callbackref){

	// check mainloop running
	if (mainloop_flag_intr == 0){
		expire_cnt = 0;
		XWdtTb_RestartWdt(&WatchdogTimer);
		mainloop_flag_intr = 1;
		return;
	}


	// check command or temperature measure progress
	if (temp_timer_intr == 1 || command_rcv_intr == 1){

		// check if a command is already running
		if (command_rcv_intr == 1){

			// check progress spinning
			if (comm_last_status != BRAM_COMMAND_GETPROGR()){
				XWdtTb_RestartWdt(&WatchdogTimer);
				comm_last_status = BRAM_COMMAND_GETPROGR();
				return
			}

		}

		// check if temperature measure running
		if (command_rcv_intr == 1){
			expire_cnt++;

			if (expire_cnt < 3){
				XWdtTb_RestartWdt(&WatchdogTimer);
			}

			return;
		}

	}


}


int init_gp_timer(){

	int status = 0;

	status = XTmrCtr_Initialize(&TimerCounter, XPAR_AXI_TIMER_GP_0_DEVICE_ID);
	if (status != XST_SUCCESS)
		return XST_FAILURE;

	status = XTmrCtr_SelfTest(&TimerCounter, 0);
	if (status != XST_SUCCESS)
		return XST_FAILURE;

	XTmrCtr_SetOptions(&TimerCounter, 0,
				XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION | XTC_DOWN_COUNT_OPTION);

	XTmrCtr_SetResetValue(&TimerCounter, 0, TIMER_RESET_VALUE);

	XTmrCtr_SetHandler(&TimerCounter, timer_intr_handler, NULL);

	return XST_SUCCESS;
}


int init_intc(){

	int status;

	status = init_gp_timer();
	if (status != XST_SUCCESS)
			return -1;


	// Init interrupt controller
	status = XIntc_Initialize(&InterruptController, INTC_DEVICE_ID);
	if (status != XST_SUCCESS)
		return -1;


	status = XIntc_SelfTest(&InterruptController);
	if (status != XST_SUCCESS)
		return -1;


	status = XIntc_Connect(&InterruptController, CMD_INTR_ID,
			(XInterruptHandler)command_intr_handler,
			(void *)0);
	if (status != XST_SUCCESS)
		return -2;


	status = XIntc_Connect(&InterruptController, TMR_INTR_ID,
				(XInterruptHandler)XTmrCtr_InterruptHandler,
				(void *)&TimerCounter);
	if (status != XST_SUCCESS)
		return -2;


	status = XIntc_Connect(&InterruptController, WDT_INTR_ID,
				(XInterruptHandler)wdt_intr_handler,
				(void *)0);
	if (status != XST_SUCCESS)
		return -2;


	status = XIntc_Start(&InterruptController, XIN_REAL_MODE);
	if (status != XST_SUCCESS)
		return -2;

	XIntc_Enable(&InterruptController, CMD_INTR_ID);
	XIntc_Enable(&InterruptController, TMR_INTR_ID);
	XIntc_Enable(&InterruptController, WDT_INTR_ID);

	// setup exceptions
	Xil_ExceptionInit();

	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler)XIntc_InterruptHandler,
			&InterruptController);

	// see hw_exception_handler.S (label ex_handler_sp_violation)
	//Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_STACK_VIOLATION,
	//		(Xil_ExceptionHandler)stack_violation_handler,
	//		NULL);

	Xil_ExceptionEnable();


	// Start timer
	XTmrCtr_Start(&TimerCounter, 0);

	return 0;
}


int start_stop_wdtmr(uint8_t set){

	if (set == INTC_WDT_SET_START){
		XWdtTb_Start(&WatchdogTimer);
		dbg_print("WDT started\n\r");
	}
	else if (set == INTC_WDT_SET_STOP) {
		XWdtTb_Stop(&WatchdogTimer);
		dbg_print("WDT stopped\n\r");
	}
	else{
		dbg_print("WDT invalid mode\n\r");
		return -1;
	}

	return 0;
}


int init_start_wdtmr(){

	XWdtTb_Config *Config;

	Config = XWdtTb_LookupConfig(XPAR_WDTTB_0_DEVICE_ID);
	if (Config == NULL)
		return -1;

	int status = 0;

	status = XWdtTb_CfgInitialize(&WatchdogTimer, Config,	Config->BaseAddr);
	if (status != XST_SUCCESS)
		return -1;


	int retcode = 0;
	status = XWdtTb_IsWdtExpired(&WatchdogTimer);
	if (status){
		dbg_print("WDT restart detected\n\r");
		retcode = 1;
	}

	status = XWdtTb_SelfTest(&WatchdogTimer);
	if (status != XST_SUCCESS)
		return -1;

	start_stop_wdtmr(INTC_WDT_SET_START);

	return retcode;
}
