#include <stdint.h>
#include <sleep.h>
#include <xil_io.h>

#include "platform.h"
#include "xparameters.h"
#include "mb_interface.h"
#include "intc.h"
#include "spw_dpram.h"
#include "FIFO.h"
#include "qspi_flash.h"
#include "ffs_io.h"
#include "alpide.h"
#include "alpide_calibration.h"
#include "onewire/temp_sensor.h"

#if PLATFORM_TYPE == __NEXYS__
	#include "power_man.h"
#endif
//#include "xil_testmem.h"


enum DAQ_command_code{
	echo         = 0x01,
	chip_scan    = 0x02,
	load_conf    = 0x03,
	run_pm_scan  = 0x04,
	fwd_alp_cmd  = 0x05,
	wr_register  = 0x06,
	rd_register  = 0x15,
	set_pixmask  = 0x07,
	hotpix_scan  = 0x08,
	fs_wr_file   = 0x09,
	fs_dump_file = 0x10,
	flash_format = 0x11,
	flash_check  = 0x22,
	//list_files   = 0x12,
	fifotest     = 0x13,
	thresh_cal   = 0x14,
	save_DAC_val = 0xa4,
	//pulse_stave  = 0x0a,
	dump_alp_regs = 0x0b,
	read_alp_temp = 0x0c,
	set_T_mask    = 0x0d,
	wr_chipmask   = 0x0e,
	rd_chipmask   = 0x0f,
#if PLATFORM_TYPE == __NEXYS__
	set_tsp_switch = 0x17,
	tsp_poweroff   = 0x18,
	tsp_poweron    = 0x19,
	iic_dump_regs  = 0x1a,
#endif
	memtest      = 0x1d,
	abortcmd     = 0xab
};


uint32_t arg_ptr = 0;


int mem_test(){

#if PLATFORM_TYPE == __TDAQ__

	uint32_t cnt = 0;
	uint32_t *memp = (uint32_t *) XPAR_AXI_EMC_0_S_AXI_MEM0_BASEADDR;
	const uint32_t size = (XPAR_EMC_0_S_AXI_MEM0_HIGHADDR & 0xFFFFFF);

	for (uint32_t i=0; i<size/4; i++){
		memp[i] = i+1;
	}

	for (uint32_t i=0; i<size/4; i++){
		if (memp[i] != i+1)
			cnt++;
	}

	dbg_print("Errcnt: %d\r\n", cnt);


	//XStatus status;

	//tatus = Xil_TestMem32((u32*)XPAR_AXI_EMC_0_S_AXI_MEM0_BASEADDR, size/4,
	//		0xAAAA5555, XIL_TESTMEM_ALLMEMTESTS);
	//print("32-bit test: ");
	//print(status == XST_SUCCESS? "PASSED!":"FAILED!"); print("\n\r");

	//status = Xil_TestMem16((u16*)XPAR_AXI_EMC_0_S_AXI_MEM0_BASEADDR, size/2,
	//		0xAA55, XIL_TESTMEM_ALLMEMTESTS);
	//print("16-bit test: ");
	//print(status == XST_SUCCESS? "PASSED!":"FAILED!"); print("\n\r");

	//status = Xil_TestMem8((u8*)XPAR_AXI_EMC_0_S_AXI_MEM0_BASEADDR, size,
	//		0xA5,	XIL_TESTMEM_ALLMEMTESTS);
	//print("8-bit test: ");
	//print(status == XST_SUCCESS? "PASSED!":"FAILED!"); print("\n\r");

#endif

	return 0;
}


void platform_initialize(){

	int status = 0;
	uint8_t retcode = 0;

	status = init_intc();
	dbg_print("intc: %d\n", status);
	if (status){
		retcode |= 1;
	}

	status = init_fifo();
	dbg_print("fifo: %d\n", status);
	if (status){
		retcode |= 2;
	}

	status = init_flash_subsystem();
	dbg_print("qspi: %d\n", status);
	if (status){
		retcode |= 4;
	}

	status = mount_spiffs();
	dbg_print("spiffs: %d\n", status);
	if (status){
		retcode |= 8;
	}

	status = init_start_wdtmr();
	dbg_print("wtd: %d\n", status);
	if (status < 0){
		retcode |= 16;
	}
	else if (status > 0){
		retcode |= 32;
	}

#if PLATFORM_TYPE == __NEXYS__
	pm_init_power_system();
#endif

	write_alarm_reg(ALARM_TYPE_BOOT, retcode);

	return;
}


void get_arg(void *dst, uint32_t size){

	for(uint32_t n=0; n<size; n++) {
		((uint8_t *)dst)[n] = Xil_In8(DPRAM_BASEADDR + arg_ptr + n);
	}

	arg_ptr += size;
}

int d_echo(){

	uint32_t var = 0xffffffff;

	for(uint32_t i=0; i<100; i++){
		bram_write_scratch(&var, sizeof(var));
		var--;
	}

	bram_write_done();

	return 0;
}

int d_run_pm_scan(){

	uint8_t stave_id;
	uint8_t mode;
	get_arg(&mode, sizeof(mode));
	get_arg(&stave_id, sizeof(stave_id));

	run_stave_pm_scan(stave_id, mode);
	return 0;
}

int d_load_conf(){

	uint8_t stave_id;
	uint8_t file;
	get_arg(&stave_id, sizeof(stave_id));
	get_arg(&file, sizeof(file));

	return configure_stave(stave_id, file);
}

//int d_pulse_stave(){
//	uint8_t staveid, pulses;
//	get_arg(&staveid, sizeof(staveid));
//	get_arg(&pulses, sizeof(pulses));
//
//	uint32_t stave_mask = 0;
//	stave_mask |= ~(1 << staveid) & 0xffff;
//	write_ctrl_reg(ALP_CMD_RDOUTMUX_REG_OFF, stave_mask);
//
//	uint16_t brcast_id = (staveid<<8) | 0xf;
//	write_alp_reg(brcast_id, ALPIDE_REG_FROMU1, 0x50);
//
//	for(int n=0; n<pulses; n++){
//		send_alpide_cmd(0x78, brcast_id, 0, 0, NULL);
//	}
//
//	usleep(100);
//	wait_nBusy(staveid<<8);
//	write_ctrl_reg(ALP_CMD_RDOUTMUX_REG_OFF, 0);
//
//	return 0;
//}

int d_fwd_alp_cmd(){

	uint8_t  op_code;
	uint16_t chip_id;
	uint16_t addr;
	uint16_t data;
	get_arg(&op_code, sizeof(op_code));
	get_arg(&chip_id, sizeof(chip_id));
	get_arg(&addr, sizeof(addr));
	get_arg(&data, sizeof(data));

	uint16_t res;
	int ret = send_alpide_cmd(op_code, chip_id, addr, data, &res);

	dbg_print("op:%x id:%x addr:%x data:%x, res:%x\r\n", op_code, chip_id, addr, data, res);

	if (ret)
		return -1;

	bram_write_scratch(&res, sizeof(res));
	bram_write_done();
	return 0;

}

int d_wr_register(){

	uint32_t offset;
	uint32_t value;
	get_arg(&offset, sizeof(offset));
	get_arg(&value, sizeof(value));

	int ret = write_ctrl_reg(offset, value);

	return ret;
}

int d_rd_register(){

	uint16_t offset;
	get_arg(&offset, sizeof(offset));

	uint32_t resp;
	int ret = read_ctrl_reg(offset, &resp);

	if (ret<0)
		return -1;

	bram_write_scratch(&resp, sizeof(resp));
	bram_write_done();
	return 0;
}

int d_hotpix_scan(){

	uint8_t stave_id;
	uint8_t thresh;
	uint32_t n_trigger;
	get_arg(&stave_id, sizeof(stave_id));
	get_arg(&thresh, sizeof(thresh));
	get_arg(&n_trigger, sizeof(n_trigger));

	scan_noisy_pixels(stave_id, thresh, n_trigger);

	return 0;
}

int d_fs_wr_file(){

	uint8_t  index;
	uint16_t len;
	uint8_t  mode;
	get_arg(&index, sizeof(index));
	get_arg(&len, sizeof(len));
	get_arg(&mode, sizeof(mode));

	void* file_data_src = (volatile void *) (DPRAM_BASEADDR + arg_ptr);
	int ret = ffs_write_file(index, file_data_src, len, mode);

	return ret;
}

int d_fs_dump_file(){

	uint8_t index;
	uint32_t offset;
	get_arg(&index, sizeof(index));
	get_arg(&offset, sizeof(offset));

	int ret = ffs_dump_file(index, offset);
	return ret;
}

int d_thresh_cal(){
	uint8_t stave_id;
	uint16_t vcasn0;
	uint8_t target;
	get_arg(&stave_id, sizeof(stave_id));
	get_arg(&vcasn0, sizeof(vcasn0));
	get_arg(&target, sizeof(target));

	int ret = run_threshold_cal(stave_id, vcasn0, target);
	return ret;
}

int d_save_DAC_val(){
	uint8_t stave_id;
	uint8_t findex;
	get_arg(&stave_id, sizeof(stave_id));
	get_arg(&findex, sizeof(findex));

	int ret = save_DAC_values(stave_id, findex);
	return ret;
}

int d_fifotest(){
	uint16_t chip_id;
	get_arg(&chip_id, sizeof(chip_id));

	int ret = run_dpram_test(chip_id);
	bram_write_scratch(&ret, sizeof(ret));
	bram_write_done();

	return 0;
}

int d_dump_alp_regs(){
	uint16_t chip_id;
	get_arg(&chip_id, sizeof(chip_id));

	int ret = dump_chip_regs(chip_id);
	return ret;
}

int d_read_alp_temp(){
	uint8_t stave_id;
	get_arg(&stave_id, sizeof(stave_id));

	int ret = measure_alp_temp(stave_id);
	return ret;
}

int d_set_temp_mask(){
	uint8_t stave_id;
	uint8_t mask;
	get_arg(&stave_id, sizeof(stave_id));
	get_arg(&mask, sizeof(mask));

	int ret = set_temp_mask(stave_id, mask);
	return ret;
}

int d_get_chipmask(){
	uint8_t stave_id;
	get_arg(&stave_id, sizeof(stave_id));

	uint16_t chipmask = get_chipmask(stave_id);
	bram_write_scratch(&chipmask, sizeof(chipmask));
	bram_write_done();

	return 0;
}

int d_set_chipmask(){

	uint8_t stave_id;
	uint16_t chipmask;
	get_arg(&stave_id, sizeof(stave_id));
	get_arg(&chipmask, sizeof(chipmask));

	int ret = set_chipmask(stave_id, chipmask);
	return ret;
}


#if PLATFORM_TYPE == __NEXYS__

int d_set_tsp_switch(){
	uint8_t staveid, channel, setrst;
	get_arg(&staveid, sizeof(staveid));
	get_arg(&channel, sizeof(channel));
	get_arg(&setrst, sizeof(setrst));

	int ret = pm_tsp_switch_set(staveid, channel, setrst);
	return ret;
}

#endif


int decode_command(){

	enum DAQ_command_code command = Xil_In8(DPRAM_BASEADDR);
	arg_ptr = 1; // skip 8-bit command field

	int ret = 0;

	dbg_print("rcv command %x\n", command);

	switch (command){
		case echo:
			ret = d_echo();
			break;
		case chip_scan:
			ret = run_chip_scan();
			break;
		case load_conf:
			ret = d_load_conf();
			break;
		case run_pm_scan:
			ret = d_run_pm_scan();
			break;
		case fwd_alp_cmd:
			ret = d_fwd_alp_cmd();
			break;
		case wr_register:
			ret = d_wr_register();
			break;
		case rd_register:
			ret = d_rd_register();
			break;
		case set_pixmask:
			ret = mask_pixels();
			break;
		case hotpix_scan:
			ret = d_hotpix_scan();
			break;
		case fs_wr_file:
			ret = d_fs_wr_file();
			break;
		case fs_dump_file:
			ret = d_fs_dump_file();
			break;
		case flash_format:
			start_stop_wdtmr(INTC_WDT_SET_STOP);
			ret = ffs_format();
			start_stop_wdtmr(INTC_WDT_SET_START);
			break;
		case flash_check:
			start_stop_wdtmr(INTC_WDT_SET_STOP);
			ret = ffs_check();
			start_stop_wdtmr(INTC_WDT_SET_START);
			break;
		//case list_files:
		//	ret = ffs_list_files();
		//	break;
		case thresh_cal:
			ret = d_thresh_cal();
			break;
		case save_DAC_val:
			ret = d_save_DAC_val();
			break;
		case fifotest:
			ret = d_fifotest();
			break;
		case dump_alp_regs:
			ret = d_dump_alp_regs();
			break;
		case read_alp_temp:
			ret = d_read_alp_temp();
			break;
		case set_T_mask:
			ret = d_set_temp_mask();
			break;
		case rd_chipmask:
			ret = d_get_chipmask();
			break;
		case wr_chipmask:
			ret = d_set_chipmask();
			break;
		case memtest:
			ret = mem_test();
			break;
		case abortcmd:
			break;
#if PLATFORM_TYPE == __NEXYS__
		case tsp_poweroff:
			ret = pm_tsp_poweroff();
			break;
		case tsp_poweron:
			ret = pm_tsp_poweron();
			break;
		case iic_dump_regs:
			ret = pm_dump_iicmux_regs();
			break;
#endif
		default:
			ret = -1;
	}
	dbg_print("cmd ret: %d\n\n", ret);

	if (ret)
		BRAM_COMMAND_FAIL()
	else
		BRAM_COMMAND_OK()

	return 0;
}


int main(){

	#define STACK_SIZE  0x0001e000
	#define MAIN_OFFSET 0x70

	mtshr(mfgpr(R1));
	mtslr(mfgpr(R1)-STACK_SIZE+MAIN_OFFSET);

	dbg_print("Started..\r\n");
	platform_initialize();

	while(1){

		if (command_rcv_intr){
			decode_command();
			command_rcv_intr = 0;
		}
		mainloop_flag_intr = 0;

		if (temp_timer_intr){
			read_tracker_temp_data();
			temp_timer_intr = 0;
		}
		mainloop_flag_intr = 0;

		update_chipmask();
	}

	return 0;
}
