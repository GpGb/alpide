#!/usr/bin/env python

import os
import ctypes as cty
import numpy as np
from time import sleep

class raw_reader():

    def __init__(self, filename, timeout=10):
        self.timeout = timeout
        self.rawfile = open(filename, 'rb')

        fheader = self.rawfile.read(4)
        if (int.from_bytes(fheader, byteorder='little') != 1):
            raise Exception('Wrong filetype')

        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.decoder = cty.cdll.LoadLibrary(script_dir + '/process_raw.so')
        self.decoder.decode_raw.argtypes = [cty.c_char_p, cty.c_int, cty.c_char_p]
        #self.decoder.decode_file.argtypes = [cty.c_char_p, cty.c_int]

        self.decoder.get_last_event.argtypes = [np.ctypeslib.ndpointer(cty.c_uint16,
            flags="C_CONTIGUOUS")]


    def get_event(self):

        bufflen = 0
        tryes = 0

        # try to read header until timeout
        while bufflen == 0:
            buff = self.rawfile.read(4)
            bufflen = len(buff)

            if (bufflen == 0):
                sleep(0.5)
                tryes += 1

            if tryes >= self.timeout:
                return None

        # grab packet len from header
        assert(len(buff) == 4)
        assert(buff[2:4] == b'\xff\xff')
        pktlen = int.from_bytes(buff[0:2], byteorder='little')

        # read and docode remaining event from file
        buff += self.rawfile.read(pktlen+12)
        self.decoder.decode_raw(buff, cty.c_int(len(buff)), None)

        # get data from decoder
        MAX_HITS = 4000
        buff = np.zeros(3*MAX_HITS, dtype=np.uint16)
        hitcnt = self.decoder.get_last_event(buff)

        buff = buff.reshape(MAX_HITS, 3)
        return buff[:hitcnt]


    def __del__(self):
        self.rawfile.close()



#if __name__ == '__main__':
#    reader = raw_reader('/tmp/dout.raw')
#
#    i = 0
#    while True:
#        _ = reader.get_event()
#        i += 1
