library ieee;
use ieee.std_logic_1164.all;

entity UART_controller is
  
  generic(
    CLK_per_BAUD  : natural := 347; -- CLK/BAUD_RATE
    Rx_FIFO_DEPTH : natural := 4;
    Tx_FIFO_DEPTH : natural := 4;
    FIFO_alm_full : natural := 5 
  );
  
  port(
   CLK : in std_logic;
   -- UART hardware ports
   Rx  : in std_logic;
   Tx  : out std_logic;
   CTS : in std_logic;
   RTS : out std_logic;
   -- UART controller interface
   Data_in  : in std_logic_vector(7 downto 0);
   Data_out : out std_logic_vector(7 downto 0);
   in_DV    : in std_logic;
   out_rd   : in std_logic;
   out_DV   : out std_logic;
   busy     : out std_logic
  );

end UART_controller;


architecture controller of UART_controller is

  signal Rx_DV, Tx_rd_rq  : std_logic;
  signal Rx_data, Tx_data : std_logic_vector(7 downto 0);
  
  signal Tx_FIFO_empty, Rx_FIFO_empty : std_logic;
  signal Rx_FIFO_full   : std_logic;
  
  signal Tx_DV : std_logic := '0';
  
begin

  UART_receiver: entity work.UART_Rx(reciever)
    generic map(CLK_per_BAUD => CLK_per_BAUD)
    port map(CLK => CLK, S_Rx => Rx, DV => Rx_DV,
             Dout => Rx_data);
         
  UART_transmitter: entity work.UART_Tx(transmitter)
    generic map(CLK_per_BAUD => CLK_per_BAUD)
    port map(CLK => CLK, S_Tx => Tx, DV => Tx_DV,
             rd_rq => Tx_rd_rq, Din => Tx_data,
             CTS => CTS);
  
  Rx_FIFO: entity work.FIFO(FIFO_logic)
    generic map(WORDS_BITS => Rx_FIFO_DEPTH,
                ALMOST_FULL => FIFO_alm_full)
    port map(CLK => CLK, 
             wr => Rx_DV, data_in => Rx_data, full => open, almst_full => Rx_FIFO_full,
             empty => Rx_FIFO_empty, rd => out_rd, data_out => Data_out);
         
  Tx_FIFO: entity work.FIFO(FIFO_logic)
    generic map(WORDS_BITS => Tx_FIFO_DEPTH,
                ALMOST_FULL => FIFO_alm_full)
    port map(CLK => CLK, 
             wr => in_DV, data_in => Data_in, full => busy, almst_full => open,
             empty => Tx_FIFO_empty, rd => Tx_rd_rq, data_out => Tx_data);

 
  Tx_DV <= not Tx_FIFO_empty;
 
  out_DV <= not Rx_FIFO_empty;
  RTS <= Rx_FIFO_full;
  
end controller;
