#define __NEXYS__ 0
#define __TDAQ__  1

#ifndef PLATFORM_TYPE
	#error PLATFORM_TYPE is not defined
#endif

#ifndef PLAT_DEBUG
	#define dbg_print(...) do { } while (0)
#else
	#include <stdio.h>
	#define STRINGIZE(x) STRINGIZE2(x)
	#define STRINGIZE2(x) #x

	#define dbg_print(fmt, ...) do { \
			xil_printf("[" __FILE__ "." STRINGIZE(__LINE__) "] " fmt ,##__VA_ARGS__); \
	} while (0)
#endif
