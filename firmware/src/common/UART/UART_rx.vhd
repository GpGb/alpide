library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UART_Rx is
  -- NOTE how we always work with CLK_per_BAUD - 2:
  -- -1 because counting start from 0
  -- -1 for the 1 clk delay during state change.
  generic(CLK_per_BAUD: natural := 25);

  port( 
    CLK  : in std_logic;
    S_Rx : in std_logic;
    DV   : out std_logic;
    Dout : out std_logic_vector(7 downto 0)
  );

end UART_Rx;


architecture reciever of UART_Rx is

  type UART_state is (idle, Rx_start_bit, Rx_data_bit, Rx_stop_bit);

  signal curr_state : UART_state := idle;
  signal counter    : natural range 0 to CLK_per_BAUD - 2 := 0;
  signal bit_in_reg : std_logic_vector(1 downto 0) := (others => '1'); --async reg
  signal bit_in     : std_logic;
  signal data_in    : std_logic_vector(7 downto 0);
  signal dat_index  : natural range 0 to 8 := 0;

  signal filter_en  : std_logic := '0';
  signal v_bit      : std_logic := '0';
  
begin
  -------- syncronize input signal ----------
  bit_in <= bit_in_reg(bit_in_reg'high);
  sample: process(CLK)
  begin
    if rising_edge(CLK) then
      bit_in_reg <= bit_in_reg(bit_in_reg'high-1 downto 0) & S_Rx;
    end if;
  end process;
  -------------------------------------------
  
  filter: entity work.MV_filter
    generic map(LENGHT => CLK_per_BAUD - 2)
    port map(
      CLK => CLK, enable => filter_en, 
      bit_in => bit_in, bit_out => v_bit
    );
  
  recv_SM: process(CLK)
  begin
    if rising_edge(CLK) then

      case curr_state is

        when idle =>
          counter <= 0;
          filter_en <= '0';
          DV <= '0';
          if bit_in = '0' then
            curr_state <= Rx_start_bit;
          else
            curr_state <= idle;
          end if;


        when Rx_start_bit =>
          if counter < CLK_per_BAUD - 2 then
            counter <= counter + 1;
          else
            counter <= 0;
            curr_state <= RX_data_bit;
          end if;


        when Rx_data_bit =>
          if counter < CLK_per_BAUD - 2 then
            filter_en <= '1';
            counter <= counter + 1;
            curr_state <= Rx_data_bit;
          else
            filter_en <= '0';
            counter <= 0;
            data_in <= v_bit & data_in(7 downto 1);
            if dat_index = 7 then
              dat_index <= 0;
              curr_state <= Rx_stop_bit;
            else
		      dat_index <= dat_index + 1;
              curr_state <= Rx_data_bit;
            end if;
          end if;


        when Rx_stop_bit =>
          -- make sure we reach the stopbit, then
          -- ping DV and go idle.
          if bit_in = '0' then
            curr_state <= Rx_stop_bit;
          else
            DV <=  '1';
            curr_state <= idle;
          end if;
      
      end case;

    end if;
  end process recv_SM;

  DOUT <= data_in;

end reciever;
