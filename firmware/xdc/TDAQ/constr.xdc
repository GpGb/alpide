# clock manager
create_clock -name sysclk -period 12.5 sysclk
create_generated_clock -name clk40 -source clk_mng_inst/clk_80_bufg/O -divide_by 2 \
	clk_mng_inst/clk_40_bufg/O

# forwarded alpide clocks
create_generated_clock -name ALP_clk_0  -source [get_pins gen_staves[0].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[0]]
create_generated_clock -name ALP_clk_1  -source [get_pins gen_staves[1].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[1]]
create_generated_clock -name ALP_clk_2  -source [get_pins gen_staves[2].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[2]]
create_generated_clock -name ALP_clk_3  -source [get_pins gen_staves[3].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[3]]
create_generated_clock -name ALP_clk_4  -source [get_pins gen_staves[4].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[4]]
create_generated_clock -name ALP_clk_5  -source [get_pins gen_staves[5].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[5]]
create_generated_clock -name ALP_clk_6  -source [get_pins gen_staves[6].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[6]]
create_generated_clock -name ALP_clk_7  -source [get_pins gen_staves[7].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[7]]
create_generated_clock -name ALP_clk_8  -source [get_pins gen_staves[8].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[8]]
create_generated_clock -name ALP_clk_9  -source [get_pins gen_staves[9].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[9]]
create_generated_clock -name ALP_clk_10 -source [get_pins gen_staves[10].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[10]]
create_generated_clock -name ALP_clk_11 -source [get_pins gen_staves[11].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[11]]
create_generated_clock -name ALP_clk_12 -source [get_pins gen_staves[12].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[12]]
create_generated_clock -name ALP_clk_13 -source [get_pins gen_staves[13].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[13]]
create_generated_clock -name ALP_clk_14 -source [get_pins gen_staves[14].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[14]]


# CTRL
set_property IOB true [get_ports ALP_ctrl_R[*]]
set_property IOB true [get_ports ALP_ctrl_D[*]]

#set min_D -2.0;
#set max_D 2.0;
#set min_R 6;
#set max_R 20.0;

#set_output_delay -clock [get_clocks ALP_clk_0] -min -add_delay $min_D [get_ports ALP_ctrl_D[0]]
#set_output_delay -clock [get_clocks ALP_clk_0] -max -add_delay $max_D [get_ports ALP_ctrl_D[0]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[0]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[0]]
#
#set_output_delay -clock [get_clocks ALP_clk_1] -min -add_delay $min_D [get_ports ALP_ctrl_D[1]]
#set_output_delay -clock [get_clocks ALP_clk_1] -max -add_delay $max_D [get_ports ALP_ctrl_D[1]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[1]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[1]]
#
#set_output_delay -clock [get_clocks ALP_clk_2] -min -add_delay $min_D [get_ports ALP_ctrl_D[2]]
#set_output_delay -clock [get_clocks ALP_clk_2] -max -add_delay $max_D [get_ports ALP_ctrl_D[2]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[2]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[2]]
#
#set_output_delay -clock [get_clocks ALP_clk_3] -min -add_delay $min_D [get_ports ALP_ctrl_D[3]]
#set_output_delay -clock [get_clocks ALP_clk_3] -max -add_delay $max_D [get_ports ALP_ctrl_D[3]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[3]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[3]]
#
#set_output_delay -clock [get_clocks ALP_clk_4] -min -add_delay $min_D [get_ports ALP_ctrl_D[4]]
#set_output_delay -clock [get_clocks ALP_clk_4] -max -add_delay $max_D [get_ports ALP_ctrl_D[4]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[4]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[4]]
#
#set_output_delay -clock [get_clocks ALP_clk_5] -min -add_delay $min_D [get_ports ALP_ctrl_D[5]]
#set_output_delay -clock [get_clocks ALP_clk_5] -max -add_delay $max_D [get_ports ALP_ctrl_D[5]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[5]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[5]]
#
#set_output_delay -clock [get_clocks ALP_clk_6] -min -add_delay $min_D [get_ports ALP_ctrl_D[6]]
#set_output_delay -clock [get_clocks ALP_clk_6] -max -add_delay $max_D [get_ports ALP_ctrl_D[6]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[6]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[6]]
#
#set_output_delay -clock [get_clocks ALP_clk_7] -min -add_delay $min_D [get_ports ALP_ctrl_D[7]]
#set_output_delay -clock [get_clocks ALP_clk_7] -max -add_delay $max_D [get_ports ALP_ctrl_D[7]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[7]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[7]]
#
#set_output_delay -clock [get_clocks ALP_clk_8] -min -add_delay $min_D [get_ports ALP_ctrl_D[8]]
#set_output_delay -clock [get_clocks ALP_clk_8] -max -add_delay $max_D [get_ports ALP_ctrl_D[8]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[8]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[8]]
#
#set_output_delay -clock [get_clocks ALP_clk_9] -min -add_delay $min_D [get_ports ALP_ctrl_D[9]]
#set_output_delay -clock [get_clocks ALP_clk_9] -max -add_delay $max_D [get_ports ALP_ctrl_D[9]]
#set_input_delay  -clock [get_clocks clk40]  -min -add_delay $min_R [get_ports ALP_ctrl_R[9]]
#set_input_delay  -clock [get_clocks clk40]  -max -add_delay $max_R [get_ports ALP_ctrl_R[9]]
#
#set_output_delay -clock [get_clocks ALP_clk_10] -min -add_delay $min_D [get_ports ALP_ctrl_D[10]]
#set_output_delay -clock [get_clocks ALP_clk_10] -max -add_delay $max_D [get_ports ALP_ctrl_D[10]]
#set_input_delay  -clock [get_clocks clk40]   -min -add_delay $min_R [get_ports ALP_ctrl_R[10]]
#set_input_delay  -clock [get_clocks clk40]   -max -add_delay $max_R [get_ports ALP_ctrl_R[10]]
#
#set_output_delay -clock [get_clocks ALP_clk_11] -min -add_delay $min_D [get_ports ALP_ctrl_D[11]]
#set_output_delay -clock [get_clocks ALP_clk_11] -max -add_delay $max_D [get_ports ALP_ctrl_D[11]]
#set_input_delay  -clock [get_clocks clk40]   -min -add_delay $min_R [get_ports ALP_ctrl_R[11]]
#set_input_delay  -clock [get_clocks clk40]   -max -add_delay $max_R [get_ports ALP_ctrl_R[11]]
#
#set_output_delay -clock [get_clocks ALP_clk_12] -min -add_delay $min_D [get_ports ALP_ctrl_D[12]]
#set_output_delay -clock [get_clocks ALP_clk_12] -max -add_delay $max_D [get_ports ALP_ctrl_D[12]]
#set_input_delay  -clock [get_clocks clk40]   -min -add_delay $min_R [get_ports ALP_ctrl_R[12]]
#set_input_delay  -clock [get_clocks clk40]   -max -add_delay $max_R [get_ports ALP_ctrl_R[12]]
#
#set_output_delay -clock [get_clocks ALP_clk_13] -min -add_delay $min_D [get_ports ALP_ctrl_D[13]]
#set_output_delay -clock [get_clocks ALP_clk_13] -max -add_delay $max_D [get_ports ALP_ctrl_D[13]]
#set_input_delay  -clock [get_clocks clk40]   -min -add_delay $min_R [get_ports ALP_ctrl_R[13]]
#set_input_delay  -clock [get_clocks clk40]   -max -add_delay $max_R [get_ports ALP_ctrl_R[13]]
#
#set_output_delay -clock [get_clocks ALP_clk_14] -min -add_delay $min_D [get_ports ALP_ctrl_D[14]]
#set_output_delay -clock [get_clocks ALP_clk_14] -max -add_delay $max_D [get_ports ALP_ctrl_D[14]]
#set_input_delay  -clock [get_clocks clk40]   -min -add_delay $min_R [get_ports ALP_ctrl_R[14]]
#set_input_delay  -clock [get_clocks clk40]   -max -add_delay $max_R [get_ports ALP_ctrl_R[14]]



# Power analisys
set_switching_activity -toggle_rate 0.001000 -static_probability 0.99 \
	[get_port reset]
set_switching_activity -toggle_rate 0.001000 -static_probability 0.4 \
	[get_port PPS]
set_switching_activity -toggle_rate 0.5 -static_probability 0.5 \
	[get_port ALP_ctrl_R]
set_switching_activity -toggle_rate 0.9 -static_probability 0.01 \
	[get_port spw_di]
set_switching_activity -toggle_rate 0.9 -static_probability 0.01 \
	[get_port spw_si]
set_switching_activity -toggle_rate 0.00003 -static_probability 0.8 \
	[get_port trig_evnt]
