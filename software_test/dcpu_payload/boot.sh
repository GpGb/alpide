#!/bin/bash

BASEDIR=$(dirname "$0")
DCPU_TTY="/dev/ttyUSB0"
DCPU_PAYLOAD="${BASEDIR}/spw_server"
TDAQ_BIT="TDAQ.bit"
IP_ADDR="192.168.1.10"

exc_prog_fpga=0
exc_login=0
exc_load_payload=0
payload_debug=0

HELP="${0} [hAflpb:D]
	-A: exec all ops\n
	-f: TDAQ .bit file
	-b: TDAQ bitstream location\n
	-l: exec login on serial port
	-p: load and exec payload
	-D: don't execute payload
	-T: path to /dev/tty
"

while getopts "hAflpb:DT" arg; do
	case $arg in
		A)
			exc_prog_fpga=1
			exc_login=1
			exc_load_payload=1
			;;
		f)
			exc_prog_fpga=1
			;;
		l)
			exc_login=1
			;;
		p)
			exc_load_payload=1
			;;
		b)
			TDAQ_BIT="${OPTARG}"
			;;
		D)
			payload_debug=1
			;;
		T)
			DCPU_TTY="${OPTARG}"
			;;
		h|*)
			echo -ne "${HELP}"
	esac
done


if [[ $exc_prog_fpga -eq 1 ]]; then
	killall hw_server >> /dev/null
	sleep 2
	openFPGALoader -c digilent_hs3 "${TDAQ_BIT}"
fi


if [[ $exc_login -eq 1 ]]; then
	tee "${DCPU_TTY}" <<< "root"
	sleep 0.5
	tee "${DCPU_TTY}" <<< "root"
	sleep 0.5
	tee "${DCPU_TTY}" <<< "ip addr add ${IP_ADDR}/24 dev eth0"
fi


if [[ $exc_load_payload -eq 1 ]]; then

	sshpass -p "root" ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oHostKeyAlgorithms=+ssh-rsa \
		"root@${IP_ADDR}" "killall spw_server"

	sshpass -p "root" scp -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oHostKeyAlgorithms=+ssh-rsa \
		"${DCPU_PAYLOAD}" "root@${IP_ADDR}:."

	if [[ $payload_debug -eq 0 ]]; then
		sshpass -p "root" ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oHostKeyAlgorithms=+ssh-rsa \
			"root@${IP_ADDR}" "./spw_server &"
	else
		tee "${DCPU_TTY}" <<< "./spw_server -v"
	fi

fi
