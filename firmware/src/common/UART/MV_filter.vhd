library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MV_filter is
 
  generic(LENGHT : natural := 8);

  port( 
    CLK    : in std_logic;
    bit_in : in std_logic;
    enable : in std_logic;
    bit_out: out std_logic	 
  );

end MV_filter;


architecture MV_counter of MV_filter is

  constant THRESHOLD : natural := LENGHT/2;
  signal counter : natural range 0 to LENGHT := 0;

begin

  process(CLK)
  begin
    if rising_edge(CLK) then
	 
	   if enable = '1' then
		   if bit_in = '1' then
		     counter <= counter + 1;
		   end if;
		 else
		   counter <= 0;
		end if;
	 
	 end if;
  end process;
  
  bit_out <= '0' when counter <= THRESHOLD else '1';

end MV_counter;