#ifndef SRC_SPW_DPRAM_H_
#define SRC_SPW_DPRAM_H_

#include <xparameters.h>
#include <xil_io.h>

#define DPRAM_BASEADDR   XPAR_BRAM_0_BASEADDR
#define DPRAM_SCRATCH_MAX  0xf30
#define DPRAM_TEMP_DATA_OFFSET  (DPRAM_BASEADDR + DPRAM_SCRATCH_MAX)

#define BRAM_COMMAND_GETPROGR(X)  Xil_In32(DPRAM_BASEADDR)
#define BRAM_COMMAND_PROGRESS(X)  Xil_Out32(DPRAM_BASEADDR, (X));
#define BRAM_COMMAND_OK()         Xil_Out32(DPRAM_BASEADDR, 0x0);
#define BRAM_COMMAND_FAIL()       Xil_Out32(DPRAM_BASEADDR, 0xffffffff);

int bram_write_done();
int bram_write_scratch(const void *data, uint32_t bytes);

#endif // SRC_FIFO_H_
