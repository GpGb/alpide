#include <stdint.h>
#include <sleep.h>

#include "xparameters.h"
#include "FIFO.h"
#include "xllfifo.h"
#include "xstatus.h"

#define PIXFIFO_DEV_ID  XPAR_AXI_FIFO_0_DEVICE_ID

XLlFifo pixFifoInstance;

struct data_packet_buffer RX_packet_buffer = {.count=0};


int init_fifo(){
	int32_t status;
	XLlFifo_Config *Config;

	Config = XLlFfio_LookupConfig(PIXFIFO_DEV_ID);
	if (!Config)
		return -1;

	status = XLlFifo_CfgInitialize(&pixFifoInstance, Config, Config->BaseAddress);
	if (status != XST_SUCCESS)
		return -2;

	// Check for the Reset value (?)
	status = XLlFifo_Status(&pixFifoInstance);
	XLlFifo_IntClear(&pixFifoInstance, 0xffffffff);
	status = XLlFifo_Status(&pixFifoInstance);
	if(status != XST_SUCCESS)
		return -1;

	return 0;
}


int do_read_fifo(XLlFifo* instance){
	RX_packet_buffer.count = 0;

	uint32_t ReceiveLength = XLlFifo_RxGetLen(instance);

	if(ReceiveLength > 0) {
		XLlFifo_Read(instance, RX_packet_buffer.data, ReceiveLength);
		RX_packet_buffer.count = ReceiveLength;
	}

	if(!XLlFifo_IsRxDone(instance))
		return -1;

	return 0;
}


int read_pix_fifo(){

	uint32_t status = 0;
	RX_packet_buffer.count = 0;

	while(RX_packet_buffer.count == 0){
		status = do_read_fifo(&pixFifoInstance);

		if (status < 0)
			break;

		if (RX_packet_buffer.count == 0)
			usleep(100);
	}

	return status;
}


//int write_fifo(){
//
//	// zero padding
//	uint8_t remainder = TX_packet_buffer.count%2;
//	if (remainder){
//		memset(&TX_packet_buffer.data[TX_packet_buffer.count], 0, remainder);
//		TX_packet_buffer.count += remainder;
//	}
//
//
//	if (XLlFifo_TxVacancy(&FifoInstance) < TX_packet_buffer.count/4)
//		return -1;
//
//	XLlFifo_Write(&FifoInstance, TX_packet_buffer.data, TX_packet_buffer.count);
//	XLlFifo_TxSetLen(&FifoInstance, TX_packet_buffer.count);
//
//	while( !(XLlFifo_IsTxDone(&FifoInstance)));
//
//	TX_packet_buffer.count = 0;
//
//	return 0;
//}


//int pkt_push_word(const void *data, uint32_t bytes){
//
//	uint32_t high_addr = TX_packet_buffer.count + bytes;
//
//	if (high_addr > DATA_BUFF_MAX_LEN)
//		return -1;
//
//	memcpy(&TX_packet_buffer.data[TX_packet_buffer.count], data, bytes);
//	TX_packet_buffer.count += bytes;
//
//	return 0;
//}
