library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity vadj_set_mod is
	port(
		clk  : in std_logic;
		rst  : in std_logic;
		vadj_en  : out std_logic := '0';
		set_vadj : out std_logic_vector(1 downto 0) := "00"
	);
end vadj_set_mod;


architecture Behavioral of vadj_set_mod is

	signal configured : std_logic := '0';

begin


	process(clk)
	begin
		if rising_edge(clk) then

			vadj_en <= configured;

			if configured = '0' then
				configured <= '1';
				set_vadj <= "11";
			end if;

			if rst = '1' then
				configured <= '0';
				set_vadj <= "00";
			end if;

		end if;
	end process;

end architecture;
