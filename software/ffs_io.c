#include <stdint.h>
#include "ffs_io.h"
#include "qspi_flash.h"
#include "spw_dpram.h"
#include "xstatus.h"
#include "platform.h"

// spiffs struct and cache buffers
static spiffs fs;
static u8_t spiffs_work_buf[PAGE_SIZE*2];
static u8_t spiffs_fds[32*4];
static u8_t spiffs_cache_buf[(PAGE_SIZE+32)*4];

static uint8_t mount_runned = 0;

// *** spiffs flash read/write/erase wrappers
static s32_t spiffs_read_wrp(u32_t addr, u32_t size, u8_t *dst) {
	if (SpiFlashRead(addr, size, dst) != XST_SUCCESS)
		return SPIFFS_ERR_INTERNAL;

	return SPIFFS_OK;
}
static s32_t spiffs_write_wrp(u32_t addr, u32_t size, u8_t *src) {
	if (SpiFlashWrite(addr, size, src) != XST_SUCCESS)
		return SPIFFS_ERR_INTERNAL;

	return SPIFFS_OK;
}
static s32_t spiffs_erase_wrp(u32_t addr, u32_t size) {
	if (SpiFlashSectorErase(addr, size) != XST_SUCCESS)
		return SPIFFS_ERR_INTERNAL;

	return SPIFFS_OK;
}
// ****


// *** spiffs wrappers for file open/close/read/wirte
// (to avoid passing aound the filesystem structure pointer)
spiffs_file ffs_open(const char *path, spiffs_flags flags, spiffs_mode mode){
	return SPIFFS_open(&fs, path, flags, mode);
}
int32_t ffs_close(spiffs_file fd){
	return SPIFFS_close(&fs, fd);
}
int32_t ffs_read(spiffs_file fd, void *buf, int32_t len){
	return SPIFFS_read(&fs, fd, buf, (s32_t)len);
}
int32_t ffs_write(spiffs_file fd, void *buf, int32_t len){
	return SPIFFS_write(&fs, fd, buf, (s32_t)len);
}
int32_t ffs_lseek(spiffs_file fd, int32_t offs, int whence){
	return SPIFFS_lseek(&fs, fd, (s32_t)offs, whence);
}
int32_t ffs_eof(spiffs_file fd){
	return SPIFFS_eof(&fs, fd);
}
int32_t ffs_errno(){
	return SPIFFS_errno(&fs);
}
int32_t ffs_check(){
	return SPIFFS_check(&fs);
}
// ****



int mount_spiffs(){

	spiffs_config cfg;

	cfg.hal_read_f = spiffs_read_wrp;
	cfg.hal_write_f = spiffs_write_wrp;
	cfg.hal_erase_f = spiffs_erase_wrp;

	int status = SPIFFS_mount(&fs,
		&cfg,
		spiffs_work_buf,
		spiffs_fds,
		sizeof(spiffs_fds),
		spiffs_cache_buf,
		sizeof(spiffs_cache_buf),
		0);

	mount_runned = 1;
	return status;
}



int ffs_format(){

	// pre-format procedure
	if (!mount_runned){
		mount_spiffs();
	}
	if (SPIFFS_mounted(&fs)){
		SPIFFS_unmount(&fs);
	}

	// format fs
	int status = SPIFFS_format(&fs);

	if (status != SPIFFS_OK)
		return -1;

	status = mount_spiffs();

	return status;
}


const char* index_to_fname(uint8_t index){

	static const char* file_enum[] = {"cnf0", "cnf1", "cnf3", "cnfa", "cnfb", "hpx"};
	const int file_imax = sizeof(file_enum)/sizeof(file_enum[0]);

	if (index > file_imax)
		return NULL;

	return file_enum[index];
}


int ffs_write_file(uint8_t index, void *src, uint32_t len, uint8_t mode) {
	dbg_print("Writing file: %i:%s, mode: %i", index, index_to_fname(index), mode);

	spiffs_flags file_flags = SPIFFS_CREAT | SPIFFS_RDWR;

	if (mode == 'a'){
		file_flags |= SPIFFS_APPEND;
	}
	else{
		file_flags |= SPIFFS_TRUNC;
	}

	spiffs_file fd = SPIFFS_open(&fs, index_to_fname(index), file_flags, 0);

	if (fd<0)
		return fd;

	int status = SPIFFS_write(&fs, fd, (char*)src, len);
	SPIFFS_close(&fs, fd);

	if (status < 0)
		return SPIFFS_errno(&fs);
	else
		return 0;

}



int ffs_dump_file(uint8_t index, uint32_t offset){
	dbg_print("Dumping file: %d:%s\n", index, index_to_fname(index));

	uint8_t buf[256];
	int status = 0;

	spiffs_file fd = SPIFFS_open(&fs, index_to_fname(index), SPIFFS_O_RDONLY, 0);

	if (fd<0)
		return fd;

	// check if file exist (..or is just empty)
	if (SPIFFS_eof(&fs, fd)){
		SPIFFS_close(&fs, fd);
		return -2;
	}

	if (offset != 0){
		SPIFFS_lseek(&fs, fd, offset, SPIFFS_SEEK_SET);
	}

	while (!SPIFFS_eof(&fs, fd)){
		int32_t len = SPIFFS_read(&fs, fd, (u8_t *)buf, 256);

		if (len < 0){
			status = SPIFFS_errno(&fs);
			break;
		}

		bram_write_scratch(&len, sizeof(len));
		bram_write_scratch(buf, len);

	}

	bram_write_done();
	SPIFFS_close(&fs, fd);

	return status;

}



//int ffs_list_files(){
//
//	spiffs_DIR rootdir;
//	struct spiffs_dirent e;
//	struct spiffs_dirent *pe = &e;
//
//	SPIFFS_opendir(&fs, "/", &rootdir);
//	while ((pe = SPIFFS_readdir(&rootdir, pe))) {
//		pkt_push_word(pe->name, strlen(pe->name)+1);
//	}
//
//	SPIFFS_closedir(&rootdir);
//
//	write_fifo();
//
//	return 0;
//
//}
