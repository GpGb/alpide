#TODO: code duplication...
import os
import socket
import struct
import ctypes as cty

import colorize

from math import ceil
from time import sleep


class cmd_package():

    READ  = 0x03
    BREAD = 0xee
    WRITE = 0x0c
    CMD_ADDR = 0x2000

    def __init__(self, pkt_type=bytes(4), addr=bytes(4), data=bytes(4), EOP=0, buffer=None):

        if buffer == None:
            self.buffer = bytearray(10)

            self.buffer[0] = pkt_type;
            self.buffer[1:5] = addr
            self.buffer[5:9] = data
            self.buffer[9] = EOP
        else:
            assert(len(buffer)==10)
            self.buffer = buffer

        #if DEBUG:
        #    self.print_pkt()


    def get_data(self):
        return self.buffer[5:9]


    def print_pkt(self):

        print('raw: ', self.buffer)

        print('cmd:  ', hex(self.buffer[0]))
        print('addr: ', hex(int.from_bytes(self.buffer[1:5], byteorder = 'little')))
        print('data: ', hex(int.from_bytes(self.buffer[5:9], byteorder = 'little')))
        print('EOP:  ', hex(self.buffer[9]))



class daq_comm():


    def __init__(self, HOST, PORT, COLOR_OUT=True, no_bread=False):

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((HOST, PORT))

        self.COLOR_OUT = COLOR_OUT
        self.no_bread = no_bread


    def write_reg(self, addr, data):
        addr = addr.to_bytes(4, byteorder='little')
        data = data.to_bytes(4, byteorder='little')

        self.sock.sendall(cmd_package(cmd_package.WRITE, addr, data).buffer)
        return 0


    def read_reg(self, addr, raw=False):
        addr = addr.to_bytes(4, byteorder='little')

        self.sock.sendall(cmd_package(cmd_package.READ, addr).buffer)
        buffer = self.sock.recv(4)
        if len(buffer) != 4:
            raise Exception('fail to read data: ', len(buffer))

        if raw:
            return buffer
        else:
            return int.from_bytes(buffer, byteorder = 'little')


    def write_command(self, command):

        cmd_addr = (cmd_package.CMD_ADDR).to_bytes(4, byteorder='little')

        # pad if necessary
        rem = len(command)%4
        if rem != 0:
            command += bytes(4-rem)

        # if no args just write command and return
        if len(command) == 4:
            self.sock.sendall(cmd_package(cmd_package.WRITE, cmd_addr, command).buffer)
            return 0

        # write args
        addr = 0x2001
        for i in range(4, len(command), 4):
            b_addr = addr.to_bytes(4, byteorder='little')
            b_data = command[i:i+4]
            self.sock.sendall(cmd_package(cmd_package.WRITE, b_addr, b_data).buffer)
            addr += 1


        # write command
        self.sock.sendall(cmd_package(cmd_package.WRITE, cmd_addr, command[0:4]).buffer)
        return 0


    def read_cmd_response(self, raw=False):

        status = self.wait_done()
        if (status != 0):
            return [status]

        res_len = self.read_reg(0x2001)
        if res_len == 0:
            return []

        word_len = ceil(res_len/4)

        if self.no_bread:
            addr = 0x2002
            data = bytearray()
            for word in range(word_len):
                print(f'reg: {word}/{word_len}')
                data += self.read_reg(addr, raw=True)
                addr += 1;
        else:
            addr = (0x2002).to_bytes(4, byteorder='little')
            data = (word_len).to_bytes(4, byteorder='little')
            self.sock.sendall(cmd_package(cmd_package.BREAD, addr, data).buffer)
            data = self.sock.recv(word_len*4)

        if raw:
            rem = res_len%4
            if rem: data = data[:-rem]
            return data
        else:
            dec_data = []
            for n in range(0, len(data), 4):
                dec_data.append(int.from_bytes(data[n:n+4], byteorder = 'little'))

            return dec_data


    def read_data_pkg(self):
        # read packet header and get pkt size
        hdata = self.sock.recv(4, socket.MSG_WAITALL | socket.MSG_PEEK)

        pktlen = int.from_bytes(hdata[0:2], byteorder="little")

        if hdata[2:4] != b'\xff\xff':
            print('\n\nerrr', hdata.hex())

        # read and return full packet (len + header)
        pktdata = self.sock.recv(pktlen+16, socket.MSG_WAITALL)

        return pktdata


    def wait_done(self, color = None, timeout = None):

        if self.COLOR_OUT:

            if color == 'thr' or color == 'digi':
                colorize.thr_progress(lambda: self.read_reg(0x2000), color)
                return self.read_reg(0x2000)


        status = self.read_reg(0x2000)

        while(status != 0 and status != 0xffffffff):
            sleep(0.5)
            status = self.read_reg(0x2000)
            print('status: ', hex(status), end='\r');

            if timeout != None:
                timeout -= 1
                if timeout == 0:
                    print('Command timed out..')
                    return -1;

        print(' '*20, end='\r') # clear line
        return status



class daq_comm_ftdi():

    def __init__(self, COLOR_OUT=True):

        self.COLOR_OUT = COLOR_OUT
        self.ftdi_init = False

        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.ftdi_spw = cty.cdll.LoadLibrary(script_dir + '/ftdi_spw.so')

        if self.ftdi_spw.init_ftdi_fifo():
            raise Exception("Error init ftdi fifo")

        self.ftdi_init = True


    def __del__(self):
        if self.ftdi_init:
            self.ftdi_spw.cleanup()


    def write_reg(self, addr, data):
        ret = self.ftdi_spw.spw_send_cmd(cmd_package.WRITE, addr, data, None)
        return ret


    def read_reg(self, addr, raw=False):
        buffer = cty.create_string_buffer(4)
        ret = self.ftdi_spw.spw_send_cmd(cmd_package.READ, addr, 0, buffer)

        if raw:
            return buffer.raw
        else:
            return int.from_bytes(buffer.raw, byteorder = 'little')


    def read_burst(self, addr, count):
        buffer = cty.create_string_buffer(4*count)
        ret = self.ftdi_spw.spw_send_cmd(cmd_package.BREAD, addr, count, buffer)

        return ret, buffer.raw



    def write_command(self, command):

        # pad if necessary
        rem = len(command)%4
        if rem != 0:
            command += bytes(4-rem)

        fmt = str(len(command)//4) + 'I'
        cmd_int = struct.unpack(fmt, command)

        # if no args just write command and return
        if len(cmd_int) == 1:
            ret = self.write_reg(cmd_package.CMD_ADDR, cmd_int[0])
            return ret

        # write args
        for i in range(1, len(cmd_int)):

            ret = self.write_reg(0x2000+i, cmd_int[i])
            if ret < 0:
                print('Error sending command: ', ret)
                return ret


        # write command
        ret = self.write_reg(cmd_package.CMD_ADDR, cmd_int[0])
        return ret


    def read_cmd_response(self, raw=False):

        status = self.wait_done()
        if (status != 0):
            return [status]

        res_len = self.read_reg(0x2001)
        if res_len == 0:
            return []

        word_len = ceil(res_len/4)
        ret, data = self.read_burst(0x2002, word_len)

        # read response
        if raw:
            rem = res_len%4
            if rem: data = data[:-rem]
            return data
        else:
            dec_data = []
            for n in range(0, len(data), 4):
                dec_data.append(int.from_bytes(data[n:n+4], byteorder = 'little'))

            return dec_data


    def read_data_pkg(self):
        RX_DATA_MAX = 800
        buffer = cty.create_string_buffer(RX_DATA_MAX*4)
        ret = self.ftdi_spw.read_event(buffer)
        if ret == -1111:
            raise KeyboardInterrupt

        return buffer.raw


    def wait_done(self, color = None, timeout = None):

        if self.COLOR_OUT:

            if color == 'thr' or color == 'digi':
                colorize.thr_progress(lambda: self.read_reg(0x2000), color)
                return self.read_reg(0x2000)


        status = self.read_reg(0x2000)

        while(status != 0 and status != 0xffffffff):
            sleep(0.5)
            status = self.read_reg(0x2000)
            print('status: ', hex(status), end='\r');

            if timeout != None:
                timeout -= 1
                if timeout == 0:
                    print('Command timed out..')
                    return -1;

        print(' '*20, end='\r') # clear line
        return status

