library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cmd_decoder_TB is
end cmd_decoder_TB;

architecture Behavioral of cmd_decoder_TB is

	constant T_clk_pkgr : time := 12.5 ns; -- 80 MHz
	constant T_clk_alp  : time := 25 ns; -- 40 MHz

	constant trigger_lut_single : std_logic_vector(31 downto 0)
		:= "0000000" & "10000" & "01000" & "00100" & "00010" & "00001";
	constant trigger_lut_triple : std_logic_vector(31 downto 0)
		:= "0000000" & "11000" & "11100" & "01110" & "00111" & "00011";
	constant trigger_lut_all : std_logic_vector(31 downto 0)
		:= "0000000" & "11111" & "11111" & "11111" & "11111" & "11111";
	constant trigger_lut_any : std_logic_vector(31 downto 0)
		:= "1000000" & "00000" & "00000" & "00000" & "00000" & "00000";

	signal clk_pkgr, clk_alp, ext_rst : std_logic := '0';
	signal rst_80, rst_daq : std_logic;
	signal rst_reg : std_logic_vector(31 downto 0);

	signal busy, daq_run : std_logic := '0';
	signal trig_evnt: std_logic := '0';
	signal trig_in, trig_out, trig_mask : std_logic_vector(4 downto 0);
	signal trig_mask_ev : std_logic;
	signal trig_cnt_out : std_logic_vector(31 downto 0);
	signal trigger_lut : std_logic_vector(31 downto 0);

	signal cmd_reg : std_logic_vector(31 downto 0);
	signal cmd_intr, cmd_intr_mcu: std_logic := '0';

begin

	uut: entity work.cmd_decoder
		port map(
			clk_alp  => clk_alp,
			clk_pkgr => clk_pkgr,
			ext_rst => ext_rst,
			rst_reg => rst_reg,
			rst_80  => rst_80,
			rst_daq => rst_daq,
			-- trigger
			daq_run   => daq_run,
			busy      => busy,
			trig_evnt => trig_evnt,
			trig_in   => trig_in,
			trig_out  => trig_out,
			trig_mask => trig_mask,
			trig_mask_ev  => trig_mask_ev,
			trig_cnt_out  => trig_cnt_out,
			busy_viol_cnt => open,
			trigger_lut   => trigger_lut
		);


		clk_pkgr <= not clk_pkgr after T_clk_pkgr/2;
		clk_alp  <= not clk_alp  after T_clk_alp/2;

		stim: process

			procedure trig_roll is
			begin
			for i in 0 to 4 loop
				trig_evnt <= '1';
				wait for T_clk_pkgr * 10;
				trig_evnt <= '0';
				wait for 10 us;
				trig_in <= trig_in rol 1;
			end loop;

			end trig_roll;

		begin
			ext_rst <= '1';
			rst_reg <= x"96969696";
			wait for T_clk_pkgr * 100;
			wait until rising_edge(clk_pkgr);
			trig_in <= "00000";
			trig_evnt <= '0';

			-- hardware reset
			ext_rst <= '0';
			wait for T_clk_alp;
			ext_rst <= '1';
			wait for T_clk_alp*5;
			assert rst_80 = '1' report "1. rst_80 not asserted";
			assert rst_daq = '1' report "1. rst_40 not asserted";
			wait until rst_80 = '0';
			wait for T_clk_alp*10;

			-- global reset command
			rst_reg <= x"55555555";
			wait for T_clk_pkgr;
			rst_reg <= x"96969696";
			wait for T_clk_pkgr*6;
			assert rst_80 = '1' report "2. rst_80 not asserted";
			assert rst_daq = '1' report "2. rst not asserted";
			wait until rst_80 = '0';
			wait for T_clk_pkgr*20;

			-- daq reset command
			rst_reg <= x"0da00da0";
			wait for T_clk_pkgr;
			rst_reg <= x"96969696";
			wait for T_clk_pkgr*6;
			assert rst_80 = '0' report "3. rst_80 should not be asserted";
			assert rst_daq = '1' report "3. rst not asserted";
			wait until rst_daq = '0';

			-- enable daq mode
			daq_run <= '1';
			trigger_lut <= trigger_lut_single;
			trig_in <= "00001";
			wait for T_clk_alp;
			trig_roll;

			wait for T_clk_alp*10;
			trigger_lut <= trigger_lut_triple;
			wait for T_clk_alp;
			trig_roll;

			wait for T_clk_alp*10;
			trigger_lut <= trigger_lut_any;
			wait for T_clk_alp;
			trig_roll;

			wait;

		end process;


end architecture;
