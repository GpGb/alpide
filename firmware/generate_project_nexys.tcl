#*****************************************************************************************
#
# ALPIDE.tcl: Tcl script for re-creating project 'ALPIDE'
#
#
# This file contains the Vivado Tcl commands for re-creating the project to the state*
# when this script was generated. In order to re-create the project, please source this
# file in the Vivado Tcl Shell.
#
# * Note that the runs in the created project will be configured the same way as the
#   original project, however they will not be launched automatically. To regenerate the
#   run results please launch the synthesis/implementation runs as needed.
#
#*****************************************************************************************

# Check file required for this script exists
proc checkRequiredFiles { origin_dir} {
  set status true

  set files [list \
 "[file normalize "$origin_dir/src/common/OneWire/sockit_owm.v"]"\
 "[file normalize "$origin_dir/src/common/types_pkg.vhd"]"\
 "[file normalize "$origin_dir/src/common/OneWire/AXI_AMM_owm_wrapper.vhd"]"\
 "[file normalize "$origin_dir/src/common/ALPIDE_pkg.vhd"]"\
 "[file normalize "$origin_dir/src/common/ALPIDE_CTRL/ALPIDE_FSM.vhd"]"\
 "[file normalize "$origin_dir/src/common/ALPIDE_CTRL/ALPIDE_CTRL.vhd"]"\
 "[file normalize "$origin_dir/src/common/ALPIDE_CTRL/stave_data_buffer.vhd"]"\
 "[file normalize "$origin_dir/src/common/ALPIDE_CTRL/CTRL_top.vhd"]"\
 "[file normalize "$origin_dir/src/common/UART/FIFO.vhd"]"\
 "[file normalize "$origin_dir/src/common/UART/MV_filter.vhd"]"\
 "[file normalize "$origin_dir/src/common/UART/UART_rx.vhd"]"\
 "[file normalize "$origin_dir/src/common/UART/UART_tx.vhd"]"\
 "[file normalize "$origin_dir/src/common/UART/UART_controller.vhd"]"\
 "[file normalize "$origin_dir/src/common/cses_reg_file_manager/register_file.vhd"]"\
 "[file normalize "$origin_dir/src/common/cses_reg_file_manager/spw_controller.vhd"]"\
 "[file normalize "$origin_dir/src/common/ucrc_par.vhd"]"\
 "[file normalize "$origin_dir/src/nexys/vadj_set.vhd"]"\
 "[file normalize "$origin_dir/src/common/AXI_ctrl_regs.vhd"]"\
 "[file normalize "$origin_dir/src/nexys/FT_USB_FSM.vhd"]"\
 "[file normalize "$origin_dir/src/common/SEM.vhd"]"\
 "[file normalize "$origin_dir/src/common/cmd_decoder.vhd"]"\
 "[file normalize "$origin_dir/src/common/pps_timestamp.vhd"]"\
 "[file normalize "$origin_dir/src/common/data_decoder.vhd"]"\
 "[file normalize "$origin_dir/src/common/output_mux.vhd"]"\
 "[file normalize "$origin_dir/src/nexys/spacewire_axis_wrapper.vhd"]"\
 "[file normalize "$origin_dir/src/nexys/TDAQ_top.vhd"]"\
 "[file normalize "$origin_dir/xdc/nexys/pin_assign.xdc"]"\
 "[file normalize "$origin_dir/xdc/nexys/constr.xdc"]"\
 "[file normalize "$origin_dir/sim/spacewire_axis_wrapper_TB.vhd"]"\
 "[file normalize "$origin_dir/sim/pps_timestamp.vhd"]"\
 "[file normalize "$origin_dir/sim/output_mux_TB.vhd"]"\
 "[file normalize "$origin_dir/sim/cmd_decoder_TB.vhd"]"\
 "[file normalize "$origin_dir/sim/ALPIDE_CTRL_TB.vhd"]"\
 "[file normalize "$origin_dir/sim/ALPIDE_FSM_TB.vhd"]"\
 "[file normalize "$origin_dir/sim/ALPIDE_bhv_sim.vhd"]"\
 "[file normalize "$origin_dir/sim/data_decoder_TB.vhd"]"\
 "[file normalize "$origin_dir/sim/stave_data_buffer_TB.vhd"]"\
  ]
  foreach ifile $files {
    if { ![file isfile $ifile] } {
      puts " Could not find remote file $ifile "
      set status false
    }
  }

  return $status
}
# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir "."

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir $::origin_dir_loc
}

# Set the project name
set _xil_proj_name_ "nexys"

# Use project name variable, if specified in the tcl shell
if { [info exists ::user_project_name] } {
  set _xil_proj_name_ $::user_project_name
}

variable script_file
set script_file "nexys.tcl"

# Help information for this script
proc print_help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir <path>\]"
  puts "$script_file -tclargs \[--project_name <name>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--project_name <name>\] Create project with the specified name. Default"
  puts "                       name is the name of the project from where this"
  puts "                       script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < $::argc} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir"   { incr i; set origin_dir [lindex $::argv $i] }
      "--project_name" { incr i; set _xil_proj_name_ [lindex $::argv $i] }
      "--help"         { print_help }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
}


# set local board store repository
set xilinx_board_store_path [get_property LOCAL_ROOT_DIR [xhub::get_xstores xilinx_board_store]]
set_param board.repoPaths ${xilinx_board_store_path}
puts "Set Board Part RepoPath: [get_param board.repoPaths]"


# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir/nexys"]"

# Check for paths and files needed for project creation
set validate_required 0
if { $validate_required } {
  if { [checkRequiredFiles $origin_dir] } {
    puts "Tcl file $script_file is valid. All files required for project creation is accesable. "
  } else {
    puts "Tcl file $script_file is not valid. Not all files required for project creation is accesable. "
    return
  }
}

# Create project
create_project ${_xil_proj_name_} ./${_xil_proj_name_} -part xc7a200tsbg484-1

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Set project properties
set obj [current_project]
set_property -name "board_part" -value "digilentinc.com:nexys_video:part0:1.2" -objects $obj
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "enable_vhdl_2008" -value "1" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# Set 'sources_1' fileset object
set obj [get_filesets sources_1]
set files [list \
 [file normalize "${origin_dir}/src/common/OneWire/sockit_owm.v"] \
 [file normalize "${origin_dir}/src/common/types_pkg.vhd"] \
 [file normalize "${origin_dir}/src/common/OneWire/AXI_AMM_owm_wrapper.vhd"] \
 [file normalize "${origin_dir}/src/common/ALPIDE_pkg.vhd"] \
 [file normalize "${origin_dir}/src/common/ALPIDE_CTRL/ALPIDE_FSM.vhd"] \
 [file normalize "${origin_dir}/src/common/ALPIDE_CTRL/ALPIDE_CTRL.vhd"] \
 [file normalize "${origin_dir}/src/common/ALPIDE_CTRL/stave_data_buffer.vhd"] \
 [file normalize "${origin_dir}/src/common/ALPIDE_CTRL/CTRL_top.vhd"] \
 [file normalize "${origin_dir}/src/common/UART/FIFO.vhd"] \
 [file normalize "${origin_dir}/src/common/UART/MV_filter.vhd"] \
 [file normalize "${origin_dir}/src/common/UART/UART_rx.vhd"] \
 [file normalize "${origin_dir}/src/common/UART/UART_tx.vhd"] \
 [file normalize "${origin_dir}/src/common/UART/UART_controller.vhd"] \
 [file normalize "${origin_dir}/src/common/cses_reg_file_manager/register_file.vhd"] \
 [file normalize "${origin_dir}/src/common/cses_reg_file_manager/spw_controller.vhd"] \
 [file normalize "${origin_dir}/src/common/ucrc_par.vhd"] \
 [file normalize "${origin_dir}/src/nexys/vadj_set.vhd"] \
 [file normalize "${origin_dir}/src/common/AXI_ctrl_regs.vhd"] \
 [file normalize "${origin_dir}/src/nexys/FT_USB_FSM.vhd"] \
 [file normalize "${origin_dir}/src/common/SEM.vhd"] \
 [file normalize "${origin_dir}/src/common/cmd_decoder.vhd"] \
 [file normalize "${origin_dir}/src/common/pps_timestamp.vhd"] \
 [file normalize "${origin_dir}/src/common/data_decoder.vhd"] \
 [file normalize "${origin_dir}/src/common/output_mux.vhd"] \
 [file normalize "${origin_dir}/src/nexys/spacewire_axis_wrapper.vhd"] \
 [file normalize "${origin_dir}/src/nexys/TDAQ_top.vhd"] \
]
add_files -norecurse -fileset $obj $files

# Set 'sources_1' fileset file properties for remote files
set file "$origin_dir/src/common/types_pkg.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/OneWire/AXI_AMM_owm_wrapper.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/ALPIDE_pkg.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/ALPIDE_CTRL/ALPIDE_FSM.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/common/ALPIDE_CTRL/ALPIDE_CTRL.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/common/ALPIDE_CTRL/stave_data_buffer.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/ALPIDE_CTRL/CTRL_top.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/UART/FIFO.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/UART/MV_filter.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/UART/UART_rx.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/UART/UART_tx.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/UART/UART_controller.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/cses_reg_file_manager/register_file.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/cses_reg_file_manager/spw_controller.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/ucrc_par.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/nexys/vadj_set.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/src/common/AXI_ctrl_regs.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/nexys/FT_USB_FSM.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/common/SEM.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/common/cmd_decoder.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/common/pps_timestamp.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/common/data_decoder.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/common/output_mux.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/nexys/spacewire_axis_wrapper.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/nexys/TDAQ_top.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property -name "top" -value "TDAQ_top" -objects $obj

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

# Set 'constrs_1' fileset object
set obj [get_filesets constrs_1]

# Add/Import constrs file and set constrs file properties
set file "[file normalize "$origin_dir/xdc/nexys/pin_assign.xdc"]"
set file_added [add_files -norecurse -fileset $obj [list $file]]
set file "$origin_dir/xdc/nexys/pin_assign.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property -name "file_type" -value "XDC" -objects $file_obj

# Add/Import constrs file and set constrs file properties
set file "[file normalize "$origin_dir/xdc/nexys/constr.xdc"]"
set file_added [add_files -norecurse -fileset $obj [list $file]]
set file "$origin_dir/xdc/nexys/constr.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property -name "file_type" -value "XDC" -objects $file_obj

# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
set files [list \
 [file normalize "${origin_dir}/sim/spacewire_axis_wrapper_TB.vhd"] \
 [file normalize "${origin_dir}/sim/pps_timestamp.vhd"] \
 [file normalize "${origin_dir}/sim/output_mux_TB.vhd"] \
 [file normalize "${origin_dir}/sim/cmd_decoder_TB.vhd"] \
 [file normalize "${origin_dir}/sim/ALPIDE_CTRL_TB.vhd"] \
 [file normalize "${origin_dir}/sim/ALPIDE_FSM_TB.vhd"] \
 [file normalize "${origin_dir}/sim/ALPIDE_bhv_sim.vhd"] \
 [file normalize "${origin_dir}/sim/data_decoder_TB.vhd"] \
 [file normalize "${origin_dir}/sim/stave_data_buffer_TB.vhd"] \
]
add_files -norecurse -fileset $obj $files

# Set 'sim_1' fileset file properties for remote files
set file "$origin_dir/sim/spacewire_axis_wrapper_TB.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/sim/pps_timestamp.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL" -objects $file_obj

set file "$origin_dir/sim/output_mux_TB.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/sim/cmd_decoder_TB.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/sim/ALPIDE_CTRL_TB.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/sim/ALPIDE_FSM_TB.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/sim/ALPIDE_bhv_sim.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/sim/data_decoder_TB.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/sim/stave_data_buffer_TB.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

# add non-BD IPs
create_ip -name clk_wiz -vendor xilinx.com -library ip -module_name clk_wiz_0
set_property -dict [list \
 CONFIG.CLKOUT2_USED {true} \
 CONFIG.CLK_OUT1_PORT {clk_40} \
 CONFIG.CLK_OUT2_PORT {clk_pkgr} \
 CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {40} \
 CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {60} \
 CONFIG.USE_LOCKED {false} \
 CONFIG.USE_RESET {false} \
 CONFIG.MMCM_DIVCLK_DIVIDE {1} \
 CONFIG.MMCM_CLKOUT2_DIVIDE {18} \
 CONFIG.MMCM_CLKFBOUT_MULT_F {9.000} \
 CONFIG.MMCM_CLKOUT0_DIVIDE_F {22.500} \
 CONFIG.MMCM_CLKOUT1_DIVIDE {15} \
 CONFIG.NUM_OUT_CLKS {3} \
 CONFIG.CLKOUT1_JITTER {270.400} \
 CONFIG.CLKOUT1_PHASE_ERROR {105.461} \
 CONFIG.CLKOUT2_JITTER {153.276} \
 CONFIG.CLKOUT2_PHASE_ERROR {105.461} \
 CONFIG.CLKOUT3_JITTER {159.475} \
 CONFIG.CLKOUT3_PHASE_ERROR {105.461} \
 CONFIG.CLKOUT3_USED {true} \
 CONFIG.CLK_OUT3_PORT {clk_qspi2} \
 CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {50}] \
 [get_ips clk_wiz_0]

create_ip -name fifo_generator -vendor xilinx.com -library ip -module_name data_fifo
set_property -dict [list \
 CONFIG.INTERFACE_TYPE {AXI_STREAM} \
 CONFIG.Clock_Type_AXI {Independent_Clock} \
 CONFIG.TUSER_WIDTH {0} \
 CONFIG.Enable_TLAST {true} \
 CONFIG.HAS_TSTRB {false} \
 CONFIG.HAS_TKEEP {false} \
 CONFIG.FIFO_Implementation_wach {Independent_Clocks_Distributed_RAM} \
 CONFIG.Full_Threshold_Assert_Value_wach {15} \
 CONFIG.Empty_Threshold_Assert_Value_wach {13} \
 CONFIG.FIFO_Implementation_wdch {Independent_Clocks_Block_RAM} \
 CONFIG.Empty_Threshold_Assert_Value_wdch {1021} \
 CONFIG.FIFO_Implementation_wrch {Independent_Clocks_Distributed_RAM} \
 CONFIG.Full_Threshold_Assert_Value_wrch {15} \
 CONFIG.Empty_Threshold_Assert_Value_wrch {13} \
 CONFIG.FIFO_Implementation_rach {Independent_Clocks_Distributed_RAM} \
 CONFIG.Full_Threshold_Assert_Value_rach {15} \
 CONFIG.Empty_Threshold_Assert_Value_rach {13} \
 CONFIG.FIFO_Implementation_rdch {Independent_Clocks_Block_RAM} \
 CONFIG.Empty_Threshold_Assert_Value_rdch {1021} \
 CONFIG.FIFO_Implementation_axis {Independent_Clocks_Block_RAM} \
 CONFIG.Enable_ECC_axis {true} \
 CONFIG.Programmable_Full_Type_axis {Single_Programmable_Full_Threshold_Constant} \
 CONFIG.Full_Threshold_Assert_Value_axis {1018} \
 CONFIG.Empty_Threshold_Assert_Value_axis {1021}] \
 [get_ips data_fifo]


# Trig ID FIFO
create_ip -name fifo_generator -vendor xilinx.com -library ip -module_name trig_fifo
set_property -dict [list \
 CONFIG.Component_Name {trig_fifo} \
 CONFIG.Fifo_Implementation {Common_Clock_Block_RAM} \
 CONFIG.Performance_Options {First_Word_Fall_Through} \
 CONFIG.Input_Data_Width {69} \
 CONFIG.Input_Depth {16} \
 CONFIG.Output_Data_Width {69} \
 CONFIG.Output_Depth {16} \
 CONFIG.Enable_ECC {true} \
 CONFIG.Use_Dout_Reset {false} \
 CONFIG.Use_Extra_Logic {true} \
 CONFIG.Data_Count_Width {5} \
 CONFIG.Write_Data_Count_Width {5} \
 CONFIG.Read_Data_Count_Width {5} \
 CONFIG.Full_Threshold_Assert_Value {15} \
 CONFIG.Full_Threshold_Negate_Value {14} \
 CONFIG.Empty_Threshold_Assert_Value {4} \
 CONFIG.Empty_Threshold_Negate_Value {5}] \
 [get_ips trig_fifo]


# event fifo (event_ff for data and header_ff for pkg headers)
create_ip -name axis_data_fifo -vendor xilinx.com -library ip -module_name event_ff
set_property -dict [list \
 CONFIG.FIFO_DEPTH {2048} \
 CONFIG.TDATA_NUM_BYTES {4} \
 CONFIG.HAS_TLAST {1} \
 CONFIG.Component_Name {event_ff}] \
 [get_ips event_ff]

create_ip -name axis_data_fifo -vendor xilinx.com -library ip -module_name header_ff
set_property -dict [list \
 CONFIG.TDATA_NUM_BYTES {4} \
 CONFIG.FIFO_DEPTH {32} \
 CONFIG.FIFO_MODE {2} \
 CONFIG.HAS_TLAST {1} \
 CONFIG.Component_Name {header_ff}] \
 [get_ips header_ff]

# SEM controller
create_ip -name sem -vendor xilinx.com -library ip -module_name sem_0
set_property -dict [list \
 CONFIG.ENABLE_CLASSIFICATION {false} \
 CONFIG.ENABLE_INJECTION {false} \
 CONFIG.INJECTION_SHIM {none} \
 CONFIG.CLOCK_FREQ {40}] \
 [get_ips sem_0]

# tx/rx buffer for FTDI FIFO
create_ip -name axis_data_fifo -vendor xilinx.com -library ip -module_name FT_data_FIFO
set_property -dict [list \
 CONFIG.FIFO_DEPTH {512} \
 CONFIG.HAS_TLAST {1} \
 CONFIG.IS_ACLK_ASYNC {1} \
 CONFIG.Component_Name {FT_data_FIFO}] \
 [get_ips FT_data_FIFO]

# width converter for packager data
create_ip -name axis_dwidth_converter -vendor xilinx.com -library ip -module_name pkgr_dwidth_converter
set_property -dict [list \
 CONFIG.S_TDATA_NUM_BYTES {4} \
 CONFIG.M_TDATA_NUM_BYTES {1} \
 CONFIG.HAS_TLAST {1} \
 CONFIG.Component_Name {pkgr_dwidth_converter}] \
 [get_ips pkgr_dwidth_converter]


generate_target all [get_ips -exclude_bd_ips]


# Set 'sim_1' fileset file properties for local files
# None

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property -name "top" -value "TDAQ_top" -objects $obj
set_property -name "top_auto_set" -value "0" -objects $obj
set_property -name "top_lib" -value "xil_defaultlib" -objects $obj


# Set 'utils_1' fileset properties
set obj [get_filesets utils_1]




# Proc to create BD BD_cpu
proc cr_bd_BD_cpu { parentCell } {
# The design that will be created by this Tcl proc contains the following
# module references:
# AXI_owm_wrapper



  # CHANGE DESIGN NAME HERE
  set design_name BD_cpu

  common::send_gid_msg -ssname BD::TCL -id 2010 -severity "INFO" "Currently there is no design <$design_name> in project, so creating one..."

  create_bd_design $design_name

  set bCheckIPsPassed 1
  ##################################################################
  # CHECK IPs
  ##################################################################
  set bCheckIPs 1
  if { $bCheckIPs == 1 } {
     set list_check_ips "\
  xilinx.com:ip:axi_iic:2.1\
  xilinx.com:ip:axi_quad_spi:3.2\
  xilinx.com:ip:axi_timebase_wdt:3.0\
  xilinx.com:ip:axi_timer:2.0\
  xilinx.com:ip:xpm_cdc_gen:1.0\
  xilinx.com:ip:mdm:3.2\
  xilinx.com:ip:microblaze:11.0\
  xilinx.com:ip:axi_intc:4.1\
  xilinx.com:ip:xlconcat:2.1\
  xilinx.com:ip:proc_sys_reset:5.0\
  xilinx.com:ip:smartconnect:1.0\
  xilinx.com:ip:axi_bram_ctrl:4.1\
  xilinx.com:ip:axi_fifo_mm_s:4.3\
  xilinx.com:ip:axis_clock_converter:1.1\
  xilinx.com:ip:blk_mem_gen:8.4\
  xilinx.com:ip:lmb_bram_if_cntlr:4.0\
  xilinx.com:ip:lmb_v10:3.0\
  "

   set list_ips_missing ""
   common::send_gid_msg -ssname BD::TCL -id 2011 -severity "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_gid_msg -ssname BD::TCL -id 2012 -severity "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

  }

  ##################################################################
  # CHECK Modules
  ##################################################################
  set bCheckModules 1
  if { $bCheckModules == 1 } {
     set list_check_mods "\
  AXI_owm_wrapper\
  "

   set list_mods_missing ""
   common::send_gid_msg -ssname BD::TCL -id 2020 -severity "INFO" "Checking if the following modules exist in the project's sources: $list_check_mods ."

   foreach mod_vlnv $list_check_mods {
      if { [can_resolve_reference $mod_vlnv] == 0 } {
         lappend list_mods_missing $mod_vlnv
      }
   }

   if { $list_mods_missing ne "" } {
      catch {common::send_gid_msg -ssname BD::TCL -id 2021 -severity "ERROR" "The following module(s) are not found in the project: $list_mods_missing" }
      common::send_gid_msg -ssname BD::TCL -id 2022 -severity "INFO" "Please add source files for the missing module(s) above."
      set bCheckIPsPassed 0
   }
}

  if { $bCheckIPsPassed != 1 } {
    common::send_gid_msg -ssname BD::TCL -id 2023 -severity "WARNING" "Will not continue with creation of design due to the error(s) above."
    return 3
  }


# Hierarchical cell: microblaze_0_local_memory
proc create_hier_cell_microblaze_0_local_memory { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_microblaze_0_local_memory() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB

  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_CTRL

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_CTRL1


  # Create pins
  create_bd_pin -dir O -type intr DLMB_Interrupt
  create_bd_pin -dir O -type intr ILMB_Interrupt
  create_bd_pin -dir I -type clk LMB_Clk
  create_bd_pin -dir I -type rst SYS_Rst
  create_bd_pin -dir I -type rst S_AXI_CTRL_ARESETN

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list \
   CONFIG.C_CE_COUNTER_WIDTH {20} \
   CONFIG.C_CE_FAILING_REGISTERS {1} \
   CONFIG.C_ECC {1} \
   CONFIG.C_ECC_ONOFF_REGISTER {0} \
   CONFIG.C_ECC_STATUS_REGISTERS {1} \
   CONFIG.C_FAULT_INJECT {0} \
   CONFIG.C_INTERCONNECT {2} \
   CONFIG.C_UE_FAILING_REGISTERS {1} \
 ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list \
   CONFIG.C_CE_COUNTER_WIDTH {20} \
   CONFIG.C_CE_FAILING_REGISTERS {1} \
   CONFIG.C_ECC {1} \
   CONFIG.C_ECC_ONOFF_REGISTER {0} \
   CONFIG.C_ECC_STATUS_REGISTERS {1} \
   CONFIG.C_INTERCONNECT {2} \
   CONFIG.C_UE_FAILING_REGISTERS {1} \
   CONFIG.C_WRITE_ACCESS {0} \
 ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 lmb_bram ]
  set_property -dict [ list \
   CONFIG.Algorithm {Fixed_Primitives} \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Primitive {2kx9} \
   CONFIG.Use_RSTB_Pin {true} \
   CONFIG.use_bram_block {BRAM_Controller} \
 ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_0_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_axi [get_bd_intf_pins S_AXI_CTRL] [get_bd_intf_pins dlmb_bram_if_cntlr/S_AXI_CTRL]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_0_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_axi [get_bd_intf_pins S_AXI_CTRL1] [get_bd_intf_pins ilmb_bram_if_cntlr/S_AXI_CTRL]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net SYS_Rst_1 [get_bd_pins SYS_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  connect_bd_net -net S_AXI_CTRL_ARESETN_1 [get_bd_pins S_AXI_CTRL_ARESETN] [get_bd_pins dlmb_bram_if_cntlr/S_AXI_CTRL_ARESETN] [get_bd_pins ilmb_bram_if_cntlr/S_AXI_CTRL_ARESETN]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/S_AXI_CTRL_ACLK] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/S_AXI_CTRL_ACLK] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_0_dlmb_int [get_bd_pins DLMB_Interrupt] [get_bd_pins dlmb_bram_if_cntlr/Interrupt]
  connect_bd_net -net microblaze_0_ilmb_int [get_bd_pins ILMB_Interrupt] [get_bd_pins ilmb_bram_if_cntlr/Interrupt]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: FIRM_IFACE
proc create_hier_cell_FIRM_IFACE { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_FIRM_IFACE() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 AXI_STR_PIXDATA

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:bram_rtl:1.0 SPW_BRAM

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_FIFO_PIXDATA

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_SPW


  # Create pins
  create_bd_pin -dir I clk_pkgr
  create_bd_pin -dir I -type clk s_axi_aclk
  create_bd_pin -dir I src_arst

  # Create instance: axi_bram_ctrl_0, and set properties
  set axi_bram_ctrl_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_0 ]
  set_property -dict [ list \
   CONFIG.ECC_TYPE {Hamming} \
   CONFIG.PROTOCOL {AXI4} \
   CONFIG.SINGLE_PORT_BRAM {1} \
 ] $axi_bram_ctrl_0

  # Create instance: axi_fifo_pixdata, and set properties
  set axi_fifo_pixdata [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_fifo_mm_s:4.3 axi_fifo_pixdata ]
  set_property -dict [ list \
   CONFIG.C_USE_TX_CTRL {0} \
   CONFIG.C_USE_TX_DATA {0} \
 ] $axi_fifo_pixdata

  # Create instance: axis_clock_converter_0, and set properties
  set axis_clock_converter_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_clock_converter:1.1 axis_clock_converter_0 ]

  # Create instance: blk_mem_gen_0, and set properties
  set blk_mem_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0 ]
  set_property -dict [ list \
   CONFIG.Assume_Synchronous_Clk {false} \
   CONFIG.Byte_Size {8} \
   CONFIG.EN_SAFETY_CKT {true} \
   CONFIG.Enable_32bit_Address {true} \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Register_PortA_Output_of_Memory_Primitives {false} \
   CONFIG.Register_PortB_Output_of_Memory_Primitives {false} \
   CONFIG.Use_Byte_Write_Enable {true} \
   CONFIG.Use_RSTA_Pin {true} \
   CONFIG.Use_RSTB_Pin {true} \
   CONFIG.use_bram_block {BRAM_Controller} \
 ] $blk_mem_gen_0

  # Create instance: xpm_cdc_gen_0, and set properties
  set xpm_cdc_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xpm_cdc_gen:1.0 xpm_cdc_gen_0 ]
  set_property -dict [ list \
   CONFIG.CDC_TYPE {xpm_cdc_sync_rst} \
   CONFIG.DEST_SYNC_FF {2} \
 ] $xpm_cdc_gen_0

  # Create interface connections
  connect_bd_intf_net -intf_net AXI_STR_PIXDATA_1 [get_bd_intf_pins AXI_STR_PIXDATA] [get_bd_intf_pins axis_clock_converter_0/S_AXIS]
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins SPW_BRAM] [get_bd_intf_pins blk_mem_gen_0/BRAM_PORTB]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S_AXI_SPW] [get_bd_intf_pins axi_bram_ctrl_0/S_AXI]
  connect_bd_intf_net -intf_net Conn5 [get_bd_intf_pins S_AXI_FIFO_PIXDATA] [get_bd_intf_pins axi_fifo_pixdata/S_AXI]
  connect_bd_intf_net -intf_net axi_bram_ctrl_0_BRAM_PORTA [get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA] [get_bd_intf_pins blk_mem_gen_0/BRAM_PORTA]
  connect_bd_intf_net -intf_net axis_clock_converter_0_M_AXIS [get_bd_intf_pins axi_fifo_pixdata/AXI_STR_RXD] [get_bd_intf_pins axis_clock_converter_0/M_AXIS]

  # Create port connections
  connect_bd_net -net clk_pkgr_1 [get_bd_pins clk_pkgr] [get_bd_pins axis_clock_converter_0/s_axis_aclk] [get_bd_pins xpm_cdc_gen_0/dest_clk]
  connect_bd_net -net s_axi_aclk_1 [get_bd_pins s_axi_aclk] [get_bd_pins axi_bram_ctrl_0/s_axi_aclk] [get_bd_pins axi_fifo_pixdata/s_axi_aclk] [get_bd_pins axis_clock_converter_0/m_axis_aclk]
  connect_bd_net -net src_arst_1 [get_bd_pins src_arst] [get_bd_pins axi_bram_ctrl_0/s_axi_aresetn] [get_bd_pins axi_fifo_pixdata/s_axi_aresetn] [get_bd_pins axis_clock_converter_0/m_axis_aresetn] [get_bd_pins xpm_cdc_gen_0/src_rst]
  connect_bd_net -net xpm_cdc_gen_0_dest_rst_out [get_bd_pins axis_clock_converter_0/s_axis_aresetn] [get_bd_pins xpm_cdc_gen_0/dest_rst_out]

  # Restore current instance
  current_bd_instance $oldCurInst
}
  variable script_folder

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set AXI_STR_PIXDATA [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 AXI_STR_PIXDATA ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {60000000} \
   CONFIG.HAS_TKEEP {0} \
   CONFIG.HAS_TLAST {1} \
   CONFIG.HAS_TREADY {1} \
   CONFIG.HAS_TSTRB {0} \
   CONFIG.LAYERED_METADATA {undef} \
   CONFIG.TDATA_NUM_BYTES {4} \
   CONFIG.TDEST_WIDTH {0} \
   CONFIG.TID_WIDTH {0} \
   CONFIG.TUSER_WIDTH {0} \
   ] $AXI_STR_PIXDATA

  set AXI_ctrl [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_ctrl ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {40000000} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.PROTOCOL {AXI4LITE} \
   ] $AXI_ctrl

  set SPW_BRAM [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:bram_rtl:1.0 SPW_BRAM ]
  set_property -dict [ list \
   CONFIG.MASTER_TYPE {BRAM_CTRL} \
   ] $SPW_BRAM

  set TSP_IIC [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 TSP_IIC ]

  set qspi_flash [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:spi_rtl:1.0 qspi_flash ]


  # Create ports
  set axi_rst_out [ create_bd_port -dir O -from 0 -to 0 -type rst axi_rst_out ]
  set clk_40 [ create_bd_port -dir I -type clk -freq_hz 40000000 clk_40 ]
  set clk_pkgr [ create_bd_port -dir I -type clk -freq_hz 60000000 clk_pkgr ]
  set cmd_intr_mcu [ create_bd_port -dir I -type intr cmd_intr_mcu ]
  set_property -dict [ list \
   CONFIG.SENSITIVITY {EDGE_RISING} \
 ] $cmd_intr_mcu
  set ext_spi_clk [ create_bd_port -dir I -type clk -freq_hz 50000000 ext_spi_clk ]
  set owr_i [ create_bd_port -dir I -from 2 -to 0 owr_i ]
  set owr_o [ create_bd_port -dir O -from 2 -to 0 owr_o ]
  set owr_t [ create_bd_port -dir O -from 2 -to 0 owr_t ]
  set reset_rtl [ create_bd_port -dir I -type rst reset_rtl ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $reset_rtl

  # Create instance: AXI_owm_wrapper_0, and set properties
  set block_name AXI_owm_wrapper
  set block_cell_name AXI_owm_wrapper_0
  if { [catch {set AXI_owm_wrapper_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $AXI_owm_wrapper_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.OWN_ports {3} \
 ] $AXI_owm_wrapper_0

  # Create instance: FIRM_IFACE
  create_hier_cell_FIRM_IFACE [current_bd_instance .] FIRM_IFACE

  # Create instance: axi_iic_0, and set properties
  set axi_iic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_iic:2.1 axi_iic_0 ]

  # Create instance: axi_quad_spi_0, and set properties
  set axi_quad_spi_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 axi_quad_spi_0 ]
  set_property -dict [ list \
   CONFIG.C_FIFO_DEPTH {256} \
   CONFIG.C_SCK_RATIO {2} \
   CONFIG.C_SHARED_STARTUP {0} \
   CONFIG.C_SPI_MEMORY {2} \
   CONFIG.C_SPI_MODE {2} \
   CONFIG.C_USE_STARTUP {1} \
   CONFIG.C_USE_STARTUP_INT {1} \
   CONFIG.QSPI_BOARD_INTERFACE {qspi_flash} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_quad_spi_0

  # Create instance: axi_timebase_wdt_0, and set properties
  set axi_timebase_wdt_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timebase_wdt:3.0 axi_timebase_wdt_0 ]
  set_property -dict [ list \
   CONFIG.C_WDT_INTERVAL {26} \
   CONFIG.ENABLE_WINDOW_WDT {0} \
   CONFIG.WDT_ENABLE_ONCE {Enable_repeatedly} \
 ] $axi_timebase_wdt_0

  # Create instance: axi_timer_GP_0, and set properties
  set axi_timer_GP_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timer:2.0 axi_timer_GP_0 ]
  set_property -dict [ list \
   CONFIG.enable_timer2 {0} \
 ] $axi_timer_GP_0

  # Create instance: cmd_intr_cdc, and set properties
  set cmd_intr_cdc [ create_bd_cell -type ip -vlnv xilinx.com:ip:xpm_cdc_gen:1.0 cmd_intr_cdc ]
  set_property -dict [ list \
   CONFIG.CDC_TYPE {xpm_cdc_pulse} \
   CONFIG.DEST_SYNC_FF {2} \
   CONFIG.INIT_SYNC_FF {true} \
   CONFIG.RST_USED {false} \
   CONFIG.SIM_ASSERT_CHK {false} \
 ] $cmd_intr_cdc

  # Create instance: mdm_1, and set properties
  set mdm_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:mdm:3.2 mdm_1 ]
  set_property -dict [ list \
   CONFIG.C_ADDR_SIZE {32} \
   CONFIG.C_M_AXI_ADDR_WIDTH {32} \
   CONFIG.C_USE_UART {1} \
 ] $mdm_1

  # Create instance: microblaze_0, and set properties
  set microblaze_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:11.0 microblaze_0 ]
  set_property -dict [ list \
   CONFIG.C_ADDR_TAG_BITS {0} \
   CONFIG.C_DCACHE_ADDR_TAG {0} \
   CONFIG.C_DEBUG_ENABLED {1} \
   CONFIG.C_D_AXI {1} \
   CONFIG.C_D_LMB {1} \
   CONFIG.C_I_LMB {1} \
   CONFIG.C_USE_BARREL {1} \
   CONFIG.C_USE_DIV {1} \
   CONFIG.C_USE_MSR_INSTR {1} \
   CONFIG.C_USE_PCMP_INSTR {1} \
   CONFIG.C_USE_STACK_PROTECTION {1} \
   CONFIG.G_USE_EXCEPTIONS {1} \
   CONFIG.C_USE_HW_MUL {1} \
 ] $microblaze_0

  # Create instance: microblaze_0_axi_intc, and set properties
  set microblaze_0_axi_intc [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 microblaze_0_axi_intc ]
  set_property -dict [ list \
   CONFIG.C_HAS_FAST {1} \
 ] $microblaze_0_axi_intc

  # Create instance: microblaze_0_local_memory
  create_hier_cell_microblaze_0_local_memory [current_bd_instance .] microblaze_0_local_memory

  # Create instance: microblaze_0_xlconcat, and set properties
  set microblaze_0_xlconcat [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 microblaze_0_xlconcat ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {5} \
 ] $microblaze_0_xlconcat

  # Create instance: rst_Clk, and set properties
  set rst_Clk [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_Clk ]

  # Create instance: sleep_timer, and set properties
  set sleep_timer [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timer:2.0 sleep_timer ]

  # Create instance: smartconnect_0, and set properties
  set smartconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_0 ]
  set_property -dict [ list \
   CONFIG.NUM_CLKS {1} \
   CONFIG.NUM_MI {13} \
   CONFIG.NUM_SI {1} \
 ] $smartconnect_0

  # Create interface connections
  connect_bd_intf_net -intf_net AXI_STR_PIXDATA_1 [get_bd_intf_ports AXI_STR_PIXDATA] [get_bd_intf_pins FIRM_IFACE/AXI_STR_PIXDATA]
  connect_bd_intf_net -intf_net BRAM_PORTB_0_1 [get_bd_intf_ports SPW_BRAM] [get_bd_intf_pins FIRM_IFACE/SPW_BRAM]
  connect_bd_intf_net -intf_net S_AXI_CTRL1_1 [get_bd_intf_pins microblaze_0_local_memory/S_AXI_CTRL1] [get_bd_intf_pins smartconnect_0/M01_AXI]
  connect_bd_intf_net -intf_net S_AXI_CTRL_1 [get_bd_intf_pins microblaze_0_local_memory/S_AXI_CTRL] [get_bd_intf_pins smartconnect_0/M00_AXI]
  connect_bd_intf_net -intf_net axi_iic_0_IIC [get_bd_intf_ports TSP_IIC] [get_bd_intf_pins axi_iic_0/IIC]
  connect_bd_intf_net -intf_net axi_quad_spi_0_SPI_0 [get_bd_intf_ports qspi_flash] [get_bd_intf_pins axi_quad_spi_0/SPI_0]
  connect_bd_intf_net -intf_net microblaze_0_M_AXI_DP [get_bd_intf_pins microblaze_0/M_AXI_DP] [get_bd_intf_pins smartconnect_0/S00_AXI]
  connect_bd_intf_net -intf_net microblaze_0_debug [get_bd_intf_pins mdm_1/MBDEBUG_0] [get_bd_intf_pins microblaze_0/DEBUG]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_1 [get_bd_intf_pins microblaze_0/DLMB] [get_bd_intf_pins microblaze_0_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_1 [get_bd_intf_pins microblaze_0/ILMB] [get_bd_intf_pins microblaze_0_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_0_interrupt [get_bd_intf_pins microblaze_0/INTERRUPT] [get_bd_intf_pins microblaze_0_axi_intc/interrupt]
  connect_bd_intf_net -intf_net smartconnect_0_M02_AXI [get_bd_intf_pins sleep_timer/S_AXI] [get_bd_intf_pins smartconnect_0/M02_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M03_AXI [get_bd_intf_pins axi_timebase_wdt_0/S_AXI] [get_bd_intf_pins smartconnect_0/M03_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M04_AXI [get_bd_intf_pins axi_quad_spi_0/AXI_LITE] [get_bd_intf_pins smartconnect_0/M04_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M05_AXI [get_bd_intf_pins FIRM_IFACE/S_AXI_FIFO_PIXDATA] [get_bd_intf_pins smartconnect_0/M05_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M06_AXI [get_bd_intf_ports AXI_ctrl] [get_bd_intf_pins smartconnect_0/M06_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M07_AXI [get_bd_intf_pins FIRM_IFACE/S_AXI_SPW] [get_bd_intf_pins smartconnect_0/M07_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M08_AXI [get_bd_intf_pins mdm_1/S_AXI] [get_bd_intf_pins smartconnect_0/M08_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M09_AXI [get_bd_intf_pins microblaze_0_axi_intc/s_axi] [get_bd_intf_pins smartconnect_0/M09_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M10_AXI [get_bd_intf_pins AXI_owm_wrapper_0/S_AXI] [get_bd_intf_pins smartconnect_0/M10_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M11_AXI [get_bd_intf_pins axi_iic_0/S_AXI] [get_bd_intf_pins smartconnect_0/M11_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M12_AXI [get_bd_intf_pins axi_timer_GP_0/S_AXI] [get_bd_intf_pins smartconnect_0/M12_AXI]

  # Create port connections
  connect_bd_net -net AXI_owm_wrapper_0_owr_o [get_bd_ports owr_o] [get_bd_pins AXI_owm_wrapper_0/owr_o]
  connect_bd_net -net AXI_owm_wrapper_0_owr_t [get_bd_ports owr_t] [get_bd_pins AXI_owm_wrapper_0/owr_t]
  connect_bd_net -net axi_timebase_wdt_0_wdt_interrupt [get_bd_pins axi_timebase_wdt_0/wdt_interrupt] [get_bd_pins microblaze_0_xlconcat/In4]
  connect_bd_net -net axi_timebase_wdt_0_wdt_reset [get_bd_pins axi_timebase_wdt_0/wdt_reset] [get_bd_pins rst_Clk/aux_reset_in]
  connect_bd_net -net axi_timer_GP_0_interrupt [get_bd_pins axi_timer_GP_0/interrupt] [get_bd_pins microblaze_0_xlconcat/In3]
  connect_bd_net -net clk_wiz_0_clk_40 [get_bd_ports clk_40] [get_bd_pins AXI_owm_wrapper_0/S_AXI_ACLK] [get_bd_pins FIRM_IFACE/s_axi_aclk] [get_bd_pins axi_iic_0/s_axi_aclk] [get_bd_pins axi_quad_spi_0/s_axi_aclk] [get_bd_pins axi_timebase_wdt_0/s_axi_aclk] [get_bd_pins axi_timer_GP_0/s_axi_aclk] [get_bd_pins cmd_intr_cdc/dest_clk] [get_bd_pins mdm_1/S_AXI_ACLK] [get_bd_pins microblaze_0/Clk] [get_bd_pins microblaze_0_axi_intc/processor_clk] [get_bd_pins microblaze_0_axi_intc/s_axi_aclk] [get_bd_pins microblaze_0_local_memory/LMB_Clk] [get_bd_pins rst_Clk/slowest_sync_clk] [get_bd_pins sleep_timer/s_axi_aclk] [get_bd_pins smartconnect_0/aclk]
  connect_bd_net -net clk_wiz_0_clk_80 [get_bd_ports clk_pkgr] [get_bd_pins FIRM_IFACE/clk_pkgr] [get_bd_pins cmd_intr_cdc/src_clk]
  connect_bd_net -net cmd_intr_mcu_1 [get_bd_ports cmd_intr_mcu] [get_bd_pins cmd_intr_cdc/src_pulse]
  connect_bd_net -net ext_spi_clk_0_1 [get_bd_ports ext_spi_clk] [get_bd_pins axi_quad_spi_0/ext_spi_clk]
  connect_bd_net -net mdm_1_Debug_SYS_Rst [get_bd_pins mdm_1/Debug_SYS_Rst] [get_bd_pins rst_Clk/mb_debug_sys_rst]
  connect_bd_net -net microblaze_0_dlmb_int [get_bd_pins microblaze_0_local_memory/DLMB_Interrupt] [get_bd_pins microblaze_0_xlconcat/In0]
  connect_bd_net -net microblaze_0_ilmb_int [get_bd_pins microblaze_0_local_memory/ILMB_Interrupt] [get_bd_pins microblaze_0_xlconcat/In1]
  connect_bd_net -net microblaze_0_intr [get_bd_pins microblaze_0_axi_intc/intr] [get_bd_pins microblaze_0_xlconcat/dout]
  connect_bd_net -net owr_i_0_1 [get_bd_ports owr_i] [get_bd_pins AXI_owm_wrapper_0/owr_i]
  connect_bd_net -net reset_rtl_1 [get_bd_ports reset_rtl] [get_bd_pins rst_Clk/ext_reset_in]
  connect_bd_net -net rst_Clk_40M_bus_struct_reset [get_bd_pins microblaze_0_local_memory/SYS_Rst] [get_bd_pins rst_Clk/bus_struct_reset]
  connect_bd_net -net rst_Clk_40M_mb_reset [get_bd_pins microblaze_0/Reset] [get_bd_pins microblaze_0_axi_intc/processor_rst] [get_bd_pins rst_Clk/mb_reset]
  connect_bd_net -net rst_Clk_40M_peripheral_aresetn [get_bd_ports axi_rst_out] [get_bd_pins AXI_owm_wrapper_0/S_AXI_ARESETN] [get_bd_pins FIRM_IFACE/src_arst] [get_bd_pins axi_iic_0/s_axi_aresetn] [get_bd_pins axi_quad_spi_0/s_axi_aresetn] [get_bd_pins axi_timebase_wdt_0/s_axi_aresetn] [get_bd_pins axi_timer_GP_0/s_axi_aresetn] [get_bd_pins mdm_1/S_AXI_ARESETN] [get_bd_pins microblaze_0_axi_intc/s_axi_aresetn] [get_bd_pins microblaze_0_local_memory/S_AXI_CTRL_ARESETN] [get_bd_pins rst_Clk/peripheral_aresetn] [get_bd_pins sleep_timer/s_axi_aresetn] [get_bd_pins smartconnect_0/aresetn]
  connect_bd_net -net xpm_cdc_gen_0_dest_pulse [get_bd_pins cmd_intr_cdc/dest_pulse] [get_bd_pins microblaze_0_xlconcat/In2]

  # Create address segments
  assign_bd_address -offset 0x44A20000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs AXI_ctrl/Reg] -force
  assign_bd_address -offset 0x40000000 -range 0x00000080 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs AXI_owm_wrapper_0/S_AXI/reg0] -force
  assign_bd_address -offset 0x00080000 -range 0x00001000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs FIRM_IFACE/axi_bram_ctrl_0/S_AXI/Mem0] -force
  assign_bd_address -offset 0x44A50000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs FIRM_IFACE/axi_fifo_pixdata/S_AXI/Mem0] -force
  assign_bd_address -offset 0x40800000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_iic_0/S_AXI/Reg] -force
  assign_bd_address -offset 0x44A10000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_quad_spi_0/AXI_LITE/Reg] -force
  assign_bd_address -offset 0x41A00000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_timebase_wdt_0/S_AXI/Reg] -force
  assign_bd_address -offset 0x41C00000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_timer_GP_0/S_AXI/Reg] -force
  assign_bd_address -offset 0x00000000 -range 0x00040000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs microblaze_0_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] -force
  assign_bd_address -offset 0x76000000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs microblaze_0_local_memory/dlmb_bram_if_cntlr/S_AXI_CTRL/Reg] -force
  assign_bd_address -offset 0x76010000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs microblaze_0_local_memory/ilmb_bram_if_cntlr/S_AXI_CTRL/Reg] -force
  assign_bd_address -offset 0x41400000 -range 0x00001000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs mdm_1/S_AXI/Reg] -force
  assign_bd_address -offset 0x41200000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs microblaze_0_axi_intc/S_AXI/Reg] -force
  assign_bd_address -offset 0x44A40000 -range 0x00010000 -target_address_space [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs sleep_timer/S_AXI/Reg] -force
  assign_bd_address -offset 0x00000000 -range 0x00040000 -target_address_space [get_bd_addr_spaces microblaze_0/Instruction] [get_bd_addr_segs microblaze_0_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] -force


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
  close_bd_design $design_name
}
# End of cr_bd_BD_cpu()
cr_bd_BD_cpu ""
set_property REGISTERED_WITH_MANAGER "1" [get_files BD_cpu.bd ]
set_property SYNTH_CHECKPOINT_MODE "Hierarchical" [get_files BD_cpu.bd ]

puts "INFO: Project created:${_xil_proj_name_}"
