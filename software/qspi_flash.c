/******************************************************************************
*
* Copyright (C) 2011 - 2019 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
*
*
******************************************************************************/

#include "xparameters.h"
#include "stdint.h"
#include "qspi_flash.h"
#include "xspi.h"

#define SPI_DEVICE_ID   XPAR_SPI_0_DEVICE_ID
#define INTC_DEVICE_ID  XPAR_INTC_0_DEVICE_ID
#define SPI_INTR_ID     XPAR_INTC_0_SPI_0_VEC_ID

#define SPI_SELECT  0x01

#define COMMAND_WRITE_REGISTER  0x01
#define COMMAND_READ            0x13
#define COMMAND_READ_QUAD       0x6c
#define COMMAND_PAGE_PROGRAM    0x12
#define COMMAND_QUAD_PAGE_PROGRAM    0x34
#define COMMAND_SECTOR_ERASE    0xdc
#define COMMAND_BULK_ERASE      0x60
#define COMMAND_WRITE_ENABLE    0x06
#define COMMAND_STATUSREG1_READ 0x05
#define COMMAND_STATUSREG2_READ 0x07
#define COMMAND_CONFIGREG_READ  0x35

/**
 * This definitions specify the EXTRA bytes in each of the command
 * transactions. This count includes Command byte, address bytes and any
 * don't care bytes needed.
 */
#define WR_EXTRA_BYTES  5
#define RD_EXTRA_BYTES  9  // 4 for quad read latency + cmd EXTRA BYTES
#define WRITE_ENABLE_BYTES  1
#define SECTOR_ERASE_BYTES  5
#define BULK_ERASE_BYTES    1
#define STATUS_READ_BYTES   2


/************************** Function Prototypes ******************************/

int SpiFlashWriteEnable();
int SpiFlashGetStatus();
int SpiFlashQuadEnable();
static int SpiFlashWaitForFlashReady();

/************************** Variable Definitions *****************************/

static XSpi Spi;

static uint8_t SpiFlash_status_reg;
static uint8_t WriteBuffer[PAGE_SIZE + WR_EXTRA_BYTES];
static uint8_t ReadBuffer[PAGE_SIZE + RD_EXTRA_BYTES];

/************************** Function Definitions ******************************/

static INLINE void WriteBuffer_clear(){
	memset(WriteBuffer, 0, sizeof(WriteBuffer));
}


int init_flash_subsystem(void)
{
	int Status;
	XSpi_Config *ConfigPtr;

	ConfigPtr = XSpi_LookupConfig(SPI_DEVICE_ID);
	if (ConfigPtr == NULL)
		return XST_DEVICE_NOT_FOUND;

	Status = XSpi_CfgInitialize(&Spi, ConfigPtr, ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS)
		return XST_FAILURE;

	//Status = XSpi_SelfTest(&Spi);
	//if (Status != XST_SUCCESS)
	//	return XST_FAILURE;

	// Set the SPI device as a master and in manual slave select mode such
	// that the slave select signal does not toggle for every byte of a
	// transfer, this must be done before the slave select is set.
	Status = XSpi_SetOptions(&Spi, XSP_MASTER_OPTION | XSP_MANUAL_SSELECT_OPTION);
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Select the quad flash device on the SPI bus
	Status = XSpi_SetSlaveSelect(&Spi, SPI_SELECT);
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Start the SPI driver in polled mode
	XSpi_Start(&Spi);
	XSpi_IntrGlobalDisable(&Spi);

	WriteBuffer_clear();

	// Set the Quad Enable (QE) bit in the flash device
	Status = SpiFlashQuadEnable();
	if (Status != XST_SUCCESS)
		return XST_FAILURE;

	return XST_SUCCESS;

}


int SpiFlashWriteEnable()
{
	int Status;

	// Wait while the Flash is busy.
	Status = SpiFlashWaitForFlashReady();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Prepare the WriteBuffer.
	WriteBuffer[0] = COMMAND_WRITE_ENABLE;

	// Initiate the Transfer.
	Status = XSpi_Transfer(&Spi, WriteBuffer, NULL, WRITE_ENABLE_BYTES);
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	return XST_SUCCESS;
}



int SpiFlashWrite(uint32_t Addr, uint32_t ByteCount, uint8_t* Buffer)
{
	int Status;

	if (ByteCount > PAGE_SIZE)
		return XST_FAILURE;

	Status = SpiFlashWriteEnable();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	Status = SpiFlashWaitForFlashReady();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Prepare the WriteBuffer.
	WriteBuffer[0] = COMMAND_PAGE_PROGRAM;
	WriteBuffer[1] = (uint8_t) (Addr >> 24);
	WriteBuffer[2] = (uint8_t) (Addr >> 16);
	WriteBuffer[3] = (uint8_t) (Addr >> 8);
	WriteBuffer[4] = (uint8_t) (Addr);

	// Fill tx buffer
	memcpy(WriteBuffer+5, Buffer, ByteCount);

	// Initiate the Transfer.
	Status = XSpi_Transfer(&Spi, WriteBuffer, NULL, (ByteCount + WR_EXTRA_BYTES));
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	return XST_SUCCESS;
}



int SpiFlashRead(uint32_t Addr, uint32_t ByteCount, uint8_t* Buffer)
{
	int Status;

	if (ByteCount > PAGE_SIZE)
		return XST_FAILURE;

	Status = SpiFlashWaitForFlashReady();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Prepare the WriteBuffer.
	WriteBuffer_clear();
	WriteBuffer[0] = COMMAND_READ_QUAD;
	WriteBuffer[1] = (uint8_t) (Addr >> 24);
	WriteBuffer[2] = (uint8_t) (Addr >> 16);
	WriteBuffer[3] = (uint8_t) (Addr >> 8);
	WriteBuffer[4] = (uint8_t) (Addr);

	//if (ReadCmd == COMMAND_QUAD_IO_READ) {
	//	ByteCount += 3;
	//if (ReadCmd == COMMAND_READ_QUAD)
	//ByteCount += 4; // Quad read latency

	// Initiate the Transfer.
	Status = XSpi_Transfer(&Spi, WriteBuffer, ReadBuffer, (ByteCount + RD_EXTRA_BYTES));
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	memcpy(Buffer, ReadBuffer+RD_EXTRA_BYTES, ByteCount);

	return XST_SUCCESS;
}



int SpiFlashBulkErase()
{
	int Status;

	// Wait while the Flash is busy.
	Status = SpiFlashWaitForFlashReady();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	Status = SpiFlashWriteEnable();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Prepare the WriteBuffer.
	WriteBuffer_clear();
	WriteBuffer[0] = COMMAND_BULK_ERASE;

	// Initiate the Transfer.
	Status = XSpi_Transfer(&Spi, WriteBuffer, NULL, BULK_ERASE_BYTES);
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	return XST_SUCCESS;
}



int SpiFlashSectorErase(uint32_t Addr, uint32_t size)
{
	int Status;

	// support erase of a sigle 64k sector for now
	if (size != 64*1024)
		return XST_FAILURE;

	// Perform the Write Enable operation.
	Status = SpiFlashWriteEnable();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Wait while the Flash is busy.
	Status = SpiFlashWaitForFlashReady();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Prepare the WriteBuffer.
	WriteBuffer[0] = COMMAND_SECTOR_ERASE;
	WriteBuffer[1] = (uint8_t) (Addr >> 24);
	WriteBuffer[2] = (uint8_t) (Addr >> 16);
	WriteBuffer[3] = (uint8_t) (Addr >> 8);
	WriteBuffer[4] = (uint8_t) (Addr);

	// Initiate the Transfer.
	Status = XSpi_Transfer(&Spi, WriteBuffer, NULL, SECTOR_ERASE_BYTES);
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	return XST_SUCCESS;
}



int SpiFlashQuadEnable()
{
	int Status;

	// Perform the Write Enable operation.
	Status = SpiFlashWriteEnable();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Wait while the Flash is busy.
	Status = SpiFlashWaitForFlashReady();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Prepare the WriteBuffer.
	WriteBuffer[0] = COMMAND_WRITE_REGISTER;
	WriteBuffer[1] = 0x0;
	WriteBuffer[2] = 0x02; // QE = 1

	// Initiate the Transfer.
	Status = XSpi_Transfer(&Spi, WriteBuffer, NULL, 3);
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Wait while the Flash is busy.
	Status = SpiFlashWaitForFlashReady();
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	// Verify that QE bit is set
	WriteBuffer_clear();
	WriteBuffer[0] = COMMAND_CONFIGREG_READ;
	uint8_t rx_buffer[STATUS_READ_BYTES];

	Status = XSpi_Transfer(&Spi, WriteBuffer, rx_buffer, STATUS_READ_BYTES);
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	if ((rx_buffer[1]&0x2) != 0x2)
		return XST_FAILURE;

	return XST_SUCCESS;
}



int SpiFlashGetStatus()
{
	int Status;
	uint8_t rx_buffer[STATUS_READ_BYTES];

	// Prepare the Write Buffer.
	WriteBuffer[0] = COMMAND_STATUSREG1_READ;
	WriteBuffer[1] = 0;

	// Initiate the Transfer.
	Status = XSpi_Transfer(&Spi, WriteBuffer, rx_buffer, STATUS_READ_BYTES);
	if(Status != XST_SUCCESS)
		return XST_FAILURE;

	SpiFlash_status_reg = rx_buffer[1];

	return XST_SUCCESS;
}



int SpiFlashWaitForFlashReady()
{
	int Status;
	uint8_t flash_busy = 1;

	while(flash_busy == 1) {

		// Get the Status Register.
		Status = SpiFlashGetStatus();
		if(Status != XST_SUCCESS)
			return XST_FAILURE;

		// loop until status reg 1 bit 0 is 0;
		flash_busy = (SpiFlash_status_reg & 0x1);
	}

	return XST_SUCCESS;
}
