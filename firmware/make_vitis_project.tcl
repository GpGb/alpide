set fullpath [ file dirname [file normalize [info script]] ]

set platform  [lindex $argv 0]
set operation [lindex $argv 1]
set release_t [lindex $argv 2]

set sources_dir    "[file dirname $fullpath]/software"
set vivado_xsa     "${fullpath}/${platform}/${platform}_top.xsa"
set vitis_prj_dir  "${fullpath}/${platform}/${platform}.vitis"


proc usage {} {
	global fullpath
	puts "\n\n*** Invalid options. USAGE:"
	puts "${fullpath} <TDAQ|nexys> <generate|build> <debug|release>"
}

proc generate {} {
	puts "***** Generating NEW vitis project *****"

	global vitis_prj_dir
	global vivado_xsa
	global sources_dir
	global platform

	app create -name MB_firmware \
		-platform {TDAQ_top} \
		-domain {standalone_microblaze_0} \
		-hw  "${vivado_xsa}" \
		-sysproj {MB_firmware_system} \
		-proc {microblaze_0} \
		-template {Empty Application(C)} \
		-os {standalone} \
		-lang {c} \
		-arch {32-bit}


	importsources -name {MB_firmware} -path "${sources_dir}" \
		-linker-script -soft-link

	# for some reason -linker-script with -soft-link doesn't work properly
	# so add a link manually
	exec ln -s "${sources_dir}/lscript.ld" \
		"${vitis_prj_dir}/MB_firmware/src/lscript.ld"

	switch $platform {

		"TDAQ" {
			set plat_code 1
		}

		"nexys" {
			set plat_code 0
		}

		default {
			throw "Build Error" "No such platform: ${release_t}"
		}

	}

	# config debug
	app config -name MB_firmware build-config debug
	app config -name {MB_firmware} define-compiler-symbols PLATFORM_TYPE=$plat_code
	app config -name {MB_firmware} define-compiler-symbols PLAT_DEBUG

	#config release
	app config -name MB_firmware build-config release
	app config -name {MB_firmware} define-compiler-symbols PLATFORM_TYPE=$plat_code

}


proc update_hw {} {
	puts "***** Updating hardware specification *****"

	global vivado_xsa
	global platform
	global vitis_prj_dir

	platform read "${vitis_prj_dir}/${platform}_top/platform.spr"
	platform active "${platform}_top"
	platform config -updatehw "${vivado_xsa}"
	platform write
}


proc build {} {
	global release_t

	app config -name MB_firmware build-config [string tolower ${release_t}]
	app build -all
}


setws "${vitis_prj_dir}"

switch $operation {

	"generate" {
		if {! [file exist "${vitis_prj_dir}/${platform}_top/platform.spr"] } {
			generate
		} else {
			update_hw
		}
	}

	"build" {
		build
	}

	default {
		usage
	}

}
