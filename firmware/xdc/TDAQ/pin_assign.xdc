# Crystal oscillator (80 MHz)
set_property -dict {PACKAGE_PIN W19 IOSTANDARD LVCMOS33} [get_ports {sysclk}]; # OSC_CK

# Storage QSPI
set_property -dict {PACKAGE_PIN J14  IOSTANDARD LVCMOS33} [get_ports {qspi_sck}];
set_property -dict {PACKAGE_PIN H13  IOSTANDARD LVCMOS33} [get_ports {qspi_cs}];
set_property -dict {PACKAGE_PIN G16  IOSTANDARD LVCMOS33} [get_ports {qspi_dq[0]}]; # SI/IO0
set_property -dict {PACKAGE_PIN G13  IOSTANDARD LVCMOS33} [get_ports {qspi_dq[1]}]; # SO/IO1
set_property -dict {PACKAGE_PIN G15  IOSTANDARD LVCMOS33} [get_ports {qspi_dq[2]}]; # WP/IO2
set_property -dict {PACKAGE_PIN J16  IOSTANDARD LVCMOS33} [get_ports {qspi_dq[3]}]; # HLD/IO3

# TSP clock
set_property IOSTANDARD LVCMOS33 [get_ports ALP_clk];
set_property PACKAGE_PIN E18 [get_ports ALP_clk[0]];  # TSP_DCTRL_MCLK_1_[1]
set_property PACKAGE_PIN F19 [get_ports ALP_clk[1]];  # TSP_DCTRL_MCLK_2_[1]
set_property PACKAGE_PIN B22 [get_ports ALP_clk[2]];  # TSP_DCTRL_MCLK_3_[1]
set_property PACKAGE_PIN B20 [get_ports ALP_clk[3]];  # TSP_DCTRL_MCLK_1_[2]
set_property PACKAGE_PIN F20 [get_ports ALP_clk[4]];  # TSP_DCTRL_MCLK_2_[2]
set_property PACKAGE_PIN B21 [get_ports ALP_clk[5]];  # TSP_DCTRL_MCLK_3_[2]
set_property PACKAGE_PIN A20 [get_ports ALP_clk[6]];  # TSP_DCTRL_MCLK_1_[3]
set_property PACKAGE_PIN D20 [get_ports ALP_clk[7]];  # TSP_DCTRL_MCLK_2_[3]
set_property PACKAGE_PIN A21 [get_ports ALP_clk[8]];  # TSP_DCTRL_MCLK_3_[3]
set_property PACKAGE_PIN A18 [get_ports ALP_clk[9]];  # TSP_DCTRL_MCLK_1_[4]
set_property PACKAGE_PIN C20 [get_ports ALP_clk[10]]; # TSP_DCTRL_MCLK_2_[4]
set_property PACKAGE_PIN E22 [get_ports ALP_clk[11]]; # TSP_DCTRL_MCLK_3_[4]
set_property PACKAGE_PIN A19 [get_ports ALP_clk[12]]; # TSP_DCTRL_MCLK_1_[5]
set_property PACKAGE_PIN C22 [get_ports ALP_clk[13]]; # TSP_DCTRL_MCLK_2_[5]
set_property PACKAGE_PIN D22 [get_ports ALP_clk[14]]; # TSP_DCTRL_MCLK_3_[5]

# TSP data driver
set_property IOSTANDARD LVCMOS33 [get_ports ALP_ctrl_D];
set_property PACKAGE_PIN C13 [get_ports ALP_ctrl_D[0]];  # TSP_DCTRL_OUT_1_[1]
set_property PACKAGE_PIN A14 [get_ports ALP_ctrl_D[1]];  # TSP_DCTRL_OUT_2_[1]
set_property PACKAGE_PIN C18 [get_ports ALP_ctrl_D[2]];  # TSP_DCTRL_OUT_3_[1]
set_property PACKAGE_PIN B13 [get_ports ALP_ctrl_D[3]];  # TSP_DCTRL_OUT_1_[2]
set_property PACKAGE_PIN B17 [get_ports ALP_ctrl_D[4]];  # TSP_DCTRL_OUT_2_[2]
set_property PACKAGE_PIN C19 [get_ports ALP_ctrl_D[5]];  # TSP_DCTRL_OUT_3_[2]
set_property PACKAGE_PIN A15 [get_ports ALP_ctrl_D[6]];  # TSP_DCTRL_OUT_1_[3]
set_property PACKAGE_PIN B18 [get_ports ALP_ctrl_D[7]];  # TSP_DCTRL_OUT_2_[3]
set_property PACKAGE_PIN E19 [get_ports ALP_ctrl_D[8]];  # TSP_DCTRL_OUT_3_[3]
set_property PACKAGE_PIN A16 [get_ports ALP_ctrl_D[9]];  # TSP_DCTRL_OUT_1_[4]
set_property PACKAGE_PIN D17 [get_ports ALP_ctrl_D[10]]; # TSP_DCTRL_OUT_2_[4]
set_property PACKAGE_PIN D19 [get_ports ALP_ctrl_D[11]]; # TSP_DCTRL_OUT_3_[4]
set_property PACKAGE_PIN A13 [get_ports ALP_ctrl_D[12]]; # TSP_DCTRL_OUT_1_[5]
set_property PACKAGE_PIN C17 [get_ports ALP_ctrl_D[13]]; # TSP_DCTRL_OUT_2_[5]
set_property PACKAGE_PIN F18 [get_ports ALP_ctrl_D[14]]; # TSP_DCTRL_OUT_3_[5]

# TSP data receivers
set_property IOSTANDARD LVCMOS33 [get_ports ALP_ctrl_R];
set_property PACKAGE_PIN F15 [get_ports ALP_ctrl_R[0]];  # TSP_DCTRL_IN_1_[1]
set_property PACKAGE_PIN C14 [get_ports ALP_ctrl_R[1]];  # TSP_DCTRL_IN_2_[1]
set_property PACKAGE_PIN D16 [get_ports ALP_ctrl_R[2]];  # TSP_DCTRL_IN_3_[1]
set_property PACKAGE_PIN F13 [get_ports ALP_ctrl_R[3]];  # TSP_DCTRL_IN_1_[2]
set_property PACKAGE_PIN C15 [get_ports ALP_ctrl_R[4]];  # TSP_DCTRL_IN_2_[2]
set_property PACKAGE_PIN D14 [get_ports ALP_ctrl_R[5]];  # TSP_DCTRL_IN_3_[2]
set_property PACKAGE_PIN F14 [get_ports ALP_ctrl_R[6]];  # TSP_DCTRL_IN_1_[3]
set_property PACKAGE_PIN E13 [get_ports ALP_ctrl_R[7]];  # TSP_DCTRL_IN_2_[3]
set_property PACKAGE_PIN D15 [get_ports ALP_ctrl_R[8]];  # TSP_DCTRL_IN_3_[3]
set_property PACKAGE_PIN F16 [get_ports ALP_ctrl_R[9]];  # TSP_DCTRL_IN_1_[4]
set_property PACKAGE_PIN E14 [get_ports ALP_ctrl_R[10]]; # TSP_DCTRL_IN_2_[4]
set_property PACKAGE_PIN B15 [get_ports ALP_ctrl_R[11]]; # TSP_DCTRL_IN_3_[4]
set_property PACKAGE_PIN E17 [get_ports ALP_ctrl_R[12]]; # TSP_DCTRL_IN_1_[5]
set_property PACKAGE_PIN E16 [get_ports ALP_ctrl_R[13]]; # TSP_DCTRL_IN_2_[5]
set_property PACKAGE_PIN B16 [get_ports ALP_ctrl_R[14]]; # TSP_DCTRL_IN_3_[5]

# TSP data D/R enable/disable
set_property IOSTANDARD LVCMOS33 [get_ports ALP_ctrl_T];
set_property PACKAGE_PIN AB22 [get_ports ALP_ctrl_T[0]];  # TSP_DCTRL_OUT_EN_1_[1]
set_property PACKAGE_PIN Y19  [get_ports ALP_ctrl_T[1]];  # TSP_DCTRL_OUT_EN_2_[1]
set_property PACKAGE_PIN V17  [get_ports ALP_ctrl_T[2]];  # TSP_DCTRL_OUT_EN_3_[1]
set_property PACKAGE_PIN U20  [get_ports ALP_ctrl_T[3]];  # TSP_DCTRL_OUT_EN_1_[2]
set_property PACKAGE_PIN V18  [get_ports ALP_ctrl_T[4]];  # TSP_DCTRL_OUT_EN_2_[2]
set_property PACKAGE_PIN W17  [get_ports ALP_ctrl_T[5]];  # TSP_DCTRL_OUT_EN_3_[2]
set_property PACKAGE_PIN V20  [get_ports ALP_ctrl_T[6]];  # TSP_DCTRL_OUT_EN_1_[3]
set_property PACKAGE_PIN V19  [get_ports ALP_ctrl_T[7]];  # TSP_DCTRL_OUT_EN_2_[3]
set_property PACKAGE_PIN AA18 [get_ports ALP_ctrl_T[8]];  # TSP_DCTRL_OUT_EN_3_[3]
set_property PACKAGE_PIN W20  [get_ports ALP_ctrl_T[9]];  # TSP_DCTRL_OUT_EN_1_[4]
set_property PACKAGE_PIN AA19 [get_ports ALP_ctrl_T[10]]; # TSP_DCTRL_OUT_EN_2_[4]
set_property PACKAGE_PIN AB18 [get_ports ALP_ctrl_T[11]]; # TSP_DCTRL_OUT_EN_3_[4]
set_property PACKAGE_PIN Y18  [get_ports ALP_ctrl_T[12]]; # TSP_DCTRL_OUT_EN_1_[5]
set_property PACKAGE_PIN AB20 [get_ports ALP_ctrl_T[13]]; # TSP_DCTRL_OUT_EN_2_[5]
set_property PACKAGE_PIN U17  [get_ports ALP_ctrl_T[14]]; # TSP_DCTRL_OUT_EN_3_[5]


# TSP analog power enable
set_property IOSTANDARD LVCMOS18 [get_ports TSP_EN_A];
set_property PACKAGE_PIN H2 [get_ports TSP_EN_A[0]];  # TSP_EN_A_1_[1]
set_property PACKAGE_PIN H5 [get_ports TSP_EN_A[1]];  # TSP_EN_A_2_[1]
set_property PACKAGE_PIN K4 [get_ports TSP_EN_A[2]];  # TSP_EN_A_3_[1]
set_property PACKAGE_PIN G2 [get_ports TSP_EN_A[3]];  # TSP_EN_A_1_[2]
set_property PACKAGE_PIN H3 [get_ports TSP_EN_A[4]];  # TSP_EN_A_2_[2]
set_property PACKAGE_PIN J4 [get_ports TSP_EN_A[5]];  # TSP_EN_A_3_[2]
set_property PACKAGE_PIN K2 [get_ports TSP_EN_A[6]];  # TSP_EN_A_1_[3]
set_property PACKAGE_PIN G3 [get_ports TSP_EN_A[7]];  # TSP_EN_A_2_[3]
set_property PACKAGE_PIN L3 [get_ports TSP_EN_A[8]];  # TSP_EN_A_3_[3]
set_property PACKAGE_PIN J2 [get_ports TSP_EN_A[9]];  # TSP_EN_A_1_[4]
set_property PACKAGE_PIN H4 [get_ports TSP_EN_A[10]]; # TSP_EN_A_2_[4]
set_property PACKAGE_PIN K3 [get_ports TSP_EN_A[11]]; # TSP_EN_A_3_[4]
set_property PACKAGE_PIN J5 [get_ports TSP_EN_A[12]]; # TSP_EN_A_1_[5]
set_property PACKAGE_PIN G4 [get_ports TSP_EN_A[13]]; # TSP_EN_A_2_[5]
set_property PACKAGE_PIN M1 [get_ports TSP_EN_A[14]]; # TSP_EN_A_3_[5]


# TSP digital power enable
set_property IOSTANDARD LVCMOS18 [get_ports TSP_EN_D];
set_property PACKAGE_PIN F4 [get_ports TSP_EN_D[0]];  # TSP_EN_D_1_[1]
set_property PACKAGE_PIN E1 [get_ports TSP_EN_D[1]];  # TSP_EN_D_2_[1]
set_property PACKAGE_PIN F1 [get_ports TSP_EN_D[2]];  # TSP_EN_D_3_[1]
set_property PACKAGE_PIN B1 [get_ports TSP_EN_D[3]];  # TSP_EN_D_1_[2]
set_property PACKAGE_PIN D1 [get_ports TSP_EN_D[4]];  # TSP_EN_D_2_[2]
set_property PACKAGE_PIN F3 [get_ports TSP_EN_D[5]];  # TSP_EN_D_3_[2]
set_property PACKAGE_PIN A1 [get_ports TSP_EN_D[6]];  # TSP_EN_D_1_[3]
set_property PACKAGE_PIN E2 [get_ports TSP_EN_D[7]];  # TSP_EN_D_2_[3]
set_property PACKAGE_PIN E3 [get_ports TSP_EN_D[8]];  # TSP_EN_D_3_[3]
set_property PACKAGE_PIN C2 [get_ports TSP_EN_D[9]];  # TSP_EN_D_1_[4]
set_property PACKAGE_PIN D2 [get_ports TSP_EN_D[10]]; # TSP_EN_D_2_[4]
set_property PACKAGE_PIN K1 [get_ports TSP_EN_D[11]]; # TSP_EN_D_3_[4]
set_property PACKAGE_PIN B2 [get_ports TSP_EN_D[12]]; # TSP_EN_D_1_[5]
set_property PACKAGE_PIN G1 [get_ports TSP_EN_D[13]]; # TSP_EN_D_2_[5]
set_property PACKAGE_PIN J1 [get_ports TSP_EN_D[14]]; # TSP_EN_D_3_[5]


# TSP bias power enable
set_property IOSTANDARD LVCMOS33 [get_ports TSP_EN_BIAS];
set_property PACKAGE_PIN W5  [get_ports TSP_EN_BIAS[0]];  # TSP_EN_BIAS_1_[1]
set_property PACKAGE_PIN Y6  [get_ports TSP_EN_BIAS[1]];  # TSP_EN_BIAS_2_[1]
set_property PACKAGE_PIN AB6 [get_ports TSP_EN_BIAS[2]];  # TSP_EN_BIAS_3_[1]
set_property PACKAGE_PIN U6  [get_ports TSP_EN_BIAS[3]];  # TSP_EN_BIAS_1_[2]
set_property PACKAGE_PIN AA6 [get_ports TSP_EN_BIAS[4]];  # TSP_EN_BIAS_2_[2]
set_property PACKAGE_PIN V9  [get_ports TSP_EN_BIAS[5]];  # TSP_EN_BIAS_3_[2]
set_property PACKAGE_PIN V5  [get_ports TSP_EN_BIAS[6]];  # TSP_EN_BIAS_1_[3]
set_property PACKAGE_PIN V7  [get_ports TSP_EN_BIAS[7]];  # TSP_EN_BIAS_2_[3]
set_property PACKAGE_PIN V8  [get_ports TSP_EN_BIAS[8]];  # TSP_EN_BIAS_3_[3]
set_property PACKAGE_PIN R6  [get_ports TSP_EN_BIAS[9]];  # TSP_EN_BIAS_1_[4]
set_property PACKAGE_PIN W7  [get_ports TSP_EN_BIAS[10]]; # TSP_EN_BIAS_2_[4]
set_property PACKAGE_PIN AA8 [get_ports TSP_EN_BIAS[11]]; # TSP_EN_BIAS_3_[4]
set_property PACKAGE_PIN T6  [get_ports TSP_EN_BIAS[12]]; # TSP_EN_BIAS_1_[5]
set_property PACKAGE_PIN AB7 [get_ports TSP_EN_BIAS[13]]; # TSP_EN_BIAS_2_[5]
set_property PACKAGE_PIN AB8 [get_ports TSP_EN_BIAS[14]]; # TSP_EN_BIAS_3_[5]


# TSP power good flags
set_property IOSTANDARD LVCMOS18 [get_ports TSP_pwr_good];
set_property PACKAGE_PIN L1 [get_ports TSP_pwr_good[0]];  # TSP_FLAG_1_[1]
set_property PACKAGE_PIN L5 [get_ports TSP_pwr_good[1]];  # TSP_FLAG_2_[1]
set_property PACKAGE_PIN P1 [get_ports TSP_pwr_good[2]];  # TSP_FLAG_3_[1]
set_property PACKAGE_PIN M3 [get_ports TSP_pwr_good[3]];  # TSP_FLAG_1_[2]
set_property PACKAGE_PIN L4 [get_ports TSP_pwr_good[4]];  # TSP_FLAG_2_[2]
set_property PACKAGE_PIN P5 [get_ports TSP_pwr_good[5]];  # TSP_FLAG_3_[2]
set_property PACKAGE_PIN M2 [get_ports TSP_pwr_good[6]];  # TSP_FLAG_1_[3]
set_property PACKAGE_PIN N4 [get_ports TSP_pwr_good[7]];  # TSP_FLAG_2_[3]
set_property PACKAGE_PIN P4 [get_ports TSP_pwr_good[8]];  # TSP_FLAG_3_[3]
set_property PACKAGE_PIN K6 [get_ports TSP_pwr_good[9]];  # TSP_FLAG_1_[4]
set_property PACKAGE_PIN N3 [get_ports TSP_pwr_good[10]]; # TSP_FLAG_2_[4]
set_property PACKAGE_PIN P2 [get_ports TSP_pwr_good[11]]; # TSP_FLAG_3_[4]
set_property PACKAGE_PIN J6 [get_ports TSP_pwr_good[12]]; # TSP_FLAG_1_[5]
set_property PACKAGE_PIN R1 [get_ports TSP_pwr_good[13]]; # TSP_FLAG_2_[5]
set_property PACKAGE_PIN N2 [get_ports TSP_pwr_good[14]]; # TSP_FLAG_3_[5]


# Thermometers
set_property IOSTANDARD LVCMOS33 [get_ports onewire];
set_property PULLUP true [get_ports onewire];
set_property PACKAGE_PIN T3  [get_ports onewire[0]];  # TSP_THERM_1_[1]
set_property PACKAGE_PIN R3  [get_ports onewire[1]];  # TSP_THERM_2_[1]
set_property PACKAGE_PIN Y1  [get_ports onewire[2]];  # TSP_THERM_3_[1]
set_property PACKAGE_PIN T1  [get_ports onewire[3]];  # TSP_THERM_1_[2]
set_property PACKAGE_PIN R2  [get_ports onewire[4]];  # TSP_THERM_2_[2]
set_property PACKAGE_PIN U3  [get_ports onewire[5]];  # TSP_THERM_3_[2]
set_property PACKAGE_PIN U1  [get_ports onewire[6]];  # TSP_THERM_1_[3]
set_property PACKAGE_PIN W2  [get_ports onewire[7]];  # TSP_THERM_2_[3]
set_property PACKAGE_PIN V3  [get_ports onewire[8]];  # TSP_THERM_3_[3]
set_property PACKAGE_PIN U2  [get_ports onewire[9]];  # TSP_THERM_1_[4]
set_property PACKAGE_PIN Y2  [get_ports onewire[10]]; # TSP_THERM_2_[4]
set_property PACKAGE_PIN AA1 [get_ports onewire[11]]; # TSP_THERM_3_[4]
set_property PACKAGE_PIN V2  [get_ports onewire[12]]; # TSP_THERM_1_[5]
set_property PACKAGE_PIN W1  [get_ports onewire[13]]; # TSP_THERM_2_[5]
set_property PACKAGE_PIN AB1 [get_ports onewire[14]]; # TSP_THERM_3_[5]
set_property PACKAGE_PIN Y8  [get_ports onewire[15]]; # LOC_THERM


# Trigger interface (CDS1)
set_property -dict {IOSTANDARD LVCMOS33  PULLDOWN true  PACKAGE_PIN Y22} [get_ports trig_evnt]; # EVNT_TRIGGER
set_property -dict {IOSTANDARD LVCMOS33  PULLDOWN true} [get_ports trig_in];
set_property PACKAGE_PIN W21  [get_ports trig_in[0]]; # TRIGGER_T_1
set_property PACKAGE_PIN W22  [get_ports trig_in[1]]; # TRIGGER_T_2
set_property PACKAGE_PIN AA20 [get_ports trig_in[2]]; # TRIGGER_T_3
set_property PACKAGE_PIN AA21 [get_ports trig_in[3]]; # TRIGGER_T_4
set_property PACKAGE_PIN Y21  [get_ports trig_in[4]]; # TRIGGER_T_5
set_property -dict {PACKAGE_PIN AB21  IOSTANDARD LVCMOS33} [get_ports busy_out]; # BUSY


# spacewire (CSW)
set_property -dict {PACKAGE_PIN T21 IOSTANDARD LVCMOS33} [get_ports {spw_do}]; # SPW_DATAOUT
set_property -dict {PACKAGE_PIN V22 IOSTANDARD LVCMOS33} [get_ports {spw_so}]; # SPW_STRBOUT
set_property -dict {PACKAGE_PIN M16 IOSTANDARD LVCMOS33} [get_ports {spw_di}]; # SPW_DATAIN
set_property -dict {PACKAGE_PIN M17 IOSTANDARD LVCMOS33} [get_ports {spw_si}]; # SPW_STRBIN
# EQM TDAQ
#set_property -dict {PACKAGE_PIN P21 IOSTANDARD LVCMOS33} [get_ports {spw_di}]; # SPW_DATAIN
#set_property -dict {PACKAGE_PIN R21 IOSTANDARD LVCMOS33} [get_ports {spw_si}]; # SPW_STRBIN

# Reset/PPS/GPIO (CSD2)
set_property -dict {PACKAGE_PIN U21 IOSTANDARD LVCMOS33} [get_ports reset];   # RESET
set_property -dict {PACKAGE_PIN R19 IOSTANDARD LVCMOS33} [get_ports PPS];     # PPS
set_property -dict {PACKAGE_PIN P19 IOSTANDARD LVCMOS33} [get_ports gpio_1];  # GPIO_1
set_property -dict {PACKAGE_PIN T20 IOSTANDARD LVCMOS33} [get_ports gpio_2];  # GPIO_2


# SRAM
set_property -dict {IOSTANDARD LVCMOS33 PACKAGE_PIN H14} [get_ports SRAM_BLE]; # SM_BLE
set_property -dict {IOSTANDARD LVCMOS33 PACKAGE_PIN G18} [get_ports SRAM_BHE]; # SM_BHE
set_property -dict {IOSTANDARD LVCMOS33 PACKAGE_PIN G17} [get_ports SRAM_OE];  # SM_OE
set_property -dict {IOSTANDARD LVCMOS33 PACKAGE_PIN H15} [get_ports SRAM_WE];  # SM_WE
set_property -dict {IOSTANDARD LVCMOS33 PACKAGE_PIN J15} [get_ports SRAM_CE];  # SM_CE

set_property IOSTANDARD LVCMOS33 [get_ports SRAM_A];
set_property PACKAGE_PIN H17 [get_ports SRAM_A[0]];  # SM_A_0
set_property PACKAGE_PIN H18 [get_ports SRAM_A[1]];  # SM_A_1
set_property PACKAGE_PIN J22 [get_ports SRAM_A[2]];  # SM_A_2
set_property PACKAGE_PIN H22 [get_ports SRAM_A[3]];  # SM_A_3
set_property PACKAGE_PIN H20 [get_ports SRAM_A[4]];  # SM_A_4
set_property PACKAGE_PIN G20 [get_ports SRAM_A[5]];  # SM_A_5
set_property PACKAGE_PIN K21 [get_ports SRAM_A[6]];  # SM_A_6
set_property PACKAGE_PIN K22 [get_ports SRAM_A[7]];  # SM_A_7
set_property PACKAGE_PIN M21 [get_ports SRAM_A[8]];  # SM_A_8
set_property PACKAGE_PIN L21 [get_ports SRAM_A[9]];  # SM_A_9
set_property PACKAGE_PIN J20 [get_ports SRAM_A[10]]; # SM_A_10
set_property PACKAGE_PIN J21 [get_ports SRAM_A[11]]; # SM_A_11
set_property PACKAGE_PIN J19 [get_ports SRAM_A[12]]; # SM_A_12
set_property PACKAGE_PIN H19 [get_ports SRAM_A[13]]; # SM_A_13
set_property PACKAGE_PIN K18 [get_ports SRAM_A[14]]; # SM_A_14
set_property PACKAGE_PIN K19 [get_ports SRAM_A[15]]; # SM_A_15
set_property PACKAGE_PIN L19 [get_ports SRAM_A[16]]; # SM_A_16
set_property PACKAGE_PIN L20 [get_ports SRAM_A[17]]; # SM_A_17
set_property PACKAGE_PIN N22 [get_ports SRAM_A[18]]; # SM_A_18
set_property PACKAGE_PIN M22 [get_ports SRAM_A[19]]; # SM_A_19
set_property PACKAGE_PIN M18 [get_ports SRAM_A[20]]; # SM_A_20

set_property IOSTANDARD LVCMOS33 [get_ports SRAM_IO];
set_property PACKAGE_PIN L18 [get_ports SRAM_IO[0]];  # SM_IO_0
set_property PACKAGE_PIN N18 [get_ports SRAM_IO[1]];  # SM_IO_1
set_property PACKAGE_PIN N19 [get_ports SRAM_IO[2]];  # SM_IO_2
set_property PACKAGE_PIN N20 [get_ports SRAM_IO[3]];  # SM_IO_3
set_property PACKAGE_PIN M20 [get_ports SRAM_IO[4]];  # SM_IO_4
set_property PACKAGE_PIN K13 [get_ports SRAM_IO[5]];  # SM_IO_5
set_property PACKAGE_PIN K14 [get_ports SRAM_IO[6]];  # SM_IO_6
set_property PACKAGE_PIN M13 [get_ports SRAM_IO[7]];  # SM_IO_7
set_property PACKAGE_PIN L13 [get_ports SRAM_IO[8]];  # SM_IO_8
set_property PACKAGE_PIN K17 [get_ports SRAM_IO[9]];  # SM_IO_9
set_property PACKAGE_PIN J17 [get_ports SRAM_IO[10]]; # SM_IO_10
set_property PACKAGE_PIN L14 [get_ports SRAM_IO[11]]; # SM_IO_11
set_property PACKAGE_PIN L15 [get_ports SRAM_IO[12]]; # SM_IO_12
set_property PACKAGE_PIN L16 [get_ports SRAM_IO[13]]; # SM_IO_13
set_property PACKAGE_PIN K16 [get_ports SRAM_IO[14]]; # SM_IO_14
set_property PACKAGE_PIN M15 [get_ports SRAM_IO[15]]; # SM_IO_15


# SEM mon shim
set_property -dict {PACKAGE_PIN R17 IOSTANDARD LVCMOS33} [get_ports {SEM_mon_tx}]; # DEBUG_13
set_property -dict {PACKAGE_PIN N15 IOSTANDARD LVCMOS33} [get_ports {SEM_mon_rx}]; # DEBUG_14


# Configuration options, can be used for all designs
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]

set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]

set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.CONFIGFALLBACK ENABLE [current_design]
#set_property BITSTREAM.CONFIG.NEXT_CONFIG_ADDR 32'h00400000 [current_design]
