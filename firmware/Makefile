# VARS

# "TDAQ" or "nexys"
prj     = TDAQ
# "Debug" or "Release
release = Debug
jobs    = $(shell nproc)


define TCL_SYTH_SCRPT
reset_run synth_1
launch_runs synth_1 -jobs ${jobs}
wait_on_run synth_1
launch_runs impl_1 -to_step write_bitstream -jobs ${jobs}
wait_on_run impl_1

write_hw_platform -fixed -force -include_bit ${prj}/${prj}_top.xsa
endef
export TCL_SYTH_SCRPT

# taget handles
vivado.xpr  = ${prj}/${prj}.xpr
synth.bit   = ${prj}/${prj}.runs/impl_1/TDAQ_top.bit
synth.mmi   = ${prj}/${prj}.runs/impl_1/TDAQ_top.mmi
vitis.prj   = ${prj}/${prj}.vitis/${prj}_top/platform.spr
MB_firm.elf = ${prj}/${prj}.vitis/MB_firmware/${release}/MB_firmware.elf

# "aliases"
.PHONY : vivado.xpr
vivado.xpr: $(vivado.xpr)
.PHONY : synth.bit
synth.bit: $(synth.bit)
.PHONY : vitis.prj
vitis.prj: $(vitis.prj)
.PHONY : MB_firm.elf
MB_firm.elf: $(MB_firm.elf)


$(vivado.xpr):
	vivado -mode batch -source "generate_project_${prj}.tcl"


$(synth.bit): $(vivado.xpr) ./src/common ./src/${prj} ./xdc/${prj}
	@echo "$$TCL_SYTH_SCRPT" > ${prj}/vivado_syth.tcl
	vivado -mode batch -source ${prj}/vivado_syth.tcl "${prj}/${prj}.xpr"


$(vitis.prj): $(synth.bit)
	xsct make_vitis_project.tcl ${prj} generate ${release}
	-patch -Np1 <bsp_extra/hw_exception_handler.patch


$(MB_firm.elf): $(vitis.prj) ../software
	xsct make_vitis_project.tcl ${prj} build ${release}
# make sure vitis.prj in not newer immediately after build..
	@touch ${MB_firm.elf}


$(prj).bit: $(synth.bit) $(MB_firm.elf)
	updatemem \
		-meminfo ${synth.mmi} \
		-data ${MB_firm.elf} \
		-proc MCU_inst/microblaze_0 \
		-bit ${synth.bit} \
		-out ${prj}.bit \
		-force


cleanup:
	rm -rv *.jou *.log .Xil

clean:
	rm -rv *.jou *.log .Xil
	rm -v ${prj}.bit ${MB_firm.elf}
	rm -vi ${synth.bit}
