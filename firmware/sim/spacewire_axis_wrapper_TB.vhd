library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.spwpkg.all;
use work.types_pkg.all;

entity spacewire_axis_wrapper_tb is
end spacewire_axis_wrapper_tb;

architecture behavior of spacewire_axis_wrapper_tb is

	-- clock & constants
	constant sysfreq: real := 80.0e6;
	constant clk_period : time := 12.5 ns;
	signal clk : std_logic := '0';

	constant test_data_len : natural := 10;

	constant spw_write_cmd : std_logic_vector(7 downto 0) := x"0c";
	constant spw_read_cmd  : std_logic_vector(7 downto 0) := x"03";
	constant spw_bread_cmd : std_logic_vector(7 downto 0) := x"ee";

	constant data_dpram_baseaddr : std_logic_vector(31 downto 0) := x"0000_1000";
	constant mcu_dpram_baseaddr : std_logic_vector(31 downto 0) := x"0000_2000";

	-- uut pixel data in inface
	signal rst : std_logic;
	signal pkgr_valid, pkgr_ready, pkgr_last : std_logic;
	signal pkgr_data : std_logic_vector(31 downto 0);
	signal event_ready, event_clear : std_logic := '1';

	signal mcu_bram_addr, mcu_bram_do : std_logic_vector(31 downto 0);

	-- spacewire mastr
	signal spw_di, spw_si : std_logic;
	signal spw_do, spw_so : std_logic;
	signal rxvalid, txrdy  : std_logic;
	signal rxread, txwrite : std_logic := '0';
	signal rxflag, txflag  : std_logic := '0';
	signal rxdata, txdata  : std_logic_vector(7 downto 0) := (others => '0');
	signal started, connecting, running : std_logic;
	signal errdisc, errpar, erresc, errcred : std_logic;

	-- test signals
	signal spwpkt_rcv_data : std_logic_vector(31 downto 0) := (others => '0');

begin

	uut: entity work.spw_axis_wrapper
	generic map (
		sysfreq => sysfreq
	)
	PORT MAP (
		clk => clk,
		rst => rst,
		rst_daq => rst,
		-- spw pins
		spw_di => spw_di,
		spw_si => spw_si,
		spw_do => spw_do,
		spw_so => spw_so,
		-- gpio spw
		event_ready => event_ready,
		event_clear => event_clear,
		-- data AXIS interface
		pkgr_data  => pkgr_data,
		pkgr_valid => pkgr_valid,
		pkgr_ready => pkgr_ready,
		pkgr_last  => pkgr_last,
		-- MCU dpram iface
		mcu_bram_we   => open,
		mcu_bram_addr => mcu_bram_addr,
		mcu_bram_di   => open,
		mcu_bram_do   => mcu_bram_do,
		-- cmd
		daq_run  => open,
		cmd_intr => open,
		rst_reg  => open,
		-- TSP switches
		TSP_EN_A     =>	open,
		TSP_EN_D     =>	open,
		TSP_EN_BIAS  =>	open,
		TSP_pwr_good =>	(others => '0'),
		-- other flags
		busy         => '0',
		SEM_flags    => (others => '0'),
		trig_count   => (others => '0'),
		busy_viol_cnt=> (others => '0'),
		timestamp    => (others => '0'),
		cpu_alarms   => (others => '0'),
		trigger_lut  => open
	);


	master_spwstream_inst: entity work.spwstream
	generic map (
		sysfreq         => sysfreq,
		txclkfreq       => sysfreq,
		rximpl          => impl_generic,
		rxchunk         => 1,
		tximpl          => impl_generic,
		rxfifosize_bits => 6,
		txfifosize_bits => 6
	)
	port map (
		clk         => clk,
		rxclk       => clk,
		txclk       => clk,
		rst         => '0',
		autostart   => '1',
		linkstart   => '1',
		linkdis     => '0',
		txdivcnt    => x"01",
		tick_in     => '0',
		ctrl_in     => (others => '0'),
		time_in     => (others => '0'),
		txwrite     => txwrite,
		txflag      => txflag,
		txdata      => txdata,
		txrdy       => txrdy,
		txhalff     => open,
		tick_out    => open,
		ctrl_out    => open,
		time_out    => open,
		rxvalid     => rxvalid,
		rxhalff     => open,
		rxflag      => rxflag,
		rxdata      => rxdata,
		rxread      => rxread,
		started     => started,
		connecting  => connecting,
		running     => running,
		errdisc     => errdisc,
		errpar      => errpar,
		erresc      => erresc,
		errcred     => errcred,
		spw_di      => spw_do,
		spw_si      => spw_so,
		spw_do      => spw_di,
		spw_so      => spw_si
	);



	clk <= not clk after clk_period/2;

	-- simple mcu dategen
	mcu_bram_do <= x"fafa" & mcu_bram_addr(15 downto 0);

	-- generates spacewire commands to uut
	spwr_master: process

		-- send a spacewire command (10 bytes)
		procedure send_packet(
			cmd     : in std_logic_vector(7 downto 0);
			address : in std_logic_vector(31 downto 0);
			data    : in std_logic_vector(31 downto 0)
		) is
		begin

			txflag  <= '0';
			txwrite <= '1';

			-- send cmd
			txdata  <= cmd;
			wait for clk_period;

			-- send address
			for n in 0 to 3 loop
				txdata  <= address(7+8*n downto 8*n);
				wait for clk_period;
			end loop;

			-- send data
			for n in 0 to 3 loop
				txdata  <= data(7+8*n downto 8*n);
				wait for clk_period;
			end loop;

			-- EOP
			txflag  <= '1';
			txdata  <= (others => '0');
			wait for clk_period;

			-- stop write
			txflag  <= '0';
			txwrite <= '0';
			wait for clk_period;

		end procedure send_packet;


		-- read a spacewire packet (10 bytes) from uut
		procedure rcv_packet(
			exp_cmd : in std_logic_vector(7 downto 0);
			address : out std_logic_vector(31 downto 0);
			data    : out std_logic_vector(31 downto 0)
		) is
		begin

			-- read response cmd
			wait until rxvalid = '1';
			rxread <= '1';
			assert rxdata = exp_cmd report "rcv command mismatch" severity failure;
			wait for clk_period;

			-- read response address
			for n in 0 to 3 loop
				wait until rxvalid = '1' for clk_period*41;
				assert rxvalid = '1' report "rxvalid not asserted" severity failure;

				address(7+8*n downto 8*n) := rxdata;
				wait for clk_period;
			end loop;

			-- read response data
			for n in 0 to 3 loop
				wait until rxvalid = '1' for clk_period*41;
				assert rxvalid = '1' report "rxvalid not asserted" severity failure;

				data(7+8*n downto 8*n) := rxdata;
				wait for clk_period;
			end loop;

			-- read EoP
			wait until rxvalid = '1' for clk_period*41;
			assert rxvalid = '1' report "rxvalid not asserted" severity failure;
			assert (rxflag = '1' and rxdata = x"00") report "missing EOP";
			wait for clk_period;

			rxread <= '0';

			wait for clk_period;

		end procedure rcv_packet;


		-- write a single register
		procedure write_register (
			address : in std_logic_vector(31 downto 0);
			data    : in std_logic_vector(31 downto 0)
		) is
		begin

			send_packet(spw_write_cmd, address, data);

		end procedure write_register;


		-- read a single register
		procedure read_register (
				address : in std_logic_vector(31 downto 0)
			) is
			variable rcv_addr : std_logic_vector(31 downto 0);
			variable rcv_data : std_logic_vector(31 downto 0);
		begin

			send_packet(spw_read_cmd, address, x"00000000");
			rcv_packet(spw_read_cmd, rcv_addr, rcv_data);

			assert rcv_addr = address report "rcv wrong address from read" severity failure;
			spwpkt_rcv_data <= rcv_data;
			wait for clk_period;

		end procedure read_register;


		-- burst read of a range of registers
		procedure read_block (
				address : in std_logic_vector(31 downto 0);
				payload : in std_logic_vector(31 downto 0)
			) is
			variable rcv_data  : std_logic_vector(31 downto 0);
			variable rcv_addr  : std_logic_vector(31 downto 0);
		begin

			-- write 'sandbox' register with check data
			write_register(x"0000_0005", x"deadbeef");

			-- send readblock command
			send_packet(spw_bread_cmd, address, payload);

			-- read response packets
			for i in 0 to slv2uint(payload)-1 loop

				rcv_packet(spw_bread_cmd, rcv_addr, rcv_data);
				assert unsigned(rcv_addr) = unsigned(address) + to_unsigned(i, address'length)
					report "recv bad addr in bread"	severity error;

				spwpkt_rcv_data <= rcv_data;

			end loop;

			-- read check data to make sure readblock is not sending
			-- any unexpected packets
			read_register(x"0000_0005");
			assert spwpkt_rcv_data = x"deadbeef" report "unexpected data after readblock"
				severity failure;

		end procedure read_block;


	begin

		wait until running = '1' for 30 us;
		assert running = '1' report "failed to start link" severity failure;

		wait for clk_period*100;

		-------------------------------------------
		-- read and write "sandbox" register
		-------------------------------------------
		write_register(x"0000_0005", x"deadbeef");
		wait for clk_period*50;
		read_register(x"0000_0005");
		assert spwpkt_rcv_data = x"deadbeef" report "fail reg r/w test 1" severity failure;

		wait for clk_period*50;
		read_register(x"0000_2001");
		assert spwpkt_rcv_data /= x"00000000" report "fail reg r/w test 2" severity failure;
		wait for clk_period*50;
		report "sandbox rw DONE" severity note;


		-------------------------------------------
		-- event data read polling mode
		-------------------------------------------
		read_register(data_dpram_baseaddr);
		while (spwpkt_rcv_data = x"0000_0000") loop
			read_register(data_dpram_baseaddr);
		end loop;
		assert spwpkt_rcv_data = uint2slv(test_data_len, spwpkt_rcv_data'length)
			report "unexpected data length" severity failure;

		-- read the data
		wait for clk_period;

		-- read data len
		read_register(data_dpram_baseaddr);
		assert spwpkt_rcv_data = uint2slv(test_data_len, pkgr_data'length)
				report "found wrong data len value" severity failure;

		for i in 1 to test_data_len loop
			read_register(std_logic_vector(
				unsigned(data_dpram_baseaddr) + to_unsigned(i, data_dpram_baseaddr'length)
			));
			assert spwpkt_rcv_data = uint2slv(i, pkgr_data'length)
				report "read bad data" severity failure;
		end loop;

		wait for clk_period*50;
		report "polled event read DONE" severity note;


		-------------------------------------------
		-- event data read readblock mode
		-------------------------------------------
		for i in 0 to 2 loop
			wait until event_ready = '1' for clk_period*50;
			assert event_ready = '1' report "event ready" severity failure;
			wait for clk_period*5;

			event_clear <= '0';
			read_block(std_logic_vector(unsigned(data_dpram_baseaddr)+1),
				uint2slv(test_data_len, 32));
			wait for clk_period*50;

			-- clear dpram
			write_register(data_dpram_baseaddr, x"0000_0000");
			wait until event_ready = '0';
			wait for clk_period*10;
			event_clear <= '1';
			wait for clk_period * 5;

		end loop;

		report "readblock DONE" severity note;


		-------------------------------------------
		-- burst read of MCU area
		-------------------------------------------
		read_block(mcu_dpram_baseaddr, x"0000_0008");
		report "mcu readblock DONE" severity note;

		wait;
	end process;



	-- Process controlling the pkgr_data interface
	-- this will constantly fill the data dpram
	datagen: process
	begin

		pkgr_valid <= '0';
		pkgr_last  <= '0';

		-- reset cycle
		rst <= '0';
		wait for clk_period*16;
		rst <= '1';
		wait for clk_period*16;
		rst <= '0';
		wait for clk_period*16;

		while true loop
			assert pkgr_ready = '1' report "pkgr data input not ready" severity failure;

			pkgr_valid <= '1';

			for i in 1 to test_data_len loop

				pkgr_data <= uint2slv(i, pkgr_data'length);
				assert pkgr_ready = '1' report "pkgr not ready";

				if i = test_data_len then
					pkgr_last <= '1';
				end if;

				wait for clk_period;
			end loop;
			pkgr_valid <= '0';
			pkgr_last <= '0';

			wait until pkgr_ready = '1';

		end loop;

		wait;
	end process;


end;
