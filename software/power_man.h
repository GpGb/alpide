#ifndef SRC_POWER_MAN_H_
#define SRC_POWER_MAN_H_

int pm_init_power_system();
int pm_tsp_poweroff();
int pm_tsp_poweron();
int pm_tsp_switch_set(uint8_t staveid, uint8_t channel, uint8_t setrst);
int pm_dump_iicmux_regs();

#endif
