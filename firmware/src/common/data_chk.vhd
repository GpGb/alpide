-- Simple protocol checker for implementation debugging.
-- Might be included in readout FSM in future.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.types_pkg.all;

entity data_chk is
	generic(
		n_chip : natural := 14
	);
	port(
		clk  : in std_logic;
		rst  : in std_logic;
		data : in std_logic_vector(23 downto 0);
		data_valid : in std_logic;
		debug_trigger : out std_logic
	);
end data_chk;


architecture Behavioral of data_chk is

	type chkfsm_state is (chip_empty, chip_header, region_header, chip_data, chip_trailer, unknown);
	signal chk_state, wordtype : chkfsm_state := chip_empty;

	signal chip_cnt : natural range 0 to n_chip+1 := 0;

	--attribute mark_debug : string;
	--attribute mark_debug of debug_trigger : signal is "true";
	--attribute mark_debug of chk_state : signal is "true";
	--attribute mark_debug of wordtype : signal is "true";
	--attribute mark_debug of data : signal is "true";
	--attribute mark_debug of data_valid : signal is "true";
	--attribute mark_debug of chip_cnt : signal is "true";

begin

	process(clk)
	begin
		if rising_edge(clk) then
			debug_trigger <= '0';

			if chip_cnt = n_chip+1 then
				debug_trigger <= '1';
			end if;

			if data_valid = '1' then

				if data(23 downto 20) /= x"f" then
					case chk_state is

						when chip_empty =>
							if wordtype = chip_empty then
								chk_state <= chip_empty;
							elsif wordtype = chip_header then
								chk_state <= chip_header;
							else
								debug_trigger <= '1';
							end if;

						when chip_header =>
							if wordtype = region_header then
								chk_state <= region_header;
							else
								debug_trigger <= '1';
							end if;

						when region_header =>
							if wordtype = chip_data then
								chk_state <= chip_data;
							else
								debug_trigger <= '1';
							end if;

						when chip_data =>
							if wordtype = chip_data then
								chk_state <= chip_data;
							elsif wordtype = region_header then
								chk_state <= region_header;
							elsif wordtype = chip_trailer then
								chk_state <= chip_trailer;
							else
								debug_trigger <= '1';
							end if;

						when chip_trailer =>
							if wordtype = chip_empty then
								chk_state <= chip_empty;
							elsif wordtype = chip_header then
								chk_state <= chip_header;
							else
								debug_trigger <= '1';
							end if;

						when unknown=>
							chk_state <= wordtype;

					end case;

					if wordtype = unknown then
						chk_state <= unknown;
						debug_trigger <= '1';
					end if;

					if wordtype = chip_empty or wordtype = chip_header then
						chip_cnt <= chip_cnt + 1;
					end if;

				elsif (chk_state /= chip_empty and chk_state /= chip_trailer) then
					debug_trigger <= '1';
				else
					chip_cnt <= 0;
					if chip_cnt /= n_chip  and chip_cnt /= 0 then
						debug_trigger <= '1';
					end if;
				end if;

			end if;

			if rst = '1' then
				chip_cnt <= 0;
				debug_trigger <= '0';
				chk_state <= chip_empty;
			end if;

		end if;
	end process;


	process(all)
	begin
		if data(23 downto 20) = x"a" then
			wordtype <= chip_header;
		elsif data(23 downto 20) = x"b" then
			wordtype <= chip_trailer;
		elsif data(23 downto 20) = x"e" then
			wordtype <= chip_empty;
		elsif data(23 downto 21) = "110" then
			wordtype <= region_header;
		elsif data(23 downto 22) = "01" then
			wordtype <= chip_data;
		elsif data(23 downto 22) = "00" and data(7) = '0' then
			wordtype <= chip_data;
		else
			wordtype <= unknown;
		end if;
	end process;

end architecture;
