library ieee;
use ieee.STD_LOGIC_1164.all;

entity stave_data_buffer_TB is
end stave_data_buffer_TB;

architecture Behavioral of stave_data_buffer_TB is

	constant T_wrclk : time := 25 ns;
	constant T_rdclk : time := 10 ns;
	signal wr_clk, rd_clk, rst : std_logic := '0';

	signal data_in : std_logic_vector(23 downto 0);
	signal data_in_strb : std_logic_vector(2 downto 0);
	signal data_in_last, data_in_valid, data_in_ready : std_logic := '0';

	signal data_out : std_logic_vector(7 downto 0);
	signal data_out_last, data_out_valid, data_out_ready : std_logic := '0';

begin

	uut: entity work.stave_data_buffer
	port map(
		wr_clk => wr_clk,
		rd_clk => rd_clk,
		rst => rst,
		-- input FF
		data_in       => data_in,
		data_in_strb  => data_in_strb,
		data_in_last  => data_in_last,
		data_in_valid => data_in_valid,
		data_in_ready => data_in_ready,
		-- output FF
		data_out       => data_out,
		data_out_last  => data_out_last,
		data_out_valid => data_out_valid,
		data_out_ready => data_out_ready
	);


	rd_clk <= not rd_clk after T_rdclk/2;
	wr_clk <= not wr_clk after T_wrclk/2;

	stim: process

		procedure send_data(
			data : in std_logic_vector(23 downto 0);
			data_strb : in std_logic_vector(2 downto 0);
			data_last : in std_logic) is
		begin
			data_in <= data;
			data_in_strb <= data_strb;
			data_in_last <= data_last;
			data_in_valid <= '1';
			wait for T_wrclk;
			data_in_valid <= '0';
			wait for T_wrclk;
			if data_in_ready = '0' then
				wait until data_in_ready = '1' and wr_clk = '1';
			end if;
		end send_data;

	begin
		rst <= '0';
		wait for T_wrclk * 10;
		rst <= '1';
		wait for T_wrclk * 10;
		rst <= '0';
		wait for T_wrclk * 10;
		data_out_ready <= '1';
		wait for T_wrclk;
		wait until rising_edge(wr_clk);


		for n in 0 to 2 loop
			-- typical data
			send_data(x"a800ff", "110", '0');
			send_data(x"c1ffff", "100", '0');
			for i in 0 to 4 loop
				send_data(x"666666", "111", '0');
			end loop;
			send_data(x"b0ffff", "100", '1');
		end loop;

		for i in 0 to 4 loop
			send_data(x"e000ff", "110", '0');
		end loop;
		send_data(x"e100ff", "110", '1');

		wait;
	end process;

end Behavioral;
