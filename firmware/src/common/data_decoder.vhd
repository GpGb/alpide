library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.types_pkg.all;
use work.ALPIDE_pkg.all;

entity data_decoder is
	port(
		clk  : in std_logic;
		rst  : in std_logic;
		word_in     : in std_logic_vector(7 downto 0);
		word_in_dv  : in std_logic;
		word_in_lst : in std_logic;
		ready       : out std_logic;
		axis_pix_tdata  : out slv32;
		axis_pix_tlast  : out std_logic;
		axis_pix_tready : in std_logic;
		axis_pix_tvalid : out std_logic;
		dec_skip_data : in std_logic
	);
end data_decoder;


architecture Behavioral of data_decoder is

	type FSM_state is (read_word, data_short, data_long, decode_data, skip,
		write_cnt, write_last);
	signal state : FSM_state := read_word;
	attribute fsm_safe_state : string;
	attribute fsm_safe_state of state : signal is "auto_safe_state";

	-- Would be nicer if the axi fifo had a counter output..
	constant  pix_fifo_max_size : natural := 500;
	signal wr_counter : natural range 0 to pix_fifo_max_size := 0;
	signal counter_max : std_logic := '0';

	type word_enum is (unknown, stave_header, stave_trailer, chip_header, region_header,
		chip_data_short, chip_data_long, chip_trailer, ev_chip_skipped);
	signal wordtype : word_enum := stave_trailer;

	signal data_r  : std_logic_vector(15 downto 0);
	signal wrd_cnt : natural range 0 to 2 := 0;
	signal data_type, hit0 : std_logic := '0';
	signal skip_cnt : std_logic := '0';

	signal stave_id : std_logic_vector(3 downto 0);
	signal chip_id : std_logic_vector(3 downto 0) := (others => '0');
	signal region_id  : unsigned(4 downto 0);
	signal encoder_id : unsigned(3 downto 0);
	signal address    : unsigned(9 downto 0);
	signal hit_map    : std_logic_vector(6 downto 0);

	signal hit_bit_index : natural range 0 to 6 := 0;
	signal hit_x, hit_y : std_logic_vector(9 downto 0);
	signal hit_counter : unsigned(10 downto 0) := (others => '0');
	signal hit_map_cnt : unsigned(2 downto 0);

	--attribute mark_debug : string;
	--attribute mark_debug of state : signal is "true";
	--attribute mark_debug of word_in, word_in_dv, ready, word_in_lst : signal is "true";
	--attribute mark_debug of hit_counter, wordtype : signal is "true";
	--attribute mark_debug of hit_x, hit_y, chip_id, stave_id : signal is "true";
	--attribute mark_debug of region_id, encoder_id, address : signal is "true";
	--attribute mark_debug of axis_pix_tdata, axis_pix_tlast, axis_pix_tready, axis_pix_tvalid :
	--	signal is "true";

begin


	process(clk)
	begin
		if rising_edge(clk) then

			axis_pix_tvalid <= '0';
			axis_pix_tlast <= '0';

			if word_in_dv = '1' then
				data_r <= data_r(7 downto 0) & word_in;
			end if;

			case state is

				when read_word =>
					ready <= '1';
					skip_cnt <= '0';
					wrd_cnt <= 0;
					hit_bit_index <= 0;
					hit0 <= '0';

					if word_in_dv = '1' then

						case wordtype is
							when stave_header =>
								stave_id <= word_in(3 downto 0);
							when chip_header =>
								chip_id <= word_in(3 downto 0);
								hit_counter <= (others => '0');
								state <= skip;
							when region_header =>
								region_id <= unsigned(word_in(4 downto 0));
							when chip_data_short =>
								hit_counter <= hit_counter + 1;
								data_type <= '0';
								state <= data_short;
							when chip_data_long =>
								hit_counter <= hit_counter + 1;
								data_type <= '1';
								state <= data_long;
							when chip_trailer | ev_chip_skipped =>
								if dec_skip_data = '1' then
									ready <= '0';
									state <= write_cnt;
								end if;
							when stave_trailer | unknown =>
								state <= skip;
								skip_cnt <= '1';
						end case;

					end if;


				when data_short =>
					if word_in_dv = '1' then
						ready <= '0';
						encoder_id <= unsigned( data_r(5 downto 2) );
						address <= unsigned( std_logic_vector'(data_r(1 downto 0) & word_in) );

						if dec_skip_data = '0' then
							state <= decode_data;
						else
							state <= read_word;
						end if;

					end if;


				when data_long =>
					if word_in_dv = '1' then
						wrd_cnt <= wrd_cnt + 1;
						if wrd_cnt = 1 then
							encoder_id <= unsigned(data_r(13 downto 10));
							address <= unsigned(data_r(9 downto 0));
							hit_map <= word_in(6 downto 0);
							hit_counter <= hit_counter + hit_map_cnt;

							if dec_skip_data = '0' then
								state <= decode_data;
								ready <= '0';
							else
								state <= read_word;
							end if;
						end if;
					end if;


				when decode_data =>

					if hit_bit_index = 0 or hit_map(hit_bit_index) = '1' then
						axis_pix_tdata(31 downto 28) <= (others => '0');
						axis_pix_tdata(27 downto 20) <= stave_id & chip_id;
						axis_pix_tdata(19 downto 10) <= hit_y;
						axis_pix_tdata(9 downto 0)   <= hit_x;
						axis_pix_tvalid <= not counter_max;
					end if;

					-- just split in decode/write states...
					if counter_max = '1' then
						state <= read_word;
					elsif (hit_bit_index = 0 or hit_map(hit_bit_index) = '1') and
						(axis_pix_tvalid and axis_pix_tready) = '0' then
						state <= decode_data;
					elsif data_type = '0' or hit_bit_index = 6 then
						state <= read_word;
					elsif hit0 = '1' then
						hit_bit_index <= hit_bit_index + 1;
						address <= address + 1;
					end if;

					if (axis_pix_tvalid and axis_pix_tready) = '1' then
						hit0 <= '1';
					end if;


				when skip =>
					axis_pix_tdata <= (others => '0');

					if word_in_dv = '1' then

						skip_cnt <= '0';
						if skip_cnt = '0' then
							state <= read_word;
						end if;

						if word_in_lst = '1' then
							state <= write_last;
							ready <= '0';
						end if;

					end if;


				when write_last =>
					axis_pix_tvalid <= '1';
					axis_pix_tlast <= '1';

					if (axis_pix_tvalid and axis_pix_tready) = '1' then
						axis_pix_tvalid <= '0';
						state <= read_word;
					end if;


				when write_cnt =>
					ready <= '0';
					axis_pix_tdata <= (others => '0');
					axis_pix_tdata(10 downto  0) <= std_logic_vector(hit_counter);
					axis_pix_tdata(14 downto 11) <= chip_id;
					axis_pix_tdata(18 downto 15) <= stave_id;
					axis_pix_tvalid <= '1';

					if (axis_pix_tready and axis_pix_tvalid) = '1' then
						ready <= '1';
						axis_pix_tvalid <= '0';
						state <= read_word;
					end if;



			end case;

			if rst = '1' then
				state <= read_word;
			end if;

		end if;
	end process;


	wr_counter_p : process(clk)
	begin
		if rising_edge(clk) then

			if (axis_pix_tready and axis_pix_tvalid and axis_pix_tlast) = '1' then
				counter_max <= '0';
				wr_counter <= 0;
			elsif wr_counter = pix_fifo_max_size then
				counter_max <= '1';
			elsif (axis_pix_tready and axis_pix_tvalid) = '1' then
				wr_counter <= wr_counter + 1;
			else
				counter_max <= '0';
			end if;

			if rst = '1' then
				counter_max <= '0';
				wr_counter <= 0;
			end if;

		end if;
	end process;


	data_decoder: process(all)
		variable n_region_id  : natural range 0 to 2**5-1;
		variable n_encoder_id : natural range 0 to 2**4-1;
		variable n_hit_map_cnt : unsigned(2 downto 0);
	begin


		n_region_id  := to_integer(region_id);
		n_encoder_id := to_integer(encoder_id);
		if (xor address(1 downto 0) = '1') then
			hit_x <= uint2slv(n_region_id*32 + n_encoder_id*2 + 1, 10);
		else
			hit_x <= uint2slv(n_region_id*32 + n_encoder_id*2, 10);
		end if;
		hit_y <= std_logic_vector('0' & address(9 downto 1));

		n_hit_map_cnt := (others => '0');
		for n in 0 to 6 loop
			if word_in(n) = '1' then
				n_hit_map_cnt := n_hit_map_cnt + 1;
			end if;
		end loop;
		hit_map_cnt <= n_hit_map_cnt;

	end process;


	word_decode: process(all)
	begin

		if word_in(7 downto 0) = x"ff" then
			wordtype <= ev_chip_skipped;
		elsif word_in(7 downto 4) = x"f" then
			wordtype <= stave_header;
		elsif word_in(7 downto 4) = x"a" then
			wordtype <= chip_header;
		elsif word_in(7 downto 4) = x"b" then
			wordtype <= chip_trailer;
		elsif word_in(7 downto 4) = x"e" then
			wordtype <= stave_trailer;
		elsif word_in(7 downto 5) = "110" then
			wordtype <= region_header;
		elsif word_in(7 downto 6) = "01" then
			wordtype <= chip_data_short;
		elsif word_in(7 downto 6) = "00" then
			wordtype <= chip_data_long;
		else
			wordtype <= unknown;
		end if;

	end process;


end architecture;
