-- Why am I not using verilog?

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.types_pkg.all;
use work.ALPIDE_pkg.all;

entity ALPIDE_bhv_sim is
	generic(
		tclk : time := 20 ns;
		chip_id : slv8
	);
	port(
		ALP_CLK : in std_logic;
		ALP_CTRL : inout std_logic
	);
end entity;


architecture Behavioral of ALPIDE_bhv_sim is

	-- assemble a data package
	function gen_data_pkg(data : slv16;	chip_id : slv8)
		return std_logic_vector is
	begin
		-- number of "gap" bits after every word should be irrelevant
		return "1111" & data(15 downto 8) & "0111" & data(7 downto 0) & "01" & chip_id & '0';
	end gen_data_pkg;

	-- read a byte from ctrl line
	procedure read_byte(
		constant tclk : time;
		signal CTRL : in std_logic;
		signal byte : out slv8) is
	begin
		wait until CTRL = '0';
		wait for tclk;
		for n in 0 to 7 loop
			wait for tclk;
			byte(n) <= CTRL;
		end loop;
		wait for tclk;
		assert(CTRL = '1') report "ctrl: missed stopbit";
	end read_byte;

	-- signals
	signal ctrl_dec : std_logic;

	signal recv_op, recv_chipid : slv8 := x"00";
	signal recv_addr, recv_data : slv16;
	alias recv_addr_LSB : slv8 is recv_addr(7 downto 0);
	alias recv_addr_MSB : slv8 is recv_addr(15 downto 8);
	alias recv_data_LSB : slv8 is recv_data(7 downto 0);
	alias recv_data_MSB : slv8 is recv_data(15 downto 8);

	signal test_data : slv32_arr(0 to 4);
	signal counter : natural range 0 to 5 := 5;

begin

	alp_mainloop: process

		variable test_data_pkg : std_logic_vector(34 downto 0);

		constant reg_dat : slv16 := x"abab";
		constant idle_dat : slv32 := x"00ffffff";

	begin

		ALP_CTRL <= 'Z';
		wait until ALP_CTRL = '1';


		while true loop
			ALP_CTRL <= 'Z';

			--read opcode
			read_byte(Tclk, ctrl_dec, recv_op);
			case recv_op is
				when ALPIDE_WROP =>
					--read_byte(Tclk, ctrl_dec, recv_chipid);
					--read_byte(Tclk, ctrl_dec, recv_addr_LSB);
					--read_byte(Tclk, ctrl_dec, recv_addr_MSB);
					--read_byte(Tclk, ctrl_dec, recv_data_LSB);
					--read_byte(Tclk, ctrl_dec, recv_data_MSB);
					wait for Tclk*50;
					next;
				when ALPIDE_RDOP =>
					-- generate readout data
					-- chip header
					test_data(0) <= x"00" & "1010" & chip_id(3 downto 0) & x"00ff";
					-- region header
					test_data(1) <= x"00" & "110" & "00001" & x"ffff";
					-- data short
					test_data(2) <= x"00" & "01" & "00000000000000" & x"ff";
					-- data short
					test_data(3) <= x"00" & "01" & "00000000000000" & x"ff";
					-- chip trailer
					test_data(4) <= x"00" & "1011" & x"0" & x"ffff";
				when ALPIDE_TRIG =>
					counter <= 0;
					next;
				when x"78" =>
					counter <= 0;
				when others =>
					next;
			end case;

			-- read chipid and address
			read_byte(Tclk, ctrl_dec, recv_chipid);
			if recv_chipid /= chip_id then
				wait for Tclk * 65;
				next;
			end if;
			read_byte(Tclk, ctrl_dec, recv_addr(7 downto 0));
			read_byte(Tclk, ctrl_dec, recv_addr(15 downto 8));

			-- first turnaround
			wait for Tclk*9;
			ALP_CTRL <= '1';
			wait for Tclk*5;


			case recv_addr is
				when ALPIDE_DMU_FIFO_LO =>
					if counter < 5 then
						test_data_pkg := gen_data_pkg(test_data(counter)(15 downto 0), recv_chipid);
					else
						test_data_pkg := gen_data_pkg(idle_dat(15 downto 0), recv_chipid);
					end if;
				when ALPIDE_DMU_FIFO_HI =>
					if counter < 5 then
						test_data_pkg := gen_data_pkg(test_data(counter)(31 downto 16), recv_chipid);
						counter <= counter + 1;
					else
						test_data_pkg := gen_data_pkg(idle_dat(31 downto 16), recv_chipid);
					end if;
				when others =>
					test_data_pkg := gen_data_pkg(reg_dat, recv_chipid);
			end case;


			for n in 0 to 34 loop
				ALP_CTRL <= test_data_pkg(n);
				wait for Tclk;
			end loop;


			-- second turnaround
			wait for Tclk*5;
			ALP_CTRL <= 'Z';
			wait for Tclk*9;

		end loop;

		wait;
	end process alp_mainloop;

	process(ALP_CLK)
	begin
		if falling_edge(ALP_CLK) then
			ctrl_dec <= ALP_CTRL;
		end if;
	end process;


end Behavioral;
