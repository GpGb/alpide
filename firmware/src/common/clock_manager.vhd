library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Library xpm;
use xpm.vcomponents.all;

entity clock_manager is
	port(
		clk_osc : in std_logic;
		clk_40 : out std_logic;
		clk_80 : out std_logic
	);
end entity;


architecture NO_PLL of clock_manager is

	signal clk_80_i : std_logic;
	signal clk_div : std_logic := '0';

begin

	clk_ibuf_inst: IBUF
	port map(
		I => clk_osc,
		O => clk_80_i
	);

	clk_80_bufg: BUFG
	port map(
		I => clk_80_i,
		O => clk_80
	);

	clk_40_bufg: BUFG
	port map(
		I => clk_div,
		O => clk_40
	);

	clk_div_proc: process(clk_80)
	begin
		if rising_edge(clk_80) then
			clk_div <= not clk_div;
		end if;
	end process;

end NO_PLL;
