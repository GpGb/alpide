#ifndef SRC_TEMP_SENSOR_H_
#define SRC_TEMP_SENSOR_H_

int read_tracker_temp_data();

int set_temp_mask(uint8_t staveid, uint8_t mask);

#endif
