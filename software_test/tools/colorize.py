import rich
import rich.panel
import rich.table
import rich.box
import rich.columns
import rich.progress
import rich.pretty

from time import sleep


def index_to_chipid(index):
    stave_id = index//10

    chip_id_lsb = index%10
    if (chip_id_lsb > 4):
        chip_id_lsb += 3

    chip_id = 0x70 | (stave_id<<8) | chip_id_lsb
    return chip_id



def gen_turret_render(turret_id: int, chipmask: list) -> None:


    def gen_stave_render(stave_id: int, mask: int) -> rich.panel.Panel:
        row_l = []
        row_r = []
        grid = rich.table.Table.grid(padding=(0,1))

        for i in range(5):
            grid.add_column(justify='center')

            if (mask & (1<<i)):
                lcolor = '[red]'
            else:
                lcolor = '[green]'

            if (mask & (1<<(9-i))):
                rcolor = '[red]'
            else:
                rcolor = '[green]'

            row_l.append(lcolor + hex(index_to_chipid(i)))
            row_r.append(rcolor + hex(index_to_chipid(9-i)))

        grid.add_row(*row_l)
        grid.add_row(*row_r)

        if stave_id%3 == 0:
            position = 'top'
        elif stave_id%3 == 2:
            position = 'bottom'
        else:
            position = 'mid'

        stave = rich.panel.Panel(grid, title=f'stave {stave_id} ({position})', expand=False,
                border_style='blue')
        return stave


    staves = []

    for st in range(turret_id*3, turret_id*3+3):

        stave_chipmask = chipmask[st]
        staves.append(gen_stave_render(st, stave_chipmask))

    grid = rich.table.Table.grid()
    grid.add_column()
    grid.add_row(*staves)
    turret = rich.panel.Panel(grid, title=f'turret {turret_id}', expand=False,
            border_style='white')

    rich.print(turret)
    print()



def thr_progress(get_status, style, refresh=0.1) -> None:
    CHIP_N = 10
    CHARGE_STEPS = 30
    DCOL_NUM = 512

    rich.print('[b][red]Running thrscan...')

    with rich.progress.Progress(
            "[progress.description]{task.description}",
             rich.progress.BarColumn(),
             "[progress.percentage]{task.percentage:>3.0f}%",
             "({task.completed}/{task.total})",
            ) as progress:

        chip  = progress.add_task('[cyan]Chip:', total = CHIP_N)
        crg_s = progress.add_task('[cyan]Charge Step:', total = CHARGE_STEPS)
        d_col = progress.add_task('[cyan]Double Col:', total = DCOL_NUM)

        status = get_status()

        while(status != 0 and status != 0xffffffff):
            status = get_status()
            dcol_state  = (status>>8)&0xffff
            cstep_state = (status>>24)

            if style == 'thr' and progress.tasks[crg_s].completed > cstep_state:
                progress.advance(chip, advance=1)
            elif style =='digi' and (progress.tasks[d_col].completed > dcol_state):
                progress.advance(chip, advance=1)

            progress.update(d_col, completed = dcol_state)
            progress.update(crg_s, completed = cstep_state)

            sleep(refresh)

        progress.update(chip, completed = CHIP_N)
        progress.update(crg_s, completed = CHARGE_STEPS)
        progress.update(d_col, completed = DCOL_NUM)

    rich.print('[b][red]..Done')
    print()


def thr_result(data: list) -> None:

    tabs = []
    for stave in range(15):

        tab = rich.table.Table(box = rich.box.SIMPLE,
                title = f'[red][b]stave {stave}')

        tab.add_column('chipid', justify='right', style='white')
        tab.add_column('thr avg', justify='right', style='cyan')
        tab.add_column('thr std', justify='right', style='cyan')

        for i in range(10):
            index = i+stave*10
            cid = hex(index_to_chipid(i))
            tab.add_row(cid, str(data[index][0]), str(data[index][1]))

        tabs.append(tab)

    columns = rich.columns.Columns(tabs, equal=True, expand=True)
    rich.print(columns)


def digiscan_result(chipmask: int, data: list) -> None:
    tab = rich.table.Table(box = rich.box.SIMPLE)

    tab.add_column('chipid', justify='right', style='cyan')
    tab.add_column('hits', justify='right', style='cyan')
    tab.add_column('missing', justify='right', style='magenta')

    HITS = 1024*512*10

    n = 0
    for i in range(0, 10):

        if (chipmask>>i)&0x1 == 1:
          tab.add_row(hex(index_to_chipid(i)), 'masked', '--')
        else:
          tab.add_row(hex(index_to_chipid(i)), str(data[n]), str(HITS-data[n]))
          n += 1

    panel = rich.panel.Panel(tab, expand=False, title = 'Digiscan results')

    rich.print(panel)

# Demo
if __name__ == '__main__':

    # chipscan demo
    chipscan_res = [0]*15
    chipscan_res[1] = 0x1
    chipscan_res[2] = 0x5555
    chipscan_res[3] = 0x7

    gen_turret_render(0, chipscan_res)
    gen_turret_render(1, chipscan_res)

    # thescan demo
    def thr_state():
        if thr_state.dcol != 512:
            thr_state.dcol += 1
        else:
            thr_state.dcol = 1
            thr_state.cstep += 10

        if (thr_state.cstep == 30):
            thr_state.cstep = 0
            thr_state.chip += 1
            if thr_state.chip == 2:
                return 0

        return (thr_state.cstep<<24 | thr_state.dcol<<8 | 0x00)

    thr_state.chip = 0
    thr_state.cstep = 0x0
    thr_state.dcol = 0x1

    thr_progress(thr_state, 'thr', 0.001)

    import random
    thrdata = []
    for i in range(150):
        avg = round(random.uniform(50, 150), 3)
        std = round(random.uniform(0, 5), 3)
        thrdata.append((avg, std))

    thr_result(thrdata)
