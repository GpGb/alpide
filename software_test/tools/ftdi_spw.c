#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <libftdi1/ftdi.h>

#include <signal.h>

#include <errno.h>
#include <string.h>

#define SPW_FSM_RDOP  0x03
#define SPW_FSM_WROP  0x0c
#define SPW_FSM_BRDOP 0xee

#define RX_DATA_MAX   800

struct ftdi_context *ftdi;
int sock_fd = 0;

int log_level = 0;

// ******** signal handler *********
static bool terminate = false;
void sigint_handler(int sig) {
	terminate = true;
}
// *********************************


// **************************** FTDI FIFO *******************************
int init_ftdi_fifo(){

	signal(SIGINT, sigint_handler);

	const int vendor_id = 0x403;
	const int product_id = 0x6010;
	const char* product_desc = "Digilent USB Device";

	int ret;

	ftdi = ftdi_new();
	ret = ftdi_set_interface(ftdi, INTERFACE_A);
	if (ret < 0){
		printf("ftdi set_interface failed\n");
		return ret;
	}

	ret = ftdi_usb_open_desc(ftdi, vendor_id, product_id, product_desc, NULL);
	if (ret < 0){
		printf("unable to open device ftdi error %d \n", ret);
		return ret;
	}

	ret = ftdi_usb_reset(ftdi);
	ret |= ftdi_set_bitmode(ftdi, 0xFF, BITMODE_RESET);
	if (ret < 0){
		printf("Device reset failed\n");
		return ret;
	}

	ret = ftdi_set_bitmode(ftdi, 0xFF, BITMODE_SYNCFF);
	if (ret < 0){
		printf("Unable to set bitmode SYNCFF\n");
		return ret;
	}

	ftdi_usb_purge_buffers(ftdi);

	return 0;
}


void cleanup(){

	if (ftdi_usb_reset(ftdi) < 0)
		printf("FTDI device reset failed\n");

	if (ftdi_set_bitmode(ftdi, 0xFF, BITMODE_RESET) < 0)
		printf("FTDI BITMODE reset failed\n");

	ftdi_usb_close(ftdi);
	ftdi_free(ftdi);

}


int write_fifo(uint8_t* data, uint32_t len){

	int ret = ftdi_write_data(ftdi, data, len);

	if (ret != len){
		printf("Error writing to FTDI fifo, errcode: %d\n", ret);
		ret = -1;
	}

	return ret;
}


int read_fifo(uint8_t* data, uint32_t len){

	int length = 0;

	while(length == 0 && !terminate){
		length = ftdi_read_data(ftdi, data, len);

		if (length < 0){
			printf("Error reading from FTDI fifo, errcode: %d\n", length);
			return length;
		}

	}

	if (terminate)
		return -1111;
	else
		return length;
}

// *****************************************************************************


int spw_send_cmd(uint8_t opcode, uint32_t addr, uint32_t data, uint32_t *rcv_data){

	if (log_level > 1){
		printf("snd cmd: %x, addr: %x, dat: %x\n", opcode, addr, data);
	}

	uint8_t pkt_buffer[10];
	pkt_buffer[0] = opcode;
	memcpy(pkt_buffer+1, &addr, sizeof(uint32_t));
	memcpy(pkt_buffer+5, &data, sizeof(uint32_t));
	pkt_buffer[9] = 0;

	write_fifo(pkt_buffer, sizeof(pkt_buffer));

	if (rcv_data == NULL){
		return 0;
	}

	// read response
	int word_count = 0;
	if (opcode == SPW_FSM_RDOP){
		word_count = 1;
	}
	else if (opcode == SPW_FSM_BRDOP){
		word_count = data;
	}
	else{
		return 0;
	}

	uint8_t read_buffer[word_count*10];
	int ret = read_fifo(read_buffer, word_count*10);
	if (ret != word_count*10){
		printf("Error reading response: %d\n", ret);
		return -1;
	}

	// extract data from pkts
	uint32_t index = 0;
	for(int n=0; n<word_count; n++){
		memcpy(&rcv_data[index++], &read_buffer[10*n+5], sizeof(uint32_t));
	}

	return word_count;
}


// buffer must be at least RX_DATA_MAX words
int read_event(uint32_t* rcv_data){


	// read first header word
	int ret = read_fifo((uint8_t*) rcv_data, sizeof(uint32_t));
	uint32_t pkt_size = rcv_data[0]&0xffff;

	if (log_level > 1){
		printf("recv header: %#x\n", rcv_data[0]);
		printf("recv pkt len: %d\n", pkt_size);
	}

	// read rest of packet
	ret = read_fifo((uint8_t*) &rcv_data[1], pkt_size+12);

	return ret;
}
