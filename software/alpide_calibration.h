#ifndef SRC_ALPIDE_CAL_H_
#define SRC_ALPIDE_CAL_H_

int run_threshold_cal(uint8_t stave_id, uint16_t vcasn0, uint8_t target);
int run_stave_pm_scan(uint8_t stave_id, uint8_t mode);
int run_chip_pm_scan(uint16_t chip_id, uint8_t mode);

int save_DAC_values(uint8_t stave_id, uint8_t findex);

#endif
