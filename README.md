# HEPD TDAQ

This repository contains the firmware and software for the HEPD-02 pixel tracker.
The following sections shortly describes the main operative procedures.

# Build the firmware
The Makefile in the in `firmware` directory is capable to fully generate and build the project at any stage.
It defines the following targets:
* vivado.xpr: generate the vivado project directory.
* syth.bit: run synthesis and implementation to generate the base bitstream and hardware platform definition (.xsa).
* vitis.prj: generate the vitis application project for the microblaze from the .xsa.
* MB_firm.elf: build the vitis project to obtain the .elf.
* TDAQ.bit/nexys.bit : merge the MB_firm.elf from vitis in the base bitstream.

In principle, starting from a clean folder, it should be possible to get to a fully working bitstream running:
```
$ make TDAQ.bit
or
$ make prj=nexys nexys.bit
```
The build process can be controlled by the following variables:
* `prj`: target hardware platform, possible values are `TDAQ` or `nexys` (default: `TDAQ`).
* `jobs`: number of jobs to use for synthesis (default: `$(nproc)`, set to lower for low memory systems).
* `release`: select `Debug` or `Release` target for the vitis project (default: `Debug`).

After the building process run `$ make cleanup` to remove unnecessary log files from the cwd.
Running `$ make clean` instead will reset the vitis project, and ask confirmation to delete the synth.bit.
To clean the project completely delete the generated vivado project and/or the project.vitis subdirectory manually.


# Test tools Description
The folder `software_test/tools` contains various utilities to interact with the DAQ system and process the received data:
* daq_test.py: CLI tool to interact with the TDAQ board.
* process_raw.py: script to elaborate and plot .raw datafile.
* raw_reader.py: simple python class to read raw files.

All 3 tools generate and help string when run with the ```-h``` or ```--help``` argument.

## ALTAI configuration handling
The configuration of the chips is stored in a binary file inside the storage flash memory on the FPGA board.
The chips are initialized and configured according the content of this file with the command:
```
$ python daq_test.py --init <staveID>
```
It is possible to specify staveID = 0xf to init all staves in sequence.

In general the configuration registers of each chip can be read and written individually with the `-w/r` option.
This is inconvenient when a large number of registers must be set or when it is necessary to switch between different configurations readily.
In this case the configuration of any chip can also be set using a configuration ini file.
A sample is available in `software_test/tools/alpide.conf`.
The new configuration can be loaded with the command:
```
$ python daq_test.py --conf-file <path/to/alpide.config>

```
In any case it is required to first run `./MB_daq_utils --init` after the system is powered on to ensure proper initialization.
After than, any new configuration can be loaded any number of times using the previous command.

If necessary, the new configuration can also be written on the flash, so that it is loaded when the `--init` command is run:
```
$ python daq_test.py --conf-file <path/to/alpide.config> --write-flash <file_index>
```
Since this replaces the default configuration that is used to configure the chips when the `--init` command is executed, care must be taken to provide a conf file with *all* the relevant registers set properly.
The configuration provided in the sample `alpide.conf` file is considered a safe default configuration.

## Chip masking
It is possible to esclude any chip from the readout procedure (masking).
The data format contain a dedicated `chip mask` word to indicate the mask used for stave, and the tool `process\_raw` will warn about any chip being masked.
A chip can be manually masked with the command:
```
python daq_test.py --mask-chip <chip_id>
```
to add single chip to the existing chip mask or
```
./toos/MB_daq_util --set-chipmask <stave-id> <10-bit hex mask>

```
where `<10-bit hex mask>` is the mask bytestring, where the nth bit is set to '1' to mask the corresponding chip on hte stave.

The current state of the chipmask can be read with the following command, returning the list of masked chip ids
```
python daq_test.py --read-chipmask <stave_id>
```


# Test Procedures (Nexys)

## Set-up
* Download the last release of the DAQ fimware and software.
* Compile the DAQ test tools:
    ```
    $ cd software_test/tools
    $ make all
    ```
* If boot from USB cable is desired, download and compile [OpenFPGAloader](https://github.com/trabucayre/openFPGALoader).
* Load the .bit file from the realse page in the FPGA:
    * To boot from micro sd, save the .bit in the micro sd (in a fat32 partition) and on the Nexys Video FPGA board set the jumper `jp4` on the “usb/sd” position.
    * To boot from USB (jtag) set the jumper `jp4` on “jtag” and run:
    ```$ openFPGALoader -b nexysVideo nexys.bit```