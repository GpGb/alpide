#ifndef SRC_ALPIDE_H_
#define SRC_ALPIDE_H_

#include <stdlib.h>
#include <stdint.h>

#define FSM_SET_MSK  0xf0
#define FSM_GET_MSK  0xf1

#define ALPIDE_GRST  0xd2
#define ALPIDE_PRST  0xe4
#define ALPIDE_TRIG  0x55
#define ALPIDE_WROP  0x9c
#define ALPIDE_RDOP  0x4e
#define ALPIDE_RORST 0x63
#define ALPIDE_PULSE 0x78
#define ALPIDE_FIFOTEST     0xff01
#define ALPIDE_LOADOBDEFCFG 0xff02
#define ALPIDE_ADCMEASURE   0xff20

#define ALPIDE_REG_CMD      0x0
#define ALPIDE_REG_MODECTRL 0x1
#define ALPIDE_REG_FROMU1   0x4
#define ALPIDE_REG_FROMU2   0x5
#define ALPIDE_REG_FROMU_PULSE1 0x7
#define ALPIDE_REG_FROMU_PULSE2 0x8

#define ALPIDE_REG_CMUDMU   0x10
#define ALPIDE_REG_PIXCFG   0x500
#define ALPIDE_REG_MON_OVR  0x600
#define ALPIDE_REG_VRESETD  0x602
#define ALPIDE_REG_VCASN    0x604
#define ALPIDE_REG_VPULSEH  0x605
#define ALPIDE_REG_VPULSEL  0x606
#define ALPIDE_REG_VCASN2   0x607
#define ALPIDE_REG_VTEMP    0x609
#define ALPIDE_REG_IDB      0x60c
#define ALPIDE_REG_ITHR     0x60e
#define ALPIDE_REG_ADC_CTRL 0x610
#define ALPIDE_REG_ADC_CAL  0x612
#define ALPIDE_REG_ADC_AVSS 0x613


#define ALP_CMD_REGS_RO_BASEADDR 0x5
#define ALP_CMD_COMMAND_REG_OFF  0x0
#define ALP_CMD_PAYLOAD_REG_OFF  0x1
#define ALP_CMD_RDOUTMUX_REG_OFF 0x3
#define ALP_CMD_ALARM_REG_OFF    0x4
#define ALP_CMD_RESPONSE_REG_OFF ALP_CMD_REGS_RO_BASEADDR
#define ALP_BUSY_REG_OFF         ALP_CMD_REGS_RO_BASEADDR+1
#define ALP_TRIG_CNT_REG_OFF     ALP_CMD_REGS_RO_BASEADDR+2


#define ALARM_TYPE_TEMP  0x0
#define ALARM_TYPE_MASK  0x1
#define ALARM_TYPE_BOOT  0x2

/******************************* Low level interface ***********************************/

int write_ctrl_reg(uint16_t offset, uint32_t value);
int read_ctrl_reg(uint32_t offset, uint32_t *value);
int send_alpide_cmd(uint8_t op_code, uint16_t chip_id, uint16_t addr, uint16_t data,
		uint16_t* res);
int wait_nBusy(uint16_t chip_id);

inline int write_alp_reg(uint16_t chip_id, uint16_t addr, uint16_t data){
	return send_alpide_cmd(ALPIDE_WROP, chip_id, addr, data, NULL);
};
inline int read_alp_reg(uint16_t chip_id, uint16_t addr, uint16_t* res){
	return send_alpide_cmd(ALPIDE_RDOP, chip_id, addr, 0, res);
};

void write_alarm_reg(uint8_t type, uint16_t data);

int set_chipmask(uint8_t stave_id, uint16_t mask);
uint16_t get_chipmask(uint8_t stave_id);


/******************************** Alpide procedures *********************************/

int chipid_to_index(uint16_t chip_id);
uint16_t index_to_chipid(uint16_t index);
uint16_t column_pixreg_addr(uint16_t x);
uint16_t row_pixreg_addr(uint16_t x);
int set_reset_pixregs(uint16_t chip_id, char reg, uint8_t s);

int configure_stave(uint8_t stave_id, uint8_t file);
int run_chip_scan();
int update_chipmask();
uint16_t get_stave_chipmask(uint8_t stave_id);

int mask_pixels();
int scan_noisy_pixels(uint8_t stave_id, uint8_t thresh, uint32_t n_trigger);

int alp_fifo_selftest(uint16_t chip_id);
int run_dpram_test(uint16_t chip_id);
int dump_chip_regs(uint16_t chip_id);

int adc_calibrate(uint16_t chip_id);
int measure_alp_temp(uint8_t stave_id);

#endif
